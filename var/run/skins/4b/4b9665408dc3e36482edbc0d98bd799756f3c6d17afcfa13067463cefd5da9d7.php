<?php

/* left_menu/sales_channels/body.twig */
class __TwigTemplate_51f22412a1949d5e4285f547458b5d8ba3231f3fabf3e3e0654348435029ab11 extends \XLite\Core\Templating\Twig\Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 4
        echo "<a href=\"#\" class=\"";
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getClass", array(), "method"), "html", null, true);
        echo "\" data-toggle=\"tooltip\" data-placement=\"bottom\" title=\"";
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('t')->getCallable(), array("Add sales channel")), "html", null, true);
        echo "\">
  ";
        // line 5
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "displayCommentedData", array(0 => $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getURLParams", array(), "method")), "method"), "html", null, true);
        echo "
  ";
        // line 6
        echo $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getSVGImage", array(0 => "images/plus.svg"), "method");
        echo "
</a>
";
    }

    public function getTemplateName()
    {
        return "left_menu/sales_channels/body.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  30 => 6,  26 => 5,  19 => 4,);
    }
}
/* {##*/
/*  # Sales channels plus button*/
/*  #}*/
/* <a href="#" class="{{ this.getClass() }}" data-toggle="tooltip" data-placement="bottom" title="{{ t('Add sales channel') }}">*/
/*   {{ this.displayCommentedData(this.getURLParams()) }}*/
/*   {{ this.getSVGImage('images/plus.svg')|raw }}*/
/* </a>*/
/* */
