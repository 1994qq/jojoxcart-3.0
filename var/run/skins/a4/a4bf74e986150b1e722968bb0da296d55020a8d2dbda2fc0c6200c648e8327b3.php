<?php

/* /var/www/html/xcart/skins/customer/modules/XC/ThemeTweaker/themetweaker_panel/panel/content.twig */
class __TwigTemplate_8b2cb964544334bead3112ddbe8ff823a1daada55fef8bb69a9d42f6c6ce4dff extends \XLite\Core\Templating\Twig\Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 6
        echo "
<div class=\"themetweaker-panel--content\" data-panel-content>
  ";
        // line 8
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('widget_list')->getCallable(), array($this->env, $context, array(0 => "themetweaker-panel--content"))), "html", null, true);
        echo "
</div>";
    }

    public function getTemplateName()
    {
        return "/var/www/html/xcart/skins/customer/modules/XC/ThemeTweaker/themetweaker_panel/panel/content.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  23 => 8,  19 => 6,);
    }
}
/* {##*/
/*  # Layout editor panel*/
/*  #*/
/*  # @ListChild(list="themetweaker-panel--body", weight="100")*/
/*  #}*/
/* */
/* <div class="themetweaker-panel--content" data-panel-content>*/
/*   {{ widget_list('themetweaker-panel--content') }}*/
/* </div>*/
