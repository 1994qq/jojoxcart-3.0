<?php

/* items_list/module/manage/parts/columns/module-main-section/actions/settings.twig */
class __TwigTemplate_a141b5947fb8aff2393abb9e35c4cf5c28ed14f5c4c191ee3a887d88af2ce99a extends \XLite\Core\Templating\Twig\Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 4
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('widget')->getCallable(), array($this->env, $context, array(0 => "XLite\\View\\Button\\SimpleLink", "location" => $this->getAttribute($this->getAttribute((isset($context["this"]) ? $context["this"] : null), "module", array()), "getSettingsForm", array(), "method"), "label" => call_user_func_array($this->env->getFunction('t')->getCallable(), array("Settings")), "style" => "settings"))), "html", null, true);
        echo "
";
    }

    public function getTemplateName()
    {
        return "items_list/module/manage/parts/columns/module-main-section/actions/settings.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  19 => 4,);
    }
}
/* {##*/
/*  # Settings link*/
/*  #}*/
/* {{ widget('XLite\\View\\Button\\SimpleLink', location=this.module.getSettingsForm(), label=t('Settings'), style='settings') }}*/
/* */
