<?php

/* /var/www/html/xcart/skins/admin/order/packing_slip/parts/body/items/item.qty_ship.twig */
class __TwigTemplate_2289e75757c8bf6768b463506761800c18e981af1d4b7f420ba6810006c4306f extends \XLite\Core\Templating\Twig\Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 6
        echo "<td class=\"qty-ship\"></td>
";
    }

    public function getTemplateName()
    {
        return "/var/www/html/xcart/skins/admin/order/packing_slip/parts/body/items/item.qty_ship.twig";
    }

    public function getDebugInfo()
    {
        return array (  19 => 6,);
    }
}
/* {##*/
/*  # Quantity item cell*/
/*  #*/
/*  # @ListChild (list="packing_slip.item", weight="40")*/
/*  #}*/
/* <td class="qty-ship"></td>*/
/* */
