<?php

/* /var/www/html/xcart/skins/admin/items_list/payment/methods/parts/header.description.twig */
class __TwigTemplate_cad4127925a0646ba596803b4f1343e389736cd11dc466e657844f804d9fb3d4 extends \XLite\Core\Templating\Twig\Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 6
        echo "
";
        // line 7
        if ($this->getAttribute($this->getAttribute((isset($context["this"]) ? $context["this"] : null), "method", array()), "getAltAdminDescription", array(), "method")) {
            // line 8
            echo "  <div class=\"description\">";
            echo $this->getAttribute($this->getAttribute((isset($context["this"]) ? $context["this"] : null), "method", array()), "getAltAdminDescription", array(), "method");
            echo "</div>
";
        }
    }

    public function getTemplateName()
    {
        return "/var/www/html/xcart/skins/admin/items_list/payment/methods/parts/header.description.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  24 => 8,  22 => 7,  19 => 6,);
    }
}
/* {##*/
/*  # Payment methods list : line : header : description*/
/*  #*/
/*  # @ListChild (list="payment.methods.list.header", weight=400)*/
/*  #}*/
/* */
/* {% if this.method.getAltAdminDescription() %}*/
/*   <div class="description">{{ this.method.getAltAdminDescription()|raw }}</div>*/
/* {% endif %}*/
/* */
