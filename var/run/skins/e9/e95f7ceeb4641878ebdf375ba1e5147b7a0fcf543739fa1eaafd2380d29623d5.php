<?php

/* /var/www/html/xcart/skins/customer/modules/XC/ThemeTweaker/themetweaker/layout_editor/panel_parts/hidden_blocks.twig */
class __TwigTemplate_8731204e5887e9c8f7ea36f9c6a8969dbc98211f8c5b11fad81bb794c7d21253 extends \XLite\Core\Templating\Twig\Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 6
        echo "
<div class='layout-editor-hidden-list'>
\t<a class='layout-editor-hidden-block' v-for=\"item in hiddenBlocks\" @click=\"showBlock(item)\">
\t\t<span class=\"name\" v-text=\"getBlockName(item)\"></span>
\t</a>
</div>
";
    }

    public function getTemplateName()
    {
        return "/var/www/html/xcart/skins/customer/modules/XC/ThemeTweaker/themetweaker/layout_editor/panel_parts/hidden_blocks.twig";
    }

    public function getDebugInfo()
    {
        return array (  19 => 6,);
    }
}
/* {##*/
/*  # Layout editor show hidden blocks button*/
/*  #*/
/*  # @ListChild (list="layout_editor", weight="20")*/
/*  #}*/
/* */
/* <div class='layout-editor-hidden-list'>*/
/* 	<a class='layout-editor-hidden-block' v-for="item in hiddenBlocks" @click="showBlock(item)">*/
/* 		<span class="name" v-text="getBlockName(item)"></span>*/
/* 	</a>*/
/* </div>*/
/* */
