<?php

/* modules/XC/Onboarding/wizard/wizard.twig */
class __TwigTemplate_62d31731285ad4b66dee46968a623be3df0c5510e3193d3a07d7edabc6e22be3 extends \XLite\Core\Templating\Twig\Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 4
        echo "<div id=\"onboarding-wizard-loader-point\">
  <xlite-onboarding-wizard
    inline-template
    step=\"";
        // line 7
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getWizardStep", array(), "method"), "html", null, true);
        echo "\"
    :steps=\"";
        // line 8
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, twig_jsonencode_filter($this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getWizardSteps", array(), "method")), "html", null, true);
        echo "\"
    state=\"";
        // line 9
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getWizardState", array(), "method"), "html", null, true);
        echo "\"
    last-product=\"";
        // line 10
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getLastAddedProductId", array(), "method"), "html", null, true);
        echo "\">
    <div class=\"onboarding-wizard-wrapper onboarding-wizard--initial\" :class=\"classes\">
      <div id=\"onboarding-wizard\" class=\"onboarding-wizard\">
        <div class=\"onboarding-wizard--inner\" :class=\"bodyClasses\">
          <div class=\"reloading-element\"></div>
          <div class=\"onboarding-wizard--header\">
            ";
        // line 16
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('widget_list')->getCallable(), array($this->env, $context, array(0 => "onboarding-wizard.header"))), "html", null, true);
        echo "
          </div>
          <div class=\"onboarding-wizard--body\">
            ";
        // line 19
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('widget_list')->getCallable(), array($this->env, $context, array(0 => "onboarding-wizard.body"))), "html", null, true);
        echo "
          </div>
        </div>
      </div>
      ";
        // line 23
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('widget_list')->getCallable(), array($this->env, $context, array(0 => "onboarding-wizard.after"))), "html", null, true);
        echo "
    </div>
  </xlite-onboarding-wizard>
</div>";
    }

    public function getTemplateName()
    {
        return "modules/XC/Onboarding/wizard/wizard.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  58 => 23,  51 => 19,  45 => 16,  36 => 10,  32 => 9,  28 => 8,  24 => 7,  19 => 4,);
    }
}
/* {##*/
/*  # Layout editor panel*/
/*  #}*/
/* <div id="onboarding-wizard-loader-point">*/
/*   <xlite-onboarding-wizard*/
/*     inline-template*/
/*     step="{{ this.getWizardStep() }}"*/
/*     :steps="{{ this.getWizardSteps() | json_encode }}"*/
/*     state="{{ this.getWizardState() }}"*/
/*     last-product="{{ this.getLastAddedProductId() }}">*/
/*     <div class="onboarding-wizard-wrapper onboarding-wizard--initial" :class="classes">*/
/*       <div id="onboarding-wizard" class="onboarding-wizard">*/
/*         <div class="onboarding-wizard--inner" :class="bodyClasses">*/
/*           <div class="reloading-element"></div>*/
/*           <div class="onboarding-wizard--header">*/
/*             {{ widget_list('onboarding-wizard.header') }}*/
/*           </div>*/
/*           <div class="onboarding-wizard--body">*/
/*             {{ widget_list('onboarding-wizard.body') }}*/
/*           </div>*/
/*         </div>*/
/*       </div>*/
/*       {{ widget_list('onboarding-wizard.after') }}*/
/*     </div>*/
/*   </xlite-onboarding-wizard>*/
/* </div>*/
