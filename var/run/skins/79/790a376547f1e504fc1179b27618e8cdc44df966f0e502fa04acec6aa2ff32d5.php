<?php

/* button/addon/install_module_not_selected.twig */
class __TwigTemplate_970ae4cf9a87bb3146ba4f5781485a8b4f8ce69cf3e55d0cd60bc2da988007fd extends \XLite\Core\Templating\Twig\Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 4
        echo "
<div class=\"modules-not-selected";
        // line 5
        if (($this->getAttribute((isset($context["this"]) ? $context["this"] : null), "countModulesSelected", array(), "method") > 0)) {
            echo " hide-selected";
        }
        echo "\">";
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('t')->getCallable(), array("Not selected")), "html", null, true);
        echo "</div>
";
    }

    public function getTemplateName()
    {
        return "button/addon/install_module_not_selected.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  22 => 5,  19 => 4,);
    }
}
/* {##*/
/*  # Regular button*/
/*  #}*/
/* */
/* <div class="modules-not-selected{% if this.countModulesSelected() > 0 %} hide-selected{% endif %}">{{ t('Not selected') }}</div>*/
/* */
