<?php

/* items_list/module/manage/parts/columns/module-main-section/actions/uninstall.twig */
class __TwigTemplate_fb5775959e231fb7b0446f7cdf9cf4eb5886a1eef3e3661be55faf5cc6dd3e3e extends \XLite\Core\Templating\Twig\Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 4
        echo "
";
        // line 5
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('widget')->getCallable(), array($this->env, $context, array(0 => "\\XLite\\View\\Button\\Remove", "buttonName" => (("switch[" . $this->getAttribute($this->getAttribute((isset($context["this"]) ? $context["this"] : null), "module", array()), "getModuleId", array(), "method")) . "][delete]"), "label" => "Uninstall", "style" => "uninstall"))), "html", null, true);
        echo "
";
    }

    public function getTemplateName()
    {
        return "items_list/module/manage/parts/columns/module-main-section/actions/uninstall.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  22 => 5,  19 => 4,);
    }
}
/* {##*/
/*  # Modules actions list*/
/*  #}*/
/* */
/* {{ widget('\\XLite\\View\\Button\\Remove', buttonName='switch[' ~ this.module.getModuleId() ~ '][delete]', label='Uninstall', style='uninstall') }}*/
/* */
