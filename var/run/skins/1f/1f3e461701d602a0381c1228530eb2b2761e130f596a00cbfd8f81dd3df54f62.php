<?php

/* form_model/body.twig */
class __TwigTemplate_991f0ee5eba07bdacfb3603a3c7a904fac1088f26ac8c889496399ebd4a3f7cd extends \XLite\Core\Templating\Twig\Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "
";
        // line 2
        $context["form"] = $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getFormView", array(), "method");
        // line 3
        $this->env->getExtension('form')->renderer->setTheme((isset($context["form"]) ? $context["form"] : null), $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getFormThemeFiles", array(), "method"));
        // line 4
        echo "
";
        // line 5
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : null), 'form_start', array("attr" => array("novalidate" => "novalidate", "v-on:submit" => "onSubmit")));
        echo "

";
        // line 7
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('widget_list')->getCallable(), array($this->env, $context, array(0 => "before", "type" => "inherited"))), "html", null, true);
        echo "

";
        // line 9
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : null), 'widget');
        echo "

";
        // line 11
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('widget_list')->getCallable(), array($this->env, $context, array(0 => "after", "type" => "inherited"))), "html", null, true);
        echo "

";
        // line 13
        if ($this->getAttribute((isset($context["this"]) ? $context["this"] : null), "useButtonPanel", array(), "method")) {
            // line 14
            echo "  ";
            echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["this"]) ? $context["this"] : null), "buttonPanel", array()), "display", array(), "method"), "html", null, true);
            echo "
";
        }
        // line 16
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('widget_list')->getCallable(), array($this->env, $context, array(0 => "footer", "type" => "inherited"))), "html", null, true);
        echo "

";
        // line 18
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : null), 'form_end');
        echo "
";
    }

    public function getTemplateName()
    {
        return "form_model/body.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  62 => 18,  57 => 16,  51 => 14,  49 => 13,  44 => 11,  39 => 9,  34 => 7,  29 => 5,  26 => 4,  24 => 3,  22 => 2,  19 => 1,);
    }
}
/* */
/* {% set form = this.getFormView() %}*/
/* {% form_theme form with this.getFormThemeFiles() %}*/
/* */
/* {{ form_start(form, {'attr': {'novalidate': 'novalidate', 'v-on:submit': 'onSubmit'}}) }}*/
/* */
/* {{ widget_list('before', type='inherited') }}*/
/* */
/* {{ form_widget(form) }}*/
/* */
/* {{ widget_list('after', type='inherited') }}*/
/* */
/* {% if this.useButtonPanel() %}*/
/*   {{ this.buttonPanel.display() }}*/
/* {% endif %}*/
/* {{ widget_list('footer', type='inherited') }}*/
/* */
/* {{ form_end(form) }}*/
/* */
