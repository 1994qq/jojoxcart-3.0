<?php

/* modules/CDev/Paypal/onboarding/body.twig */
class __TwigTemplate_ff4e1f9a88af73e19d29ceedd2de1bcc2f5368ec64b9e78735161386cfde19e3 extends \XLite\Core\Templating\Twig\Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 4
        echo "
<xlite-onboarding-paypal-card inline-template>
  <div class=\"online online-paypal\" :class=\"classes\">
    <div class=\"method-type\">
      ";
        // line 8
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('t')->getCallable(), array("Online method")), "html", null, true);
        echo "
    </div>

    <div class=\"image\">
      <img src=\"";
        // line 12
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('asset')->getCallable(), array("modules/CDev/Paypal/onboarding/logo.png")), "html", null, true);
        echo "\">
    </div>

    <div class=\"method-name\">
      ";
        // line 16
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('t')->getCallable(), array("PayPal Express Checkout")), "html", null, true);
        echo "
    </div>

    <div class=\"note\">
      ";
        // line 20
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('t')->getCallable(), array("No merchant account required. Simple onboarding for you and easy checkout for your customers.")), "html", null, true);
        echo "
    </div>

    ";
        // line 23
        if ($this->getAttribute((isset($context["this"]) ? $context["this"] : null), "isPaypalConfigured", array(), "method")) {
            // line 24
            echo "    <div class=\"switcher";
            if ($this->getAttribute((isset($context["this"]) ? $context["this"] : null), "isPaypalMethodEnabled", array(), "method")) {
                echo " enabled";
            }
            echo "\">
      <span class=\"inactive\">";
            // line 25
            echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('t')->getCallable(), array("checkbox.onoff.off")), "html", null, true);
            echo "</span>
      <a href=\"#\" @click.prevent=\"switchPaypalMethod(";
            // line 26
            echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getMethodId", array(), "method"), "html", null, true);
            echo ", \$event)\">
        <div>
          <span class=\"fa fa-check\"></span>
        </div>
      </a>
      <span class=\"active\">";
            // line 31
            echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('t')->getCallable(), array("checkbox.onoff.on")), "html", null, true);
            echo "</span>
    </div>
    ";
        } else {
            // line 34
            echo "    <div class=\"button\">
      ";
            // line 35
            echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('widget')->getCallable(), array($this->env, $context, array(0 => "\\XLite\\Module\\CDev\\Paypal\\View\\Button\\OnboardingSignup", "label" => call_user_func_array($this->env->getFunction('t')->getCallable(), array("Launch (Paypal Onboarding)"))))), "html", null, true);
            echo "
    </div>
    ";
        }
        // line 38
        echo "  </div>
</xlite-onboarding-paypal-card>";
    }

    public function getTemplateName()
    {
        return "modules/CDev/Paypal/onboarding/body.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  88 => 38,  82 => 35,  79 => 34,  73 => 31,  65 => 26,  61 => 25,  54 => 24,  52 => 23,  46 => 20,  39 => 16,  32 => 12,  25 => 8,  19 => 4,);
    }
}
/* {##*/
/*  # Onboarding wizard online payment method block*/
/*  #}*/
/* */
/* <xlite-onboarding-paypal-card inline-template>*/
/*   <div class="online online-paypal" :class="classes">*/
/*     <div class="method-type">*/
/*       {{ t('Online method') }}*/
/*     </div>*/
/* */
/*     <div class="image">*/
/*       <img src="{{ asset('modules/CDev/Paypal/onboarding/logo.png') }}">*/
/*     </div>*/
/* */
/*     <div class="method-name">*/
/*       {{ t('PayPal Express Checkout') }}*/
/*     </div>*/
/* */
/*     <div class="note">*/
/*       {{ t('No merchant account required. Simple onboarding for you and easy checkout for your customers.') }}*/
/*     </div>*/
/* */
/*     {% if this.isPaypalConfigured() %}*/
/*     <div class="switcher{% if this.isPaypalMethodEnabled() %} enabled{% endif %}">*/
/*       <span class="inactive">{{ t('checkbox.onoff.off') }}</span>*/
/*       <a href="#" @click.prevent="switchPaypalMethod({{ this.getMethodId() }}, $event)">*/
/*         <div>*/
/*           <span class="fa fa-check"></span>*/
/*         </div>*/
/*       </a>*/
/*       <span class="active">{{ t('checkbox.onoff.on') }}</span>*/
/*     </div>*/
/*     {% else %}*/
/*     <div class="button">*/
/*       {{ widget('\\XLite\\Module\\CDev\\Paypal\\View\\Button\\OnboardingSignup', label=t('Launch (Paypal Onboarding)')) }}*/
/*     </div>*/
/*     {% endif %}*/
/*   </div>*/
/* </xlite-onboarding-paypal-card>*/
