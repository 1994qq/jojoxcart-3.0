<?php

/* items_list/module/manage/parts/columns/module-main-section/actions/main.twig */
class __TwigTemplate_cd1006d1706d7bf15a8ed69c63da7a24a50af7134275590a0fd0d7fc3cb14cdd extends \XLite\Core\Templating\Twig\Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 4
        if (($this->getAttribute((isset($context["this"]) ? $context["this"] : null), "isDisabledHard", array(), "method") || $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "isEnabledHard", array(), "method"))) {
            // line 5
            echo "
  <div class=\"switcher disabled\">
    ";
            // line 7
            echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('widget')->getCallable(), array($this->env, $context, array(0 => "\\XLite\\View\\Tooltip", "isImageTag" => "true", "className" => "help-icon", "imageClass" => "warning fa fa-exclamation-circle", "helpId" => sprintf("moduleMessages%d", $this->getAttribute($this->getAttribute((isset($context["this"]) ? $context["this"] : null), "module", array()), "getModuleId", array(), "method")), "delay" => 800))), "html", null, true);
            echo "
  </div>

";
        } else {
            // line 11
            echo "
  ";
            // line 12
            echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('widget')->getCallable(), array($this->env, $context, array(0 => "XLite\\View\\FormField\\Input\\Checkbox\\ModuleSwitcher", "fieldId" => sprintf("switch%d", $this->getAttribute($this->getAttribute(            // line 14
(isset($context["this"]) ? $context["this"] : null), "module", array()), "getModuleId", array(), "method")), "fieldName" => sprintf("switch[%d][new]", $this->getAttribute($this->getAttribute(            // line 15
(isset($context["this"]) ? $context["this"] : null), "module", array()), "getModuleId", array(), "method")), "fieldOnly" => true, "value" => $this->getAttribute($this->getAttribute(            // line 17
(isset($context["this"]) ? $context["this"] : null), "module", array()), "getEnabled", array(), "method"), "moduleId" => $this->getAttribute($this->getAttribute(            // line 18
(isset($context["this"]) ? $context["this"] : null), "module", array()), "getModuleId", array(), "method"), "attributes" => $this->getAttribute(            // line 19
(isset($context["this"]) ? $context["this"] : null), "getFieldAttributes", array(0 => $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "module", array())), "method"), "isReadOnly" => $this->getAttribute(            // line 20
(isset($context["this"]) ? $context["this"] : null), "isFieldDisabled", array(0 => $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "module", array())), "method"), "helpId" => sprintf("moduleMessages%d", $this->getAttribute($this->getAttribute(            // line 21
(isset($context["this"]) ? $context["this"] : null), "module", array()), "getModuleId", array(), "method"))))), "html", null, true);
            // line 22
            echo "

";
        }
    }

    public function getTemplateName()
    {
        return "items_list/module/manage/parts/columns/module-main-section/actions/main.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  44 => 22,  42 => 21,  41 => 20,  40 => 19,  39 => 18,  38 => 17,  37 => 15,  36 => 14,  35 => 12,  32 => 11,  25 => 7,  21 => 5,  19 => 4,);
    }
}
/* {##*/
/*  # Modules actions list*/
/*  #}*/
/* {% if this.isDisabledHard() or this.isEnabledHard() %}*/
/* */
/*   <div class="switcher disabled">*/
/*     {{ widget('\\XLite\\View\\Tooltip', isImageTag='true', className='help-icon', imageClass='warning fa fa-exclamation-circle', helpId='moduleMessages%d'|format(this.module.getModuleId()), delay=800) }}*/
/*   </div>*/
/* */
/* {% else %}*/
/* */
/*   {{ widget(*/
/*   'XLite\\View\\FormField\\Input\\Checkbox\\ModuleSwitcher',*/
/*   fieldId='switch%d'|format(this.module.getModuleId()),*/
/*   fieldName='switch[%d][new]'|format(this.module.getModuleId()),*/
/*   fieldOnly=true,*/
/*   value=this.module.getEnabled(),*/
/*   moduleId=this.module.getModuleId(),*/
/*   attributes=this.getFieldAttributes(this.module),*/
/*   isReadOnly=this.isFieldDisabled(this.module),*/
/*   helpId='moduleMessages%d'|format(this.module.getModuleId())*/
/*   ) }}*/
/* */
/* {% endif %}*/
/* */
