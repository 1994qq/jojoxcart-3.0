<?php

/* /var/www/html/xcart/skins/customer/signin/signin_title.twig */
class __TwigTemplate_feb3768f995f9ee0b2c62d5485484e963f92c6e3f7c8c7b8b862ffb0638799a0 extends \XLite\Core\Templating\Twig\Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 6
        echo "<div class=\"signin-anonymous-title-description\">";
        echo $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getSigninAnonymousTitle", array(), "method");
        echo "</div>
";
    }

    public function getTemplateName()
    {
        return "/var/www/html/xcart/skins/customer/signin/signin_title.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  19 => 6,);
    }
}
/* {##*/
/*  # Sign-in*/
/*  #*/
/*  # @ListChild (list="signin-anonymous-title", weight="20")*/
/*  #}*/
/* <div class="signin-anonymous-title-description">{{ this.getSigninAnonymousTitle()|raw }}</div>*/
/* */
