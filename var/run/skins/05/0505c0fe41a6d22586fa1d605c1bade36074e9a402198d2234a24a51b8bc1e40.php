<?php

/* /var/www/html/xcart/skins/customer/product/search/parts/advanced.options.descr.twig */
class __TwigTemplate_d132d41026b53bb7f4bdb848597f0132dd3cf730441667fa3139855ba83a401f extends \XLite\Core\Templating\Twig\Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 6
        echo "
<li><label for=\"by-descr\">
  <input type=\"checkbox\" name=\"by_descr\" id=\"by-descr\" value=\"Y\" ";
        // line 8
        if ($this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getChecked", array(0 => "by_descr"), "method")) {
            echo " checked=\"checked\" ";
        }
        echo " />
  ";
        // line 9
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('t')->getCallable(), array("Description")), "html", null, true);
        echo "
</label></li>
";
    }

    public function getTemplateName()
    {
        return "/var/www/html/xcart/skins/customer/product/search/parts/advanced.options.descr.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  29 => 9,  23 => 8,  19 => 6,);
    }
}
/* {##*/
/*  # Search in description*/
/*  #*/
/*  # @listChild (list="products.search.conditions.advanced.options", weight="200")*/
/*  #}*/
/* */
/* <li><label for="by-descr">*/
/*   <input type="checkbox" name="by_descr" id="by-descr" value="Y" {% if this.getChecked('by_descr') %} checked="checked" {% endif %} />*/
/*   {{ t('Description') }}*/
/* </label></li>*/
/* */
