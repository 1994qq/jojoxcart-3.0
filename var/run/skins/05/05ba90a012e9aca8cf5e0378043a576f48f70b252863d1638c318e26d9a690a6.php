<?php

/* file_uploader/body.twig */
class __TwigTemplate_15a5b180c9a8b98b719ef04c832b75a4d8232292be9fa243117a2311d8dede19 extends \XLite\Core\Templating\Twig\Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 4
        echo "
<xlite-file-uploader inline-template ";
        // line 5
        if ($this->getAttribute((isset($context["this"]) ? $context["this"] : null), "hasMultipleSelector", array(), "method")) {
            echo ":multiple=\"true\"";
        }
        // line 6
        echo "                     help-message=\"";
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('t')->getCallable(), array($this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getHelp", array(), "method"))), "html", null, true);
        echo "\">
  <div class=\"";
        // line 7
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getDivStyle", array(), "method"), "html", null, true);
        echo "\" data-object-id=\"";
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getObjectId", array(), "method"), "html", null, true);
        echo "\"
       v-data='{ \"basePath\": \"";
        // line 8
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getVModel", array(), "method"), "html", null, true);
        echo "\",
       \"isRemovable\": \"";
        // line 9
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "isRemovable", array(), "method"), "html", null, true);
        echo "\",
       \"isTemporary\": \"";
        // line 10
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "isTemporary", array(), "method"), "html", null, true);
        echo "\",
       \"isImage\": \"";
        // line 11
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "isImage", array(), "method"), "html", null, true);
        echo "\",
       \"hasFile\": \"";
        // line 12
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "hasFile", array(), "method"), "html", null, true);
        echo "\",
       \"error\": \"";
        // line 13
        echo (($this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getMessage", array(), "method")) ? ("1") : (""));
        echo "\",
       \"defaultErrorMessage\": \"";
        // line 14
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('t')->getCallable(), array($this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getErrorMessageDefault", array(), "method"))), "html", null, true);
        echo "\",
       \"realErrorMessage\": \"";
        // line 15
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('t')->getCallable(), array($this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getMessage", array(), "method"))), "html", null, true);
        echo "\"}'>
      <input type=\"checkbox\" name=\"";
        // line 16
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getName", array(), "method"), "html", null, true);
        echo "[delete]\" v-model=\"delete\" value=\"1\"
             class=\"input-delete\"
             v-if=\"isRemovable\"
             v-data='{ \"delete\": false }'/>
    ";
        // line 20
        if ($this->getAttribute((isset($context["this"]) ? $context["this"] : null), "isMultiple", array(), "method")) {
            // line 21
            echo "      <input type=\"hidden\" name=\"";
            echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getName", array(), "method"), "html", null, true);
            echo "[position]\" v-model=\"position\"
             value=\"";
            // line 22
            echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getPosition", array(), "method"), "html", null, true);
            echo "\"
             class=\"input-position\"/>
    ";
        }
        // line 25
        echo "    ";
        if ($this->getAttribute((isset($context["this"]) ? $context["this"] : null), "isTemporary", array(), "method")) {
            // line 26
            echo "      <input type=\"hidden\" name=\"";
            echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getName", array(), "method"), "html", null, true);
            echo "[temp_id]\" v-model=\"temp_id\"
             value=\"";
            // line 27
            echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["this"]) ? $context["this"] : null), "object", array()), "id", array()), "html", null, true);
            echo "\"
             v-if=\"isTemporary\"
             class=\"input-temp-id\"/>
    ";
        }
        // line 31
        echo "    <a href=\"";
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getLink", array(), "method"), "html", null, true);
        echo "\" class=\"link\" data-toggle=\"dropdown\">
      <i class=\"icon fa fa-camera\" v-if=\"isDisplayCamera\"></i>
      <i class=\"icon fa warning fa-exclamation-triangle\" v-if=\"errorMessage\"></i>
      <div class=\"preview\" v-if=\"isDisplayPreview\">
        ";
        // line 35
        echo $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getPreview", array(), "method");
        echo "
      </div>
      <div :class=\"error ? 'error' : 'help'\" v-html=\"message\" v-if=\"shouldShowMessage\"></div>
      <div class=\"icon\">
        <i class=\"";
        // line 39
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getIconStyle", array(), "method"), "html", null, true);
        echo "\"></i>
      </div>
    </a>
    ";
        // line 42
        if ($this->getAttribute((isset($context["this"]) ? $context["this"] : null), "hasAlt", array(), "method")) {
            // line 43
            echo "      <div class=\"alt\">
        <div class=\"dropdown-toggle\" data-toggle=\"dropdown\">
          <span>[alt]</span>
        </div>
        <div class=\"dropdown-menu\" role=\"menu\">
          <input name=\"";
            // line 48
            echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getName", array(), "method"), "html", null, true);
            echo "[alt]\"
                 v-model=\"alt\"
                 value=\"";
            // line 50
            echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["this"]) ? $context["this"] : null), "object", array()), "alt", array()), "html", null, true);
            echo "\"
                 class=\"form-control input-alt\"
                 @change=\"doChangeAlt\"
                 @keydown=\"doChangeAlt\"
                 @blur=\"doChangeAlt\"/>
        </div>
      </div>
    ";
        }
        // line 58
        echo "    <ul class=\"dropdown-menu\" role=\"menu\">
      ";
        // line 59
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('widget_list')->getCallable(), array($this->env, $context, array(0 => "file-uploader.menu"))), "html", null, true);
        echo "
    </ul>
  </div>
</xlite-file-uploader>
";
    }

    public function getTemplateName()
    {
        return "file_uploader/body.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  153 => 59,  150 => 58,  139 => 50,  134 => 48,  127 => 43,  125 => 42,  119 => 39,  112 => 35,  104 => 31,  97 => 27,  92 => 26,  89 => 25,  83 => 22,  78 => 21,  76 => 20,  69 => 16,  65 => 15,  61 => 14,  57 => 13,  53 => 12,  49 => 11,  45 => 10,  41 => 9,  37 => 8,  31 => 7,  26 => 6,  22 => 5,  19 => 4,);
    }
}
/* {##*/
/*  # File upload template*/
/*  #}*/
/* */
/* <xlite-file-uploader inline-template {% if this.hasMultipleSelector() %}:multiple="true"{% endif %}*/
/*                      help-message="{{ t(this.getHelp()) }}">*/
/*   <div class="{{ this.getDivStyle() }}" data-object-id="{{ this.getObjectId() }}"*/
/*        v-data='{ "basePath": "{{ this.getVModel() }}",*/
/*        "isRemovable": "{{ this.isRemovable() }}",*/
/*        "isTemporary": "{{ this.isTemporary() }}",*/
/*        "isImage": "{{ this.isImage() }}",*/
/*        "hasFile": "{{ this.hasFile() }}",*/
/*        "error": "{{ this.getMessage() ? '1' : '' }}",*/
/*        "defaultErrorMessage": "{{ t(this.getErrorMessageDefault()) }}",*/
/*        "realErrorMessage": "{{ t(this.getMessage()) }}"}'>*/
/*       <input type="checkbox" name="{{ this.getName() }}[delete]" v-model="delete" value="1"*/
/*              class="input-delete"*/
/*              v-if="isRemovable"*/
/*              v-data='{ "delete": false }'/>*/
/*     {% if this.isMultiple() %}*/
/*       <input type="hidden" name="{{ this.getName() }}[position]" v-model="position"*/
/*              value="{{ this.getPosition() }}"*/
/*              class="input-position"/>*/
/*     {% endif %}*/
/*     {% if this.isTemporary() %}*/
/*       <input type="hidden" name="{{ this.getName() }}[temp_id]" v-model="temp_id"*/
/*              value="{{ this.object.id }}"*/
/*              v-if="isTemporary"*/
/*              class="input-temp-id"/>*/
/*     {% endif %}*/
/*     <a href="{{ this.getLink() }}" class="link" data-toggle="dropdown">*/
/*       <i class="icon fa fa-camera" v-if="isDisplayCamera"></i>*/
/*       <i class="icon fa warning fa-exclamation-triangle" v-if="errorMessage"></i>*/
/*       <div class="preview" v-if="isDisplayPreview">*/
/*         {{ this.getPreview()|raw }}*/
/*       </div>*/
/*       <div :class="error ? 'error' : 'help'" v-html="message" v-if="shouldShowMessage"></div>*/
/*       <div class="icon">*/
/*         <i class="{{ this.getIconStyle() }}"></i>*/
/*       </div>*/
/*     </a>*/
/*     {% if this.hasAlt() %}*/
/*       <div class="alt">*/
/*         <div class="dropdown-toggle" data-toggle="dropdown">*/
/*           <span>[alt]</span>*/
/*         </div>*/
/*         <div class="dropdown-menu" role="menu">*/
/*           <input name="{{ this.getName() }}[alt]"*/
/*                  v-model="alt"*/
/*                  value="{{ this.object.alt }}"*/
/*                  class="form-control input-alt"*/
/*                  @change="doChangeAlt"*/
/*                  @keydown="doChangeAlt"*/
/*                  @blur="doChangeAlt"/>*/
/*         </div>*/
/*       </div>*/
/*     {% endif %}*/
/*     <ul class="dropdown-menu" role="menu">*/
/*       {{ widget_list('file-uploader.menu') }}*/
/*     </ul>*/
/*   </div>*/
/* </xlite-file-uploader>*/
/* */
