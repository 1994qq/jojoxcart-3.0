<?php

/* modules/XC/Onboarding/form_model/type/template.twig */
class __TwigTemplate_a58225dda37db3536334577a8588bd1358634b7fa7912ce326e42cfc9ce1f2fe extends \XLite\Core\Templating\Twig\Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'xc_onboarding_template_widget' => array($this, 'block_xc_onboarding_template_widget'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $this->displayBlock('xc_onboarding_template_widget', $context, $blocks);
    }

    public function block_xc_onboarding_template_widget($context, array $blocks = array())
    {
        $fullPath = \XLite\Core\Layout::getInstance()->getResourceFullPath(        // line 2
(isset($context["template"]) ? $context["template"] : null));        list($templateWrapperText, $templateWrapperStart) = $this->getThis()->startMarker($fullPath);
        if ($templateWrapperText) {
echo $templateWrapperStart;
}

        $this->loadTemplate((isset($context["template"]) ? $context["template"] : null), "modules/XC/Onboarding/form_model/type/template.twig", 2)->display($context);
        if ($templateWrapperText) {
            echo $this->getThis()->endMarker($fullPath, $templateWrapperText);
        }
    }

    public function getTemplateName()
    {
        return "modules/XC/Onboarding/form_model/type/template.twig";
    }

    public function getDebugInfo()
    {
        return array (  26 => 2,  20 => 1,);
    }
}
/* {% block xc_onboarding_template_widget -%}*/
/*   {% include template %}*/
/* {%- endblock xc_onboarding_template_widget %}*/
/* */
