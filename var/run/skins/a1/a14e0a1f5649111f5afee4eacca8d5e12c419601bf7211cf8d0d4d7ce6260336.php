<?php

/* /var/www/html/xcart/skins/customer/header/parts/meta_mobile_capable.twig */
class __TwigTemplate_79892deed6bc3d0379d4a581c30cffed2c02709dc4cbb324a61ef73fb6c5caf0 extends \XLite\Core\Templating\Twig\Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 6
        echo "
<meta name=\"apple-mobile-web-app-capable\"   content=\"yes\" />
<meta name=\"mobile-web-app-capable\"         content=\"yes\" />";
    }

    public function getTemplateName()
    {
        return "/var/www/html/xcart/skins/customer/header/parts/meta_mobile_capable.twig";
    }

    public function getDebugInfo()
    {
        return array (  19 => 6,);
    }
}
/* {##*/
/*  # Head list children*/
/*  #*/
/*  # @ListChild (list="head", weight="700")*/
/*  #}*/
/* */
/* <meta name="apple-mobile-web-app-capable"   content="yes" />*/
/* <meta name="mobile-web-app-capable"         content="yes" />*/
