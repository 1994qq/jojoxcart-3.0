<?php

/* left_menu/extensions/body.twig */
class __TwigTemplate_5572396b361ee0016abfd17b3e9a2b53c80cde5529d6dadcfe9780c24a8d3ba2 extends \XLite\Core\Templating\Twig\Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 4
        echo "<a ";
        echo $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "printTagAttributes", array(0 => $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getAttributes", array(), "method")), "method");
        echo ">
  ";
        // line 5
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "displayCommentedData", array(0 => $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getURLParams", array(), "method")), "method"), "html", null, true);
        echo "
  ";
        // line 6
        echo $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getSVGImage", array(0 => "images/plus.svg"), "method");
        echo "
</a>
";
    }

    public function getTemplateName()
    {
        return "left_menu/extensions/body.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  28 => 6,  24 => 5,  19 => 4,);
    }
}
/* {##*/
/*  # Extensions plus button*/
/*  #}*/
/* <a {{ this.printTagAttributes(this.getAttributes())|raw }}>*/
/*   {{ this.displayCommentedData(this.getURLParams()) }}*/
/*   {{ this.getSVGImage('images/plus.svg')|raw }}*/
/* </a>*/
/* */
