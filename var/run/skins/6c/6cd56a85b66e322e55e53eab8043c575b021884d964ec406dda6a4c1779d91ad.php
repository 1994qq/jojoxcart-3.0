<?php

/* modules_manager/manage/body.twig */
class __TwigTemplate_f819af5350a3ebc3faed41f63a919c33bd94c960c8287c83321e7bcef5e456c6 extends \XLite\Core\Templating\Twig\Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 4
        echo "
";
        // line 5
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('widget_list')->getCallable(), array($this->env, $context, array(0 => "modules-manage.top-controls"))), "html", null, true);
        echo "
";
        // line 6
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('widget')->getCallable(), array($this->env, $context, array(0 => "\\XLite\\View\\ItemsList\\Module\\Manage"))), "html", null, true);
        echo "
";
    }

    public function getTemplateName()
    {
        return "modules_manager/manage/body.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  26 => 6,  22 => 5,  19 => 4,);
    }
}
/* {##*/
/*  # Modules*/
/*  #}*/
/* */
/* {{ widget_list('modules-manage.top-controls') }}*/
/* {{ widget('\\XLite\\View\\ItemsList\\Module\\Manage') }}*/
/* */
