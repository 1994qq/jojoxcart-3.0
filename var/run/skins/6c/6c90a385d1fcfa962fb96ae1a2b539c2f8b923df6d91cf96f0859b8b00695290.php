<?php

/* /var/www/html/xcart/skins/admin/shipping/methods.status.twig */
class __TwigTemplate_00172c75c30a27d9de293df117a95e2c23a57e0c94ce4a0a4ed839fbf5af78bf extends \XLite\Core\Templating\Twig\Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 6
        echo "
";
        // line 7
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('widget')->getCallable(), array($this->env, $context, array(0 => "XLite\\View\\Shipping\\CarrierStatus", "method" => $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getMethod", array(), "method")))), "html", null, true);
        echo "
";
    }

    public function getTemplateName()
    {
        return "/var/www/html/xcart/skins/admin/shipping/methods.status.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  22 => 7,  19 => 6,);
    }
}
/* {##*/
/*  # Shipping methods management page main template*/
/*  #*/
/*  # @ListChild (list="itemsList.shipping-methods.header", weight="10")*/
/*  #}*/
/* */
/* {{ widget('XLite\\View\\Shipping\\CarrierStatus', method=this.getMethod()) }}*/
/* */
