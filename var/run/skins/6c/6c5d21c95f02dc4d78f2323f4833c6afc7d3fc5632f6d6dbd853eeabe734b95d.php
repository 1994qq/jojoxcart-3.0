<?php

/* /var/www/html/xcart/skins/customer/modules/XC/FastLaneCheckout/sections/details/order_total.twig */
class __TwigTemplate_40205d355afe5781f1e2e4ca688c5de3a4a9a0ef6a9ebdfac0da6d9a238f44c1 extends \XLite\Core\Templating\Twig\Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 6
        echo "
<order-total inline-template>
    <div class=\"order-total\" v-text='total_text'></div>
</order-total>";
    }

    public function getTemplateName()
    {
        return "/var/www/html/xcart/skins/customer/modules/XC/FastLaneCheckout/sections/details/order_total.twig";
    }

    public function getDebugInfo()
    {
        return array (  19 => 6,);
    }
}
/* {##*/
/*  # Fastlane checkout payment right section*/
/*  #*/
/*  # @ListChild (list="checkout_fastlane.sections.details.right", weight="19")*/
/*  #}*/
/* */
/* <order-total inline-template>*/
/*     <div class="order-total" v-text='total_text'></div>*/
/* </order-total>*/
