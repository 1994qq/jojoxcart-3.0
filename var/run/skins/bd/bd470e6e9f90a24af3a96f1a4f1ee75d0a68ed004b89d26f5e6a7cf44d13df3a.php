<?php

/* /var/www/html/xcart/skins/customer/items_list/product/parts/table.captions.add2cart-button.twig */
class __TwigTemplate_216942e3b0a2d4e90a9bfd617633d5a004b0281cada2a8a1cc212875446a61c4 extends \XLite\Core\Templating\Twig\Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 6
        echo "<th class=\"caption-product-add2cart\">&nbsp;</th>
";
    }

    public function getTemplateName()
    {
        return "/var/www/html/xcart/skins/customer/items_list/product/parts/table.captions.add2cart-button.twig";
    }

    public function getDebugInfo()
    {
        return array (  19 => 6,);
    }
}
/* {##*/
/*  # "Add to cart" column title*/
/*  #*/
/*  # @ListChild (list="itemsList.product.table.customer.captions", weight="50")*/
/*  #}*/
/* <th class="caption-product-add2cart">&nbsp;</th>*/
/* */
