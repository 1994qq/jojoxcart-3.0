<?php

/* /var/www/html/xcart/skins/crisp_white/customer/layout/header/header.right.mobile.minicart.twig */
class __TwigTemplate_ef9dd70ee4b80398cd44f89d46d0bd88242dd17530a646c8445ead8f15e12b97 extends \XLite\Core\Templating\Twig\Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 6
        echo "
<div class=\"lc-minicart-placeholder\"></div>
";
    }

    public function getTemplateName()
    {
        return "/var/www/html/xcart/skins/crisp_white/customer/layout/header/header.right.mobile.minicart.twig";
    }

    public function getDebugInfo()
    {
        return array (  19 => 6,);
    }
}
/* {##*/
/*  # Header bar search box*/
/*  #*/
/*  # @ListChild (list="layout.header.right.mobile", weight="100")*/
/*  #}*/
/* */
/* <div class="lc-minicart-placeholder"></div>*/
/* */
