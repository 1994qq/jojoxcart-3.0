<?php

/* /var/www/html/xcart/skins/admin/modules/Amazon/PayWithAmazon/method_style.twig */
class __TwigTemplate_70acd429e291948d4b898922692af45f7e58d6a7a5c1344e059d2ddb6b8c52cd extends \XLite\Core\Templating\Twig\Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 4
        echo "
<style type=\"text/css\">
.module-name-Amazon_PayWithAmazon .payment-logo img {
    margin-top: 7px;
}
</style>";
    }

    public function getTemplateName()
    {
        return "/var/www/html/xcart/skins/admin/modules/Amazon/PayWithAmazon/method_style.twig";
    }

    public function getDebugInfo()
    {
        return array (  19 => 4,);
    }
}
/* {##*/
/*  # @ListChild (list="payment.methods.list.header", weight=9999)*/
/*  #}*/
/* */
/* <style type="text/css">*/
/* .module-name-Amazon_PayWithAmazon .payment-logo img {*/
/*     margin-top: 7px;*/
/* }*/
/* </style>*/
