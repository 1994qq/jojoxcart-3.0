<?php

/* /var/www/html/xcart/skins/customer/header/parts/meta_compat_ie.twig */
class __TwigTemplate_0d6110b5b1af4578be7bb78508194055773e8c89a8e99fed60bc86047161d87b extends \XLite\Core\Templating\Twig\Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 6
        echo "
<meta http-equiv=\"X-UA-Compatible\" content=\"IE=Edge\" />
";
    }

    public function getTemplateName()
    {
        return "/var/www/html/xcart/skins/customer/header/parts/meta_compat_ie.twig";
    }

    public function getDebugInfo()
    {
        return array (  19 => 6,);
    }
}
/* {##*/
/*  # Head list children*/
/*  #*/
/*  # @ListChild (list="head", weight="800")*/
/*  #}*/
/* */
/* <meta http-equiv="X-UA-Compatible" content="IE=Edge" />*/
/* */
