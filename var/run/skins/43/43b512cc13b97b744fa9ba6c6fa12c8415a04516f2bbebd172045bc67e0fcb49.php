<?php

/* upgrade/step/prepare/entries_list_update/body.twig */
class __TwigTemplate_a956a8e050b834f7386f279d3bbcfdb19a52e846e16dbd1f159ba11ffbc5f1ec extends \XLite\Core\Templating\Twig\Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 4
        echo "
";
        // line 5
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('widget_list')->getCallable(), array($this->env, $context, array(0 => "sections", "type" => "inherited"))), "html", null, true);
        echo "
";
    }

    public function getTemplateName()
    {
        return "upgrade/step/prepare/entries_list_update/body.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  22 => 5,  19 => 4,);
    }
}
/* {##*/
/*  # List of upgrade cell entries*/
/*  #}*/
/* */
/* {{ widget_list('sections', type='inherited') }}*/
/* */
