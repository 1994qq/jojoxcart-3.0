<?php

/* /var/www/html/xcart/skins/admin/modules/CDev/Paypal/welcome_block/paypal/block.content.twig */
class __TwigTemplate_753fc0b9ed2599b6a19f1c915bc6ebb5c7b4805d2c0c02e12c9029003113cafb extends \XLite\Core\Templating\Twig\Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 6
        echo "<div class=\"content\">
  <div class=\"info\">

    ";
        // line 9
        echo call_user_func_array($this->env->getFunction('t')->getCallable(), array("paypal_welcome_text", array("email" => $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getPaypalEmail", array(), "method"))));
        echo "

    <div class=\"action\">
      ";
        // line 12
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('widget')->getCallable(), array($this->env, $context, array(0 => "\\XLite\\Module\\CDev\\Paypal\\View\\Button\\SignUp", "label" => call_user_func_array($this->env->getFunction('t')->getCallable(), array("Launch PayPal"))))), "html", null, true);
        echo "
    </div>

  </div>
</div>";
    }

    public function getTemplateName()
    {
        return "/var/www/html/xcart/skins/admin/modules/CDev/Paypal/welcome_block/paypal/block.content.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  30 => 12,  24 => 9,  19 => 6,);
    }
}
/* {##*/
/*  # Block content : items*/
/*  #*/
/*  # @ListChild (list="welcome-block.paypal.content", weight="10")*/
/*  #}*/
/* <div class="content">*/
/*   <div class="info">*/
/* */
/*     {{ t('paypal_welcome_text', {'email': this.getPaypalEmail()})|raw }}*/
/* */
/*     <div class="action">*/
/*       {{ widget('\\XLite\\Module\\CDev\\Paypal\\View\\Button\\SignUp', label=t('Launch PayPal')) }}*/
/*     </div>*/
/* */
/*   </div>*/
/* </div>*/
