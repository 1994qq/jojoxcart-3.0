<?php

/* button/addon/purchase.twig */
class __TwigTemplate_c56df1d9a4d56ef5b0bd8d975780772eec882dbd0838747e30d77104bd37b465 extends \XLite\Core\Templating\Twig\Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 4
        echo "
<button
  type=\"button\"
  ";
        // line 7
        echo $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getJSCode", array(), "method");
        echo "
  class=\"";
        // line 8
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getClass", array(), "method"), "html", null, true);
        echo "\">
  <span>";
        // line 9
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('t')->getCallable(), array($this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getButtonLabel", array(), "method"))), "html", null, true);
        echo "</span>
</button>
";
    }

    public function getTemplateName()
    {
        return "button/addon/purchase.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  32 => 9,  28 => 8,  24 => 7,  19 => 4,);
    }
}
/* {##*/
/*  # Regular button*/
/*  #}*/
/* */
/* <button*/
/*   type="button"*/
/*   {{ this.getJSCode()|raw }}*/
/*   class="{{ this.getClass() }}">*/
/*   <span>{{ t(this.getButtonLabel()) }}</span>*/
/* </button>*/
/* */
