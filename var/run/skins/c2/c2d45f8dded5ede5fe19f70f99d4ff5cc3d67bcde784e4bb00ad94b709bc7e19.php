<?php

/* modules/XC/Onboarding/wizard_steps/intro/body.twig */
class __TwigTemplate_5058a047f783b6ff14de2175c3d7dfc846e0e78d64c8b6ec4fafdd076128a532 extends \XLite\Core\Templating\Twig\Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 4
        echo "
<div
  class=\"onboarding-wizard-step step-";
        // line 6
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getStepIndex", array(), "method"), "html", null, true);
        echo "\"
  v-show=\"isCurrentStep('";
        // line 7
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getStepIndex", array(), "method"), "html", null, true);
        echo "')\"
  :transition=\"stepTransition\">
  <div class=\"step-contents\">
    <div class=\"bottom-sticky\" v-if=\"isCurrentStep('";
        // line 10
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getStepIndex", array(), "method"), "html", null, true);
        echo "')\" transition=\"fade-in-out\">
      ";
        // line 11
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('widget')->getCallable(), array($this->env, $context, array(0 => "XLite\\View\\Button\\Regular", "label" => call_user_func_array($this->env->getFunction('t')->getCallable(), array("onboarding.intro.next_button")), "style" => "regular-main-button", "attributes" => array("@click" => "goToNextStep"), "jsCode" => "null;"))), "html", null, true);
        echo "
      <div class=\"additional-buttons\">
        ";
        // line 13
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('widget')->getCallable(), array($this->env, $context, array(0 => "XLite\\View\\Button\\SimpleLink", "label" => call_user_func_array($this->env->getFunction('t')->getCallable(), array("onboarding.intro.later_button")), "attributes" => array("@click" => "hideWizard"), "jsCode" => "null;"))), "html", null, true);
        echo "
        ";
        // line 14
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('widget')->getCallable(), array($this->env, $context, array(0 => "XLite\\View\\Button\\SimpleLink", "label" => call_user_func_array($this->env->getFunction('t')->getCallable(), array("onboarding.intro.disable_button")), "attributes" => array("@click" => "disableWizard"), "jsCode" => "null;"))), "html", null, true);
        echo "
      </div>
    </div>
  </div>
</div>";
    }

    public function getTemplateName()
    {
        return "modules/XC/Onboarding/wizard_steps/intro/body.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  46 => 14,  42 => 13,  37 => 11,  33 => 10,  27 => 7,  23 => 6,  19 => 4,);
    }
}
/* {##*/
/*  # Intro step*/
/*  #}*/
/* */
/* <div*/
/*   class="onboarding-wizard-step step-{{ this.getStepIndex() }}"*/
/*   v-show="isCurrentStep('{{ this.getStepIndex() }}')"*/
/*   :transition="stepTransition">*/
/*   <div class="step-contents">*/
/*     <div class="bottom-sticky" v-if="isCurrentStep('{{ this.getStepIndex() }}')" transition="fade-in-out">*/
/*       {{ widget('XLite\\View\\Button\\Regular', label=t("onboarding.intro.next_button"), style='regular-main-button', attributes={'@click': 'goToNextStep'}, jsCode="null;") }}*/
/*       <div class="additional-buttons">*/
/*         {{ widget('XLite\\View\\Button\\SimpleLink', label=t("onboarding.intro.later_button"), attributes={'@click': 'hideWizard'}, jsCode="null;") }}*/
/*         {{ widget('XLite\\View\\Button\\SimpleLink', label=t("onboarding.intro.disable_button"), attributes={'@click': 'disableWizard'}, jsCode="null;") }}*/
/*       </div>*/
/*     </div>*/
/*   </div>*/
/* </div>*/
