<?php

/* left_menu/link.twig */
class __TwigTemplate_95ee5f535a3cb2387e2798a1361ffac245aa68e0f5e7b8d7d90b3e61f3433534 extends \XLite\Core\Templating\Twig\Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 4
        echo "
<div class=\"line";
        // line 5
        if ($this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getLabel", array(), "method")) {
            echo " with-label";
        }
        echo "\">
  ";
        // line 6
        if ($this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getLink", array(), "method")) {
            // line 7
            echo "    <a href=\"";
            echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getLink", array(), "method"), "html", null, true);
            echo "\" class=\"link\"";
            if ($this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getTooltip", array(), "method")) {
                echo " title=\"";
                echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getTooltip", array(), "method"), "html", null, true);
                echo "\"";
            }
            if ($this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getBlankPage", array(), "method")) {
                echo " target=\"_blank\"";
            }
            echo "><span class=\"icon\">";
            echo $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getIcon", array(), "method");
            echo "</span>";
            if ($this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getTitle", array(), "method")) {
                echo "<span class=\"title\">";
                echo $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getTitle", array(), "method");
                echo "</span>";
            }
            echo "</a>
  ";
        } else {
            // line 9
            echo "    <span class=\"link\"";
            if ($this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getTooltip", array(), "method")) {
                echo " title=\"";
                echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getTooltip", array(), "method"), "html", null, true);
                echo "\"";
            }
            echo "><span class=\"icon\">";
            echo $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getIcon", array(), "method");
            echo "</span>";
            if ($this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getTitle", array(), "method")) {
                echo "<span class=\"title\">";
                echo $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getTitle", array(), "method");
                echo "</span>";
            }
            echo "</span>
  ";
        }
        // line 11
        echo "  ";
        if ($this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getLabel", array(), "method")) {
            // line 12
            echo "    <a href=\"";
            echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getLabelLink", array(), "method"), "html", null, true);
            echo "\" class=\"label\"><span title=\"";
            echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getLabelTitle", array(), "method"), "html", null, true);
            echo "\">";
            echo $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getLabel", array(), "method");
            echo "</span></a>
  ";
        }
        // line 14
        echo "  ";
        if ($this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getActionWidget", array(), "method")) {
            // line 15
            echo "    <div class=\"action-widget\">";
            echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getActionWidget", array(), "method"), "display", array(), "method"), "html", null, true);
            echo "</div>
  ";
        }
        // line 17
        echo "</div>
";
    }

    public function getTemplateName()
    {
        return "left_menu/link.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  93 => 17,  87 => 15,  84 => 14,  74 => 12,  71 => 11,  53 => 9,  30 => 7,  28 => 6,  22 => 5,  19 => 4,);
    }
}
/* {##*/
/*  # Left side menu link*/
/*  #}*/
/* */
/* <div class="line{% if this.getLabel() %} with-label{% endif %}">*/
/*   {% if this.getLink() %}*/
/*     <a href="{{ this.getLink() }}" class="link"{% if this.getTooltip() %} title="{{ this.getTooltip() }}"{% endif %}{% if this.getBlankPage() %} target="_blank"{% endif %}><span class="icon">{{ this.getIcon()|raw }}</span>{% if this.getTitle() %}<span class="title">{{ this.getTitle()|raw }}</span>{% endif %}</a>*/
/*   {% else %}*/
/*     <span class="link"{% if this.getTooltip() %} title="{{ this.getTooltip() }}"{% endif %}><span class="icon">{{ this.getIcon()|raw }}</span>{% if this.getTitle() %}<span class="title">{{ this.getTitle()|raw }}</span>{% endif %}</span>*/
/*   {% endif %}*/
/*   {% if this.getLabel() %}*/
/*     <a href="{{ this.getLabelLink() }}" class="label"><span title="{{ this.getLabelTitle() }}">{{ this.getLabel()|raw }}</span></a>*/
/*   {% endif %}*/
/*   {% if this.getActionWidget() %}*/
/*     <div class="action-widget">{{ this.getActionWidget().display() }}</div>*/
/*   {% endif %}*/
/* </div>*/
/* */
