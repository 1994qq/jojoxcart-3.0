<?php

/* form_field/input/checkbox/module_switcher.twig */
class __TwigTemplate_ae98b80dd4562fffc1f605b3d99ff610defaa7f74b150d09adbdcc5561fcd4c7 extends \XLite\Core\Templating\Twig\Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 4
        echo "<input type=\"hidden\" class=\"old-value\" name=\"";
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getOldName", array(), "method"), "html", null, true);
        echo "\" value=\"";
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getValue", array(), "method"), "html", null, true);
        echo "\" />
";
        $fullPath = \XLite\Core\Layout::getInstance()->getResourceFullPath("form_field/input/checkbox/on_off.twig");        list($templateWrapperText, $templateWrapperStart) = $this->getThis()->startMarker($fullPath);
        if ($templateWrapperText) {
echo $templateWrapperStart;
}

        // line 5
        $this->loadTemplate("form_field/input/checkbox/on_off.twig", "form_field/input/checkbox/module_switcher.twig", 5)->display($context);
        if ($templateWrapperText) {
            echo $this->getThis()->endMarker($fullPath, $templateWrapperText);
        }
    }

    public function getTemplateName()
    {
        return "form_field/input/checkbox/module_switcher.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  31 => 5,  19 => 4,);
    }
}
/* {##*/
/*  # Module switcher*/
/*  #}*/
/* <input type="hidden" class="old-value" name="{{ this.getOldName() }}" value="{{ this.getValue() }}" />*/
/* {% include 'form_field/input/checkbox/on_off.twig' %}*/
/* */
