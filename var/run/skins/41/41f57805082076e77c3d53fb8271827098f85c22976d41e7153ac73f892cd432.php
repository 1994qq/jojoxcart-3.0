<?php

/* modules/XC/Onboarding/wizard_steps/done/body.twig */
class __TwigTemplate_95c2968b69bddd597f253a4cf424ecbeb33658c9fb5931cda894a658206daee4 extends \XLite\Core\Templating\Twig\Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 4
        echo "
<div class=\"onboarding-wizard-step step-";
        // line 5
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getStepIndex", array(), "method"), "html", null, true);
        echo "\"
     v-show=\"isCurrentStep('";
        // line 6
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getStepIndex", array(), "method"), "html", null, true);
        echo "')\"
     :transition=\"stepTransition\">
  <div class=\"step-contents\">
    <h2 class=\"heading\">";
        // line 9
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('t')->getCallable(), array("Excellent! Now you can get paid")), "html", null, true);
        echo "</h2>
    <p class=\"text\">
      ";
        // line 11
        echo call_user_func_array($this->env->getFunction('t')->getCallable(), array("Go on, make your first sale.", array("url" => $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getStorefrontUrl", array(), "method"))));
        echo "<br>
      ";
        // line 12
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('t')->getCallable(), array("Need help setting up your store?")), "html", null, true);
        echo "
    </p>

    <div class=\"links\">
      <div class=\"expert\">
        <div class=\"image\">";
        // line 17
        echo call_user_func_array($this->env->getFunction('svg')->getCallable(), array($this->env, $context, "modules/XC/Onboarding/images/icon-contact-our-expert.svg"));
        echo "</div>
        <div class=\"link\">
          <a href=\"";
        // line 19
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getSupportUrl", array(), "method"), "html", null, true);
        echo "\" target=\"_blank\">";
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('t')->getCallable(), array("Contact our expert")), "html", null, true);
        echo "</a>
        </div>
        <div class=\"note\">
          ";
        // line 22
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('t')->getCallable(), array("X-Cart professionals will easily materialize just about any idea you’ve got.")), "html", null, true);
        echo "
        </div>
      </div>

      <div class=\"knowledge-base\">
        <div class=\"image\">";
        // line 27
        echo call_user_func_array($this->env->getFunction('svg')->getCallable(), array($this->env, $context, "modules/XC/Onboarding/images/icon-knowledge-base.svg"));
        echo "</div>
        <div class=\"link\">
          <a href=\"";
        // line 29
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getKBUrl", array(), "method"), "html", null, true);
        echo "\" target=\"_blank\">";
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('t')->getCallable(), array("Knowledge base")), "html", null, true);
        echo "</a>
        </div>
        <div class=\"note\">
          ";
        // line 32
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('t')->getCallable(), array("User manuals, release notes, modules, videos and more.")), "html", null, true);
        echo "
        </div>
      </div>

      <div class=\"developer-docs\">
        <div class=\"image\">";
        // line 37
        echo call_user_func_array($this->env->getFunction('svg')->getCallable(), array($this->env, $context, "modules/XC/Onboarding/images/icon-developer-docs.svg"));
        echo "</div>
        <div class=\"link\">
          <a href=\"";
        // line 39
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getDevDocsUrl", array(), "method"), "html", null, true);
        echo "\" target=\"_blank\">";
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('t')->getCallable(), array("Developer docs")), "html", null, true);
        echo "</a>
        </div>
        <div class=\"note\">
          ";
        // line 42
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('t')->getCallable(), array("See the tutorials and documentation about X-Cart store customization.")), "html", null, true);
        echo "
        </div>
      </div>
    </div>

    <div class=\"contacts\">
      <div class=\"phone-email text\">
        <div class=\"phone\">
          ";
        // line 50
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('t')->getCallable(), array("Sales & Customer Service: [phone]", array("phone" => $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getPhoneNumber", array(), "method")))), "html", null, true);
        echo "
        </div>
        <div class=\"separator\">/</div>
        <div class=\"email\">
          <a href=\"mailto:";
        // line 54
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getSalesEmail", array(), "method"), "html", null, true);
        echo "\">";
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getSalesEmail", array(), "method"), "html", null, true);
        echo "</a>
        </div>
      </div>
    </div>

    <div class=\"buttons\">
      <div class=\"next-step\">
        ";
        // line 61
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('widget')->getCallable(), array($this->env, $context, array(0 => "\\XLite\\View\\Button\\Link", "label" => "Close wizard", "attributes" => array("@click" => "closeWizard"), "style" => "regular-main-button", "jsCode" => "null;"))), "html", null, true);
        echo "
      </div>
    </div>
  </div>
</div>";
    }

    public function getTemplateName()
    {
        return "modules/XC/Onboarding/wizard_steps/done/body.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  134 => 61,  122 => 54,  115 => 50,  104 => 42,  96 => 39,  91 => 37,  83 => 32,  75 => 29,  70 => 27,  62 => 22,  54 => 19,  49 => 17,  41 => 12,  37 => 11,  32 => 9,  26 => 6,  22 => 5,  19 => 4,);
    }
}
/* {##*/
/*  # Done step*/
/*  #}*/
/* */
/* <div class="onboarding-wizard-step step-{{ this.getStepIndex() }}"*/
/*      v-show="isCurrentStep('{{ this.getStepIndex() }}')"*/
/*      :transition="stepTransition">*/
/*   <div class="step-contents">*/
/*     <h2 class="heading">{{ t("Excellent! Now you can get paid") }}</h2>*/
/*     <p class="text">*/
/*       {{ t("Go on, make your first sale.", {'url': this.getStorefrontUrl() })|raw }}<br>*/
/*       {{ t("Need help setting up your store?") }}*/
/*     </p>*/
/* */
/*     <div class="links">*/
/*       <div class="expert">*/
/*         <div class="image">{{ svg('modules/XC/Onboarding/images/icon-contact-our-expert.svg')|raw  }}</div>*/
/*         <div class="link">*/
/*           <a href="{{ this.getSupportUrl() }}" target="_blank">{{ t('Contact our expert') }}</a>*/
/*         </div>*/
/*         <div class="note">*/
/*           {{ t('X-Cart professionals will easily materialize just about any idea you’ve got.') }}*/
/*         </div>*/
/*       </div>*/
/* */
/*       <div class="knowledge-base">*/
/*         <div class="image">{{ svg('modules/XC/Onboarding/images/icon-knowledge-base.svg')|raw  }}</div>*/
/*         <div class="link">*/
/*           <a href="{{ this.getKBUrl() }}" target="_blank">{{ t('Knowledge base') }}</a>*/
/*         </div>*/
/*         <div class="note">*/
/*           {{ t('User manuals, release notes, modules, videos and more.') }}*/
/*         </div>*/
/*       </div>*/
/* */
/*       <div class="developer-docs">*/
/*         <div class="image">{{ svg('modules/XC/Onboarding/images/icon-developer-docs.svg')|raw  }}</div>*/
/*         <div class="link">*/
/*           <a href="{{ this.getDevDocsUrl() }}" target="_blank">{{ t('Developer docs') }}</a>*/
/*         </div>*/
/*         <div class="note">*/
/*           {{ t('See the tutorials and documentation about X-Cart store customization.') }}*/
/*         </div>*/
/*       </div>*/
/*     </div>*/
/* */
/*     <div class="contacts">*/
/*       <div class="phone-email text">*/
/*         <div class="phone">*/
/*           {{ t('Sales & Customer Service: [phone]', {phone: this.getPhoneNumber()}) }}*/
/*         </div>*/
/*         <div class="separator">/</div>*/
/*         <div class="email">*/
/*           <a href="mailto:{{ this.getSalesEmail() }}">{{ this.getSalesEmail() }}</a>*/
/*         </div>*/
/*       </div>*/
/*     </div>*/
/* */
/*     <div class="buttons">*/
/*       <div class="next-step">*/
/*         {{ widget('\\XLite\\View\\Button\\Link', label='Close wizard', attributes={'@click': 'closeWizard'}, style='regular-main-button', jsCode="null;") }}*/
/*       </div>*/
/*     </div>*/
/*   </div>*/
/* </div>*/
