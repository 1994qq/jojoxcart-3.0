<?php

/* button/remove.twig */
class __TwigTemplate_2b1f293d4049f83a27fab6941e81004672feec5743db1684f7a66e7a7226b339 extends \XLite\Core\Templating\Twig\Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 4
        echo "<div class=\"remove-wrapper ";
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getStyle", array(), "method"), "html", null, true);
        echo "\">
  <button type=\"button\" class=\"";
        // line 5
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getStyle", array(), "method"), "html", null, true);
        echo "\" title=\"";
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('t')->getCallable(), array($this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getButtonLabel", array(), "method"))), "html", null, true);
        echo "\" tabindex=\"-1\">
    ";
        // line 6
        if ($this->getAttribute((isset($context["this"]) ? $context["this"] : null), "isCrossIcon", array(), "method")) {
            // line 7
            echo "      ";
            echo call_user_func_array($this->env->getFunction('svg')->getCallable(), array($this->env, $context, "images/close.svg", "common"));
            echo "
    ";
        } else {
            // line 9
            echo "      <i class=\"fa fa-trash-o\"></i>
    ";
        }
        // line 11
        echo "  </button>
  <input type=\"checkbox\" name=\"";
        // line 12
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getName", array(), "method"), "html", null, true);
        echo "\" value=\"1\" tabindex=\"-1\" />
</div>
";
    }

    public function getTemplateName()
    {
        return "button/remove.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  45 => 12,  42 => 11,  38 => 9,  32 => 7,  30 => 6,  24 => 5,  19 => 4,);
    }
}
/* {##*/
/*  # Remove button*/
/*  #}*/
/* <div class="remove-wrapper {{ this.getStyle() }}">*/
/*   <button type="button" class="{{ this.getStyle() }}" title="{{ t(this.getButtonLabel()) }}" tabindex="-1">*/
/*     {% if this.isCrossIcon() %}*/
/*       {{ svg('images/close.svg', 'common')|raw }}*/
/*     {% else %}*/
/*       <i class="fa fa-trash-o"></i>*/
/*     {% endif %}*/
/*   </button>*/
/*   <input type="checkbox" name="{{ this.getName() }}" value="1" tabindex="-1" />*/
/* </div>*/
/* */
