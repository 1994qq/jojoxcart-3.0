<?php

/* /var/www/html/xcart/skins/common/order/invoice/parts/head.companyAddress.website.twig */
class __TwigTemplate_a85fd8ac5b21390aca37ae2e2a0bb4250fd24c5c6c9d95b605461311b47bcccc extends \XLite\Core\Templating\Twig\Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 6
        echo "
";
        // line 7
        if ($this->getAttribute($this->getAttribute((isset($context["this"]) ? $context["this"] : null), "company", array()), "company_website", array())) {
            // line 8
            echo "<p class=\"url\">
  <a href=\"";
            // line 9
            echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["this"]) ? $context["this"] : null), "company", array()), "company_website", array()), "html", null, true);
            echo "\">";
            echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["this"]) ? $context["this"] : null), "company", array()), "company_website", array()), "html", null, true);
            echo "</a>
</p>
";
        }
    }

    public function getTemplateName()
    {
        return "/var/www/html/xcart/skins/common/order/invoice/parts/head.companyAddress.website.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  27 => 9,  24 => 8,  22 => 7,  19 => 6,);
    }
}
/* {##*/
/*  # Invoice : header : Company address box : website*/
/*  #*/
/*  # @ListChild (list="invoice.subhead.companyAddress", weight="500")*/
/*  #}*/
/* */
/* {% if this.company.company_website %}*/
/* <p class="url">*/
/*   <a href="{{ this.company.company_website }}">{{ this.company.company_website }}</a>*/
/* </p>*/
/* {% endif %}*/
/* */
