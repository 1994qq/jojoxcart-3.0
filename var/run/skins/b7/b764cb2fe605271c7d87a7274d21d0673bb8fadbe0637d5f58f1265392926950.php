<?php

/* menu/account/top_menu_node.twig */
class __TwigTemplate_6ad503e2ea7361785daa10204ca6b7e3013029cae2c6e080ec8e622d86217423 extends \XLite\Core\Templating\Twig\Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 4
        echo "<div class=\"account-menu menu\">
  <div class=\"title\">
    <div class=\"icon\">";
        // line 6
        echo $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getSVGImage", array(0 => "images/fa-male-user.svg"), "method");
        echo "</div>
    <i class=\"fa fa-angle-right\"></i>
  </div>
  <div class=\"box\">";
        // line 9
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('widget')->getCallable(), array($this->env, $context, array(0 => "XLite\\View\\Menu\\Admin\\Account\\Menu"))), "html", null, true);
        echo "</div>
</div>
";
    }

    public function getTemplateName()
    {
        return "menu/account/top_menu_node.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  29 => 9,  23 => 6,  19 => 4,);
    }
}
/* {##*/
/*  # Account top menu node*/
/*  #}*/
/* <div class="account-menu menu">*/
/*   <div class="title">*/
/*     <div class="icon">{{ this.getSVGImage('images/fa-male-user.svg')|raw }}</div>*/
/*     <i class="fa fa-angle-right"></i>*/
/*   </div>*/
/*   <div class="box">{{ widget('XLite\\View\\Menu\\Admin\\Account\\Menu') }}</div>*/
/* </div>*/
/* */
