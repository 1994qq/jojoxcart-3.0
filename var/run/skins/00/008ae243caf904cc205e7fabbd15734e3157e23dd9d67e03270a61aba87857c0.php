<?php

/* /var/www/html/xcart/skins/admin/modules/QSL/BraintreeVZ/config/status.twig */
class __TwigTemplate_31392c07282ed99cc005537f0466e0dfaff6d54d6feaf13a26b33f3dce7265e6 extends \XLite\Core\Templating\Twig\Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 5
        echo "
";
        // line 6
        if ($this->getAttribute($this->getAttribute((isset($context["this"]) ? $context["this"] : null), "paymentMethod", array()), "getSetting", array(0 => "merchantId"), "method")) {
            // line 7
            echo "  <strong>";
            echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('t')->getCallable(), array("Merchant ID")), "html", null, true);
            echo ":</strong> ";
            echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["this"]) ? $context["this"] : null), "paymentMethod", array()), "getSetting", array(0 => "merchantId"), "method"), "html", null, true);
            echo "
";
        }
    }

    public function getTemplateName()
    {
        return "/var/www/html/xcart/skins/admin/modules/QSL/BraintreeVZ/config/status.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  24 => 7,  22 => 6,  19 => 5,);
    }
}
/* {##*/
/*  # Braintree merchant ID in the payment status panel*/
/*  # @ListChild (list="payment_status.after.PayPal_powered_by_Braintree", weight="20")*/
/*  #}*/
/* */
/* {% if (this.paymentMethod.getSetting('merchantId')) %}*/
/*   <strong>{{ t('Merchant ID') }}:</strong> {{ this.paymentMethod.getSetting('merchantId') }}*/
/* {% endif %}*/
/* */
