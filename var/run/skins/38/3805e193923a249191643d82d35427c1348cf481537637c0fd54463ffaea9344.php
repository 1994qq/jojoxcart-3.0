<?php

/* /var/www/html/xcart/skins/admin/dashboard/parts/body_markup.twig */
class __TwigTemplate_e372ee5505f474ebd933392167f1ec83a4b2c88c4d49deb25c496e87c6e210a4 extends \XLite\Core\Templating\Twig\Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 6
        echo "
<div class=\"center-block\">
    ";
        // line 8
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('widget_list')->getCallable(), array($this->env, $context, array(0 => "dashboard-center"))), "html", null, true);
        echo "
</div>

<div class=\"sidebar\">
  ";
        // line 12
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('widget_list')->getCallable(), array($this->env, $context, array(0 => "dashboard-sidebar"))), "html", null, true);
        echo "
</div>

<div class=\"clear\"></div>

";
    }

    public function getTemplateName()
    {
        return "/var/www/html/xcart/skins/admin/dashboard/parts/body_markup.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  30 => 12,  23 => 8,  19 => 6,);
    }
}
/* {##*/
/*  # Dashboard page markup template*/
/*  #*/
/*  # @ListChild (list="dashboard", weight="100")*/
/*  #}*/
/* */
/* <div class="center-block">*/
/*     {{ widget_list('dashboard-center') }}*/
/* </div>*/
/* */
/* <div class="sidebar">*/
/*   {{ widget_list('dashboard-sidebar') }}*/
/* </div>*/
/* */
/* <div class="clear"></div>*/
/* */
/* */
