<?php

/* modules_manager/install/body.twig */
class __TwigTemplate_4080a063b59a3da4950024c27201ad51e27f836e10d33d384f32e3f81ecb9a32 extends \XLite\Core\Templating\Twig\Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 5
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('widget')->getCallable(), array($this->env, $context, array(0 => "XLite\\View\\ItemsList\\Module\\Install"))), "html", null, true);
        echo "
";
    }

    public function getTemplateName()
    {
        return "modules_manager/install/body.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  19 => 5,);
    }
}
/* {##*/
/*  # Modules*/
/*  #}*/
/* {# Display add-ons list #}*/
/* {{ widget('XLite\\View\\ItemsList\\Module\\Install') }}*/
/* */
