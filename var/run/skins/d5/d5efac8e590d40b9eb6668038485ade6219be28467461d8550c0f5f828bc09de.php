<?php

/* items_list/module/manage/parts/columns/module-main-section/actions/manage-layout.twig */
class __TwigTemplate_1b3e3e8d4ceaeef2eb6a09015cfcded0402711b30d64f288a161e733fe900ed7 extends \XLite\Core\Templating\Twig\Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 4
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('widget')->getCallable(), array($this->env, $context, array(0 => "XLite\\View\\Button\\SimpleLink", "location" => call_user_func_array($this->env->getFunction('url')->getCallable(), array($this->env, $context, "layout")), "label" => call_user_func_array($this->env->getFunction('t')->getCallable(), array("Manage layout")), "style" => "manage-layout"))), "html", null, true);
        echo "
";
    }

    public function getTemplateName()
    {
        return "items_list/module/manage/parts/columns/module-main-section/actions/manage-layout.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  19 => 4,);
    }
}
/* {##*/
/*  # Manage layout link*/
/*  #}*/
/* {{ widget('XLite\\View\\Button\\SimpleLink', location=url('layout'), label=t('Manage layout'), style='manage-layout') }}*/
/* */
