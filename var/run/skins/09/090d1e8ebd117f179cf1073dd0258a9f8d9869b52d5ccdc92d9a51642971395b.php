<?php

/* modules/XC/Onboarding/wizard_progress/progress.twig */
class __TwigTemplate_14a7527b8f6a5c1a78102a9382ea5285cdbc2e842617c1243bfc958d28e32009 extends \XLite\Core\Templating\Twig\Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<xlite-wizard-progress inline-template :step=\"step\" :steps=\"steps\" :landmarks=\"";
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, twig_jsonencode_filter($this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getLandmarks", array(), "method")), "html", null, true);
        echo "\" :current-step=\"currentStep\" :last-product=\"lastProduct\">
  <div class=\"onboarding-wizard-progress\" v-data='{\"progress\": ";
        // line 2
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getPercentage", array(), "method"), "html", null, true);
        echo " }'>
    <div class=\"percentage\" v-text=\"this.progress + '%'\" v-if=\"!isCurrentStep('intro')\" transition=\"fade-in-out\">";
        // line 3
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, ($this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getPercentage", array(), "method") . "%"), "html", null, true);
        echo "</div>
    <div class=\"bar\">
      <div class=\"landmarks\">
        ";
        // line 6
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getLandmarks", array(), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["step"]) {
            // line 7
            echo "          <div class=\"landmark landmark-";
            echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute($context["step"], "index", array()), "html", null, true);
            echo "\" :class=\"";
            echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, ("landmarkClass." . $this->getAttribute($context["step"], "index", array())), "html", null, true);
            echo "\" @click=\"goToStep('";
            echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute($context["step"], "index", array()), "html", null, true);
            echo "')\" tabindex=\"-1\">
            ";
            // line 8
            echo call_user_func_array($this->env->getFunction('svg')->getCallable(), array($this->env, $context, $this->getAttribute($context["step"], "image", array())));
            echo "
            <span class=\"landmark-text\" v-if=\"isCurrentStep('intro')\" transition=\"fade-in-out\">";
            // line 9
            echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('t')->getCallable(), array($this->getAttribute($context["step"], "name", array()))), "html", null, true);
            echo "</span>
          </div>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['step'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 12
        echo "      </div>
      <div class=\"progress-line\">
        <div class=\"progress-line-filled\" style=\"width: ";
        // line 14
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, ($this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getPercentage", array(), "method") . "%"), "html", null, true);
        echo "\" :style=\"barStyle\"></div>
      </div>
    </div>
    <div class=\"finish-mark\" :class=\"finishClass\" v-if=\"!isCurrentStep('intro')\" transition=\"fade-in-out\">
      ";
        // line 18
        echo call_user_func_array($this->env->getFunction('svg')->getCallable(), array($this->env, $context, "modules/XC/Onboarding/images/ok-mark.svg"));
        echo "
    </div>
  </div>
</xlite-wizard-progress>";
    }

    public function getTemplateName()
    {
        return "modules/XC/Onboarding/wizard_progress/progress.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  71 => 18,  64 => 14,  60 => 12,  51 => 9,  47 => 8,  38 => 7,  34 => 6,  28 => 3,  24 => 2,  19 => 1,);
    }
}
/* <xlite-wizard-progress inline-template :step="step" :steps="steps" :landmarks="{{ this.getLandmarks() | json_encode }}" :current-step="currentStep" :last-product="lastProduct">*/
/*   <div class="onboarding-wizard-progress" v-data='{"progress": {{ this.getPercentage() }} }'>*/
/*     <div class="percentage" v-text="this.progress + '%'" v-if="!isCurrentStep('intro')" transition="fade-in-out">{{ this.getPercentage() ~ '%' }}</div>*/
/*     <div class="bar">*/
/*       <div class="landmarks">*/
/*         {% for step in this.getLandmarks() %}*/
/*           <div class="landmark landmark-{{ step.index }}" :class="{{ 'landmarkClass.' ~ step.index }}" @click="goToStep('{{ step.index }}')" tabindex="-1">*/
/*             {{ svg(step.image)|raw }}*/
/*             <span class="landmark-text" v-if="isCurrentStep('intro')" transition="fade-in-out">{{ t(step.name) }}</span>*/
/*           </div>*/
/*         {% endfor %}*/
/*       </div>*/
/*       <div class="progress-line">*/
/*         <div class="progress-line-filled" style="width: {{ this.getPercentage() ~ '%' }}" :style="barStyle"></div>*/
/*       </div>*/
/*     </div>*/
/*     <div class="finish-mark" :class="finishClass" v-if="!isCurrentStep('intro')" transition="fade-in-out">*/
/*       {{ svg('modules/XC/Onboarding/images/ok-mark.svg')|raw }}*/
/*     </div>*/
/*   </div>*/
/* </xlite-wizard-progress>*/
