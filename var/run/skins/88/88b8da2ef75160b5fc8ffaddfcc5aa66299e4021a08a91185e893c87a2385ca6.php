<?php

/* /var/www/html/xcart/skins/customer/modules/XC/FastLaneCheckout/blocks/payment_methods/available/payment_tpl.twig */
class __TwigTemplate_bfc9753b4d4335d964187d0661a64a4ab2c80f1fff96a567011ba6b0922ab60a extends \XLite\Core\Templating\Twig\Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 6
        echo "
";
        // line 7
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('widget')->getCallable(), array($this->env, $context, array(0 => "XLite\\View\\Checkout\\Payment"))), "html", null, true);
        echo "
";
    }

    public function getTemplateName()
    {
        return "/var/www/html/xcart/skins/customer/modules/XC/FastLaneCheckout/blocks/payment_methods/available/payment_tpl.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  22 => 7,  19 => 6,);
    }
}
/* {##*/
/*  # Fastlane checkout address section*/
/*  #*/
/*  # @ListChild (list="checkout_fastlane.blocks.payment_methods.available", weight="10")*/
/*  #}*/
/* */
/* {{ widget('XLite\\View\\Checkout\\Payment') }}*/
/* */
