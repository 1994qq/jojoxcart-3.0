<?php

/* form_field/select_state.twig */
class __TwigTemplate_68008c4e27d302ead466d3a236c78db5eea91ff21d27e7e2bb4fb72ae57c6009 extends \XLite\Core\Templating\Twig\Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 4
        echo "
";
        // line 5
        ob_start();
        // line 6
        echo "<select ";
        echo $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getAttributesCode", array(), "method");
        echo " data-value=\"";
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getValue", array(), "method"), "html", null, true);
        echo "\">
  ";
        // line 7
        if ($this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getParam", array(0 => "selectOne"), "method")) {
            // line 8
            echo "      <option value=\"\" ";
            if ($this->getAttribute((isset($context["this"]) ? $context["this"] : null), "isOptionSelected", array(0 => null), "method")) {
                echo "selected=\"selected\"";
            }
            echo " data-select-one=\"data-select-one\">";
            echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getParam", array(0 => "selectOneLabel"), "method"), "html", null, true);
            echo "</option>
  ";
        }
        // line 10
        echo "  ";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getOptions", array(), "method"));
        foreach ($context['_seq'] as $context["index"] => $context["state"]) {
            // line 11
            echo "    ";
            if ($this->getAttribute((isset($context["this"]) ? $context["this"] : null), "isGroup", array(0 => $context["state"]), "method")) {
                // line 12
                echo "        <optgroup ";
                echo $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getOptionGroupAttributesCode", array(0 => $context["index"], 1 => $context["state"]), "method");
                echo " data-id='";
                echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $context["index"], "html", null, true);
                echo "'>
          ";
                // line 13
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["state"], "options", array()));
                foreach ($context['_seq'] as $context["_key"] => $context["state2"]) {
                    // line 14
                    echo "              <option value=\"";
                    echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute($context["state2"], "getStateId", array(), "method"), "html", null, true);
                    echo "\" ";
                    if ($this->getAttribute((isset($context["this"]) ? $context["this"] : null), "isOptionSelected", array(0 => $this->getAttribute($context["state2"], "getStateId", array(), "method")), "method")) {
                        echo " selected=\"selected\" ";
                    }
                    echo ">";
                    echo $this->getAttribute($context["state2"], "getState", array(), "method");
                    echo "</option>
          ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['state2'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 16
                echo "        </optgroup>
    ";
            } else {
                // line 18
                echo "        <option value=\"";
                echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute($context["state"], "getStateId", array(), "method"), "html", null, true);
                echo "\" ";
                if ($this->getAttribute((isset($context["this"]) ? $context["this"] : null), "isOptionSelected", array(0 => $this->getAttribute($context["state"], "getStateId", array(), "method")), "method")) {
                    echo " selected=\"selected\" ";
                }
                echo ">";
                echo $this->getAttribute($context["state"], "getState", array(), "method");
                echo "</option>
    ";
            }
            // line 20
            echo "  ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['index'], $context['state'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 21
        echo "</select>
";
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
    }

    public function getTemplateName()
    {
        return "form_field/select_state.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  99 => 21,  93 => 20,  81 => 18,  77 => 16,  62 => 14,  58 => 13,  51 => 12,  48 => 11,  43 => 10,  33 => 8,  31 => 7,  24 => 6,  22 => 5,  19 => 4,);
    }
}
/* {##*/
/*  # Select state*/
/*  #}*/
/* */
/* {% spaceless %}*/
/* <select {{ this.getAttributesCode()|raw }} data-value="{{ this.getValue() }}">*/
/*   {% if this.getParam('selectOne') %}*/
/*       <option value="" {% if this.isOptionSelected(null) %}selected="selected"{% endif %} data-select-one="data-select-one">{{ this.getParam('selectOneLabel') }}</option>*/
/*   {% endif %}*/
/*   {% for index, state in this.getOptions() %}*/
/*     {% if this.isGroup(state) %}*/
/*         <optgroup {{ this.getOptionGroupAttributesCode(index, state)|raw }} data-id='{{ index }}'>*/
/*           {% for state2 in state.options %}*/
/*               <option value="{{ state2.getStateId() }}" {% if this.isOptionSelected(state2.getStateId()) %} selected="selected" {% endif %}>{{ state2.getState()|raw }}</option>*/
/*           {% endfor %}*/
/*         </optgroup>*/
/*     {% else %}*/
/*         <option value="{{ state.getStateId() }}" {% if this.isOptionSelected(state.getStateId()) %} selected="selected" {% endif %}>{{ state.getState()|raw }}</option>*/
/*     {% endif %}*/
/*   {% endfor %}*/
/* </select>*/
/* {% endspaceless %}*/
