<?php

/* modules/XC/Onboarding/wizard_steps/shipping_rates/body.twig */
class __TwigTemplate_f5b71d431623c288826a83b5ea65ee5c8d3dc04bbc31d529725d70f3a1e62dff extends \XLite\Core\Templating\Twig\Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 4
        echo "
<div class=\"onboarding-wizard-step step-";
        // line 5
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getStepIndex", array(), "method"), "html", null, true);
        echo "\"
     v-show=\"isCurrentStep('";
        // line 6
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getStepIndex", array(), "method"), "html", null, true);
        echo "')\"
     :transition=\"stepTransition\">
  <xlite-wizard-step-shipping-rates inline-template :methods=\"";
        // line 8
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, twig_jsonencode_filter($this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getMethodsData", array(), "method")), "html", null, true);
        echo "\">
    <div class=\"step-contents\">
      <h2 class=\"heading\">";
        // line 10
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('t')->getCallable(), array("Set flat shipping rates for your store")), "html", null, true);
        echo "</h2>

      <p class=\"text\">
        ";
        // line 13
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('t')->getCallable(), array("or choose a shipping carrier to provide your customers with real-time quotes")), "html", null, true);
        echo "
      </p>

      ";
        // line 16
        if ($this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getMethods", array(), "method")) {
            // line 17
            echo "        <div class=\"shipping-carriers\">
          ";
            // line 18
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getMethods", array(), "method"));
            foreach ($context['_seq'] as $context["_key"] => $context["method"]) {
                // line 19
                echo "            <div class=\"shipping-carrier image\" v-if=\"isMethodAvailable('";
                echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute($context["method"], "processor", array()), "html", null, true);
                echo "')\" :class=\"";
                echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, ("methodClasses." . $this->getAttribute($context["method"], "processor", array())), "html", null, true);
                echo "\" data-processor=\"";
                echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute($context["method"], "processor", array()), "html", null, true);
                echo "\">
              <a href=\"";
                // line 20
                echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getSettingsURL", array(0 => $context["method"]), "method"), "html", null, true);
                echo "\" target=\"_blank\"
                 @click.prevent=\"addShippingMethod('";
                // line 21
                echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getSettingsURL", array(0 => $context["method"]), "method"), "html", null, true);
                echo "', \$event)\">
                <img src=\"";
                // line 22
                echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute($context["method"], "getAdminIconURL", array(), "method"), "html", null, true);
                echo "\" alt=\"";
                echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute($context["method"], "name", array()), "html", null, true);
                echo "\"/>
              </a>
            </div>
          ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['method'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 26
            echo "        </div>

        <div class=\"separator\">
          ";
            // line 29
            echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('t')->getCallable(), array("Here you can manually define your shipping rates and etc.")), "html", null, true);
            echo "
          <a href=\"";
            // line 30
            echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getArticleLink", array(), "method"), "html", null, true);
            echo "\" target=\"_blank\" class=\"external\" style=\"margin-left: 5px;\">";
            echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('t')->getCallable(), array("How to define shipping rates")), "html", null, true);
            echo "</a>
        </div>
      ";
        }
        // line 33
        echo "

      <div class=\"my-shipping\">
        <div class=\"method-label\">
          <p class=\"input-display\" v-text=\"name\" v-show=\"!focusedName\" tabindex=\"-1\">";
        // line 37
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getDefaultMethodName", array(), "method"), "html", null, true);
        echo "</p>
          <div class=\"input-wrapper\" v-show=\"focusedName\">
            ";
        // line 39
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('widget')->getCallable(), array($this->env, $context, array(0 => "\\XLite\\View\\FormField\\Input\\Text", "fieldOnly" => true, "attributes" => array("v-model" => "name"), "value" => $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getDefaultMethodName", array(), "method")))), "html", null, true);
        echo "
          </div>
        </div>
        <div class=\"zones\">
          ";
        // line 43
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('widget')->getCallable(), array($this->env, $context, array(0 => "\\XLite\\View\\FormField\\Select\\ShippingZone", "fieldOnly" => true))), "html", null, true);
        echo "
        </div>
        <div class=\"flat-rate\">
          ";
        // line 46
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('widget')->getCallable(), array($this->env, $context, array(0 => "\\XLite\\View\\FormField\\Input\\Text\\Price", "fieldOnly" => true, "placeholder" => "0.00"))), "html", null, true);
        echo "
        </div>
      </div>

      <div class=\"buttons\">
        <div class=\"more-button\">
          ";
        // line 52
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('widget')->getCallable(), array($this->env, $context, array(0 => "\\XLite\\View\\Button\\Link", "label" => "Advanced shipping options", "location" => $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getMoreSettingsLocation", array(), "method"), "blank" => 1))), "html", null, true);
        echo "
        </div>
        <div class=\"next-step\">
          ";
        // line 55
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('widget')->getCallable(), array($this->env, $context, array(0 => "\\XLite\\View\\Button\\Regular", "label" => "Create shipping rate", "style" => "regular-main-button", "attributes" => array("@click" => "createMethod"), "jsCode" => "null;"))), "html", null, true);
        echo "
        </div>
      </div>

      <div class=\"popup-template\">
        <div class=\"method-added-popup\">
          <div class=\"image\">
            <img src=\"\" alt=\"\"/>
          </div>

          <div class=\"note\">
            ";
        // line 66
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('t')->getCallable(), array("Bingo! You've added carrier as a shipping carrier")), "html", null, true);
        echo "
          </div>

          <div class=\"settings-link\">
          </div>

          <div class=\"buttons\">
            <a class=\"settings-link btn regular-button\" href=\"\" target=\"_blank\">
              <span>";
        // line 74
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('t')->getCallable(), array("Module settings (shipping_rates)")), "html", null, true);
        echo "</span>
            </a>
            ";
        // line 76
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('widget')->getCallable(), array($this->env, $context, array(0 => "\\XLite\\View\\Button\\Regular", "label" => "Proceed to the next step", "style" => "regular-main-button", "jsCode" => "null;"))), "html", null, true);
        echo "
          </div>
        </div>
      </div>
    </div>
  </xlite-wizard-step-shipping-rates>
</div>";
    }

    public function getTemplateName()
    {
        return "modules/XC/Onboarding/wizard_steps/shipping_rates/body.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  172 => 76,  167 => 74,  156 => 66,  142 => 55,  136 => 52,  127 => 46,  121 => 43,  114 => 39,  109 => 37,  103 => 33,  95 => 30,  91 => 29,  86 => 26,  74 => 22,  70 => 21,  66 => 20,  57 => 19,  53 => 18,  50 => 17,  48 => 16,  42 => 13,  36 => 10,  31 => 8,  26 => 6,  22 => 5,  19 => 4,);
    }
}
/* {##*/
/*  # Shipping rates step*/
/*  #}*/
/* */
/* <div class="onboarding-wizard-step step-{{ this.getStepIndex() }}"*/
/*      v-show="isCurrentStep('{{ this.getStepIndex() }}')"*/
/*      :transition="stepTransition">*/
/*   <xlite-wizard-step-shipping-rates inline-template :methods="{{ this.getMethodsData() | json_encode }}">*/
/*     <div class="step-contents">*/
/*       <h2 class="heading">{{ t("Set flat shipping rates for your store") }}</h2>*/
/* */
/*       <p class="text">*/
/*         {{ t("or choose a shipping carrier to provide your customers with real-time quotes") }}*/
/*       </p>*/
/* */
/*       {% if this.getMethods() %}*/
/*         <div class="shipping-carriers">*/
/*           {% for method in this.getMethods() %}*/
/*             <div class="shipping-carrier image" v-if="isMethodAvailable('{{ method.processor }}')" :class="{{ 'methodClasses.' ~ method.processor }}" data-processor="{{ method.processor }}">*/
/*               <a href="{{ this.getSettingsURL(method) }}" target="_blank"*/
/*                  @click.prevent="addShippingMethod('{{ this.getSettingsURL(method) }}', $event)">*/
/*                 <img src="{{ method.getAdminIconURL() }}" alt="{{ method.name }}"/>*/
/*               </a>*/
/*             </div>*/
/*           {% endfor %}*/
/*         </div>*/
/* */
/*         <div class="separator">*/
/*           {{ t("Here you can manually define your shipping rates and etc.") }}*/
/*           <a href="{{ this.getArticleLink() }}" target="_blank" class="external" style="margin-left: 5px;">{{ t("How to define shipping rates") }}</a>*/
/*         </div>*/
/*       {% endif %}*/
/* */
/* */
/*       <div class="my-shipping">*/
/*         <div class="method-label">*/
/*           <p class="input-display" v-text="name" v-show="!focusedName" tabindex="-1">{{ this.getDefaultMethodName() }}</p>*/
/*           <div class="input-wrapper" v-show="focusedName">*/
/*             {{ widget('\\XLite\\View\\FormField\\Input\\Text', fieldOnly=true, attributes={'v-model': 'name'}, value=this.getDefaultMethodName()) }}*/
/*           </div>*/
/*         </div>*/
/*         <div class="zones">*/
/*           {{ widget('\\XLite\\View\\FormField\\Select\\ShippingZone', fieldOnly=true) }}*/
/*         </div>*/
/*         <div class="flat-rate">*/
/*           {{ widget('\\XLite\\View\\FormField\\Input\\Text\\Price', fieldOnly=true, placeholder="0.00") }}*/
/*         </div>*/
/*       </div>*/
/* */
/*       <div class="buttons">*/
/*         <div class="more-button">*/
/*           {{ widget('\\XLite\\View\\Button\\Link', label='Advanced shipping options', location=this.getMoreSettingsLocation(), blank=1) }}*/
/*         </div>*/
/*         <div class="next-step">*/
/*           {{ widget('\\XLite\\View\\Button\\Regular', label='Create shipping rate', style='regular-main-button', attributes={'@click': 'createMethod'}, jsCode="null;") }}*/
/*         </div>*/
/*       </div>*/
/* */
/*       <div class="popup-template">*/
/*         <div class="method-added-popup">*/
/*           <div class="image">*/
/*             <img src="" alt=""/>*/
/*           </div>*/
/* */
/*           <div class="note">*/
/*             {{ t("Bingo! You've added carrier as a shipping carrier") }}*/
/*           </div>*/
/* */
/*           <div class="settings-link">*/
/*           </div>*/
/* */
/*           <div class="buttons">*/
/*             <a class="settings-link btn regular-button" href="" target="_blank">*/
/*               <span>{{ t('Module settings (shipping_rates)') }}</span>*/
/*             </a>*/
/*             {{ widget('\\XLite\\View\\Button\\Regular', label='Proceed to the next step', style='regular-main-button', jsCode="null;") }}*/
/*           </div>*/
/*         </div>*/
/*       </div>*/
/*     </div>*/
/*   </xlite-wizard-step-shipping-rates>*/
/* </div>*/
