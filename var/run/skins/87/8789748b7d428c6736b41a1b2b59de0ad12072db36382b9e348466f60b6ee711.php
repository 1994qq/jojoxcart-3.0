<?php

/* modules/XC/Onboarding/wizard_steps/company_info/body.twig */
class __TwigTemplate_b8845b07e5685f44f79d7f861f4d9aabf9c1cbca8e2af0575ed59cee0872ec75 extends \XLite\Core\Templating\Twig\Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 4
        echo "
<div class=\"onboarding-wizard-step step-";
        // line 5
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getStepIndex", array(), "method"), "html", null, true);
        echo "\"
     v-show=\"isCurrentStep('";
        // line 6
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getStepIndex", array(), "method"), "html", null, true);
        echo "')\"
     :transition=\"stepTransition\">
  <xlite-wizard-step-company-info inline-template>
    <div class=\"step-contents\">
      <h2 class=\"heading\">";
        // line 10
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('t')->getCallable(), array("Set your company info")), "html", null, true);
        echo "</h2>
      <p class=\"text\">";
        // line 11
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('t')->getCallable(), array("We will use this info to create invoices, send emails and calculate shipping rates for your customers.")), "html", null, true);
        echo "</p>

      ";
        // line 13
        $this->startForm("\\XLite\\Module\\XC\\Onboarding\\View\\Form\\CompanyInfo");        // line 14
        echo "      <div class=\"fields\">
        ";
        // line 15
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('widget')->getCallable(), array($this->env, $context, array(0 => "\\XLite\\View\\FormField\\Input\\Text", "label" => "Company name", "fieldName" => "company_name", "value" => $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getCompanyName", array(), "method")))), "html", null, true);
        echo "
        ";
        // line 16
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('widget')->getCallable(), array($this->env, $context, array(0 => "\\XLite\\View\\FormField\\Input\\Text", "label" => "Address", "fieldName" => "address", "value" => $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getAddress", array(), "method")))), "html", null, true);
        echo "
        <div class=\"parent-block\">
          ";
        // line 18
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('widget')->getCallable(), array($this->env, $context, array(0 => "\\XLite\\View\\FormField\\Select\\State", "fieldName" => "address_state_select", "label" => "State", "value" => $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getState", array(), "method"), "country" => $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getCountry", array(), "method")))), "html", null, true);
        echo "
        </div>
        <div class=\"parent-block\">
          ";
        // line 21
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('widget')->getCallable(), array($this->env, $context, array(0 => "\\XLite\\View\\FormField\\Input\\Text", "fieldName" => "address_custom_state", "value" => $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getOtherState", array(), "method"), "label" => "State"))), "html", null, true);
        echo "
        </div>
        ";
        // line 23
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('widget')->getCallable(), array($this->env, $context, array(0 => "\\XLite\\View\\FormField\\Input\\Text", "label" => "Phone", "fieldName" => "phone", "value" => $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getPhone", array(), "method")))), "html", null, true);
        echo "
        ";
        // line 24
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('widget')->getCallable(), array($this->env, $context, array(0 => "\\XLite\\View\\FormField\\Input\\Text", "label" => "City", "fieldName" => "city", "value" => $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getCity", array(), "method")))), "html", null, true);
        echo "
        ";
        // line 25
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('widget')->getCallable(), array($this->env, $context, array(0 => "\\XLite\\View\\FormField\\Input\\Text", "label" => "Zip code", "fieldName" => "zipcode", "value" => $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getZipCode", array(), "method")))), "html", null, true);
        echo "
      </div>

      <div class=\"buttons bottom-sticky\">
        <div class=\"next-step\">
          ";
        // line 30
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('widget')->getCallable(), array($this->env, $context, array(0 => "\\XLite\\View\\Button\\Submit", "label" => "Save and go to the next step", "style" => "action regular-main-button"))), "html", null, true);
        echo "
        </div>
      </div>
      ";
        $this->endForm();        // line 34
        echo "    </div>
  </xlite-wizard-step-company-info>
</div>";
    }

    public function getTemplateName()
    {
        return "modules/XC/Onboarding/wizard_steps/company_info/body.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  88 => 34,  82 => 30,  74 => 25,  70 => 24,  66 => 23,  61 => 21,  55 => 18,  50 => 16,  46 => 15,  43 => 14,  42 => 13,  37 => 11,  33 => 10,  26 => 6,  22 => 5,  19 => 4,);
    }
}
/* {##*/
/*  # Company info step*/
/*  #}*/
/* */
/* <div class="onboarding-wizard-step step-{{ this.getStepIndex() }}"*/
/*      v-show="isCurrentStep('{{ this.getStepIndex() }}')"*/
/*      :transition="stepTransition">*/
/*   <xlite-wizard-step-company-info inline-template>*/
/*     <div class="step-contents">*/
/*       <h2 class="heading">{{ t("Set your company info") }}</h2>*/
/*       <p class="text">{{ t("We will use this info to create invoices, send emails and calculate shipping rates for your customers.") }}</p>*/
/* */
/*       {% form '\\XLite\\Module\\XC\\Onboarding\\View\\Form\\CompanyInfo' %}*/
/*       <div class="fields">*/
/*         {{ widget('\\XLite\\View\\FormField\\Input\\Text', label='Company name', fieldName='company_name', value=this.getCompanyName()) }}*/
/*         {{ widget('\\XLite\\View\\FormField\\Input\\Text', label='Address', fieldName='address', value=this.getAddress()) }}*/
/*         <div class="parent-block">*/
/*           {{ widget('\\XLite\\View\\FormField\\Select\\State', fieldName='address_state_select', label='State', value=this.getState(), country=this.getCountry()) }}*/
/*         </div>*/
/*         <div class="parent-block">*/
/*           {{ widget('\\XLite\\View\\FormField\\Input\\Text', fieldName='address_custom_state', value=this.getOtherState(), label='State') }}*/
/*         </div>*/
/*         {{ widget('\\XLite\\View\\FormField\\Input\\Text', label='Phone', fieldName='phone', value=this.getPhone()) }}*/
/*         {{ widget('\\XLite\\View\\FormField\\Input\\Text', label='City', fieldName='city', value=this.getCity()) }}*/
/*         {{ widget('\\XLite\\View\\FormField\\Input\\Text', label='Zip code', fieldName='zipcode', value=this.getZipCode()) }}*/
/*       </div>*/
/* */
/*       <div class="buttons bottom-sticky">*/
/*         <div class="next-step">*/
/*           {{ widget('\\XLite\\View\\Button\\Submit', label='Save and go to the next step', style='action regular-main-button') }}*/
/*         </div>*/
/*       </div>*/
/*       {% endform %}*/
/*     </div>*/
/*   </xlite-wizard-step-company-info>*/
/* </div>*/
