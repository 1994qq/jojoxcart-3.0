<?php

/* button/dropdown.twig */
class __TwigTemplate_1e257fd2d070e86d92c62018cc492d9418c2e803c0289354b6303c7fc063aa58 extends \XLite\Core\Templating\Twig\Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 4
        echo "<div class=\"btn-group btn-dropdown ";
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getDropDirection", array(), "method"), "html", null, true);
        echo "\" role=\"group\">

  ";
        // line 6
        $context["showCaret"] = $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getShowCaret", array(), "method");
        // line 7
        echo "  ";
        $context["useCaretButton"] = $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getUseCaretButton", array(), "method");
        // line 8
        echo "
  <button type=\"button\"
          ";
        // line 10
        if ($this->getAttribute((isset($context["this"]) ? $context["this"] : null), "hasName", array(), "method")) {
            echo " name=\"";
            echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getName", array(), "method"), "html", null, true);
            echo "\"";
        }
        // line 11
        echo "          ";
        if ($this->getAttribute((isset($context["this"]) ? $context["this"] : null), "hasValue", array(), "method")) {
            echo " value=\"";
            echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getValue", array(), "method"), "html", null, true);
            echo "\"";
        }
        // line 12
        echo "          class=\"";
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getClass", array(), "method"), "html", null, true);
        echo "\"
          role=\"group\"
          ";
        // line 14
        if ( !(isset($context["useCaretButton"]) ? $context["useCaretButton"] : null)) {
            // line 15
            echo "    data-toggle=\"dropdown\"
    aria-haspopup=\"true\"
    aria-expanded=\"false\"
          ";
        }
        // line 19
        echo "          ";
        if ($this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getButtonTitle", array(), "method")) {
            // line 20
            echo "            title=\"";
            echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getButtonTitle", array(), "method"), "html", null, true);
            echo "\"
          ";
        }
        // line 21
        echo ">

    ";
        // line 23
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "displayCommentedData", array(0 => $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getCommentedData", array(), "method")), "method"), "html", null, true);
        echo "

    ";
        // line 25
        $context["iconStyle"] = $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getParam", array(0 => "icon-style"), "method");
        // line 26
        echo "    ";
        if ((isset($context["iconStyle"]) ? $context["iconStyle"] : null)) {
            echo "<span><i class=\"";
            echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, (isset($context["iconStyle"]) ? $context["iconStyle"] : null), "html", null, true);
            echo "\"></i></span>";
        }
        // line 27
        echo "
    ";
        // line 28
        $context["buttonLabel"] = call_user_func_array($this->env->getFunction('t')->getCallable(), array($this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getButtonLabel", array(), "method")));
        // line 29
        echo "    ";
        if ((isset($context["buttonLabel"]) ? $context["buttonLabel"] : null)) {
            echo "<span>";
            echo (isset($context["buttonLabel"]) ? $context["buttonLabel"] : null);
            echo "</span>";
        }
        // line 30
        echo "
    ";
        // line 31
        if (((isset($context["showCaret"]) ? $context["showCaret"] : null) &&  !(isset($context["useCaretButton"]) ? $context["useCaretButton"] : null))) {
            // line 32
            echo "      <span class=\"caret\"></span>
    ";
        }
        // line 34
        echo "  </button>
  ";
        // line 35
        if ((isset($context["useCaretButton"]) ? $context["useCaretButton"] : null)) {
            // line 36
            echo "    <button
            type=\"button\"
            class=\"btn btn-default dropdown-toggle\"
            data-toggle=\"dropdown\"
            aria-haspopup=\"true\"
            aria-expanded=\"false\">
      <span class=\"caret\"></span>
    </button>
  ";
        }
        // line 45
        echo "
  ";
        // line 46
        if ($this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getAdditionalButtons", array(), "method")) {
            // line 47
            echo "    <ul class=\"dropdown-menu\" role=\"menu\">
      ";
            // line 48
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getAdditionalButtons", array(), "method"));
            foreach ($context['_seq'] as $context["_key"] => $context["button"]) {
                // line 49
                echo "        <li>";
                echo $this->getAttribute($context["button"], "display", array(), "method");
                echo "</li>
      ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['button'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 51
            echo "    </ul>
  ";
        }
        // line 53
        echo "</div>
";
    }

    public function getTemplateName()
    {
        return "button/dropdown.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  150 => 53,  146 => 51,  137 => 49,  133 => 48,  130 => 47,  128 => 46,  125 => 45,  114 => 36,  112 => 35,  109 => 34,  105 => 32,  103 => 31,  100 => 30,  93 => 29,  91 => 28,  88 => 27,  81 => 26,  79 => 25,  74 => 23,  70 => 21,  64 => 20,  61 => 19,  55 => 15,  53 => 14,  47 => 12,  40 => 11,  34 => 10,  30 => 8,  27 => 7,  25 => 6,  19 => 4,);
    }
}
/* {##*/
/*  # Dropdown button*/
/*  #}*/
/* <div class="btn-group btn-dropdown {{ this.getDropDirection() }}" role="group">*/
/* */
/*   {% set showCaret = this.getShowCaret() %}*/
/*   {% set useCaretButton = this.getUseCaretButton() %}*/
/* */
/*   <button type="button"*/
/*           {% if this.hasName() %} name="{{ this.getName() }}"{% endif %}*/
/*           {% if this.hasValue() %} value="{{ this.getValue() }}"{% endif %}*/
/*           class="{{ this.getClass() }}"*/
/*           role="group"*/
/*           {% if not useCaretButton %}*/
/*     data-toggle="dropdown"*/
/*     aria-haspopup="true"*/
/*     aria-expanded="false"*/
/*           {% endif %}*/
/*           {% if this.getButtonTitle() %}*/
/*             title="{{ this.getButtonTitle() }}"*/
/*           {% endif %}>*/
/* */
/*     {{ this.displayCommentedData(this.getCommentedData()) }}*/
/* */
/*     {% set iconStyle = this.getParam('icon-style') %}*/
/*     {% if iconStyle %}<span><i class="{{ iconStyle }}"></i></span>{% endif %}*/
/* */
/*     {% set buttonLabel = t(this.getButtonLabel()) %}*/
/*     {% if buttonLabel %}<span>{{ buttonLabel|raw }}</span>{% endif %}*/
/* */
/*     {% if showCaret and not useCaretButton %}*/
/*       <span class="caret"></span>*/
/*     {% endif %}*/
/*   </button>*/
/*   {% if useCaretButton %}*/
/*     <button*/
/*             type="button"*/
/*             class="btn btn-default dropdown-toggle"*/
/*             data-toggle="dropdown"*/
/*             aria-haspopup="true"*/
/*             aria-expanded="false">*/
/*       <span class="caret"></span>*/
/*     </button>*/
/*   {% endif %}*/
/* */
/*   {% if this.getAdditionalButtons() %}*/
/*     <ul class="dropdown-menu" role="menu">*/
/*       {% for button in this.getAdditionalButtons() %}*/
/*         <li>{{ button.display()|raw }}</li>*/
/*       {% endfor %}*/
/*     </ul>*/
/*   {% endif %}*/
/* </div>*/
/* */
