<?php

/* form_field/input/checkbox/on_off.twig */
class __TwigTemplate_a4212b07fce90493c04654a0df4926965a13365f1457924568751e15fc1b81e1 extends \XLite\Core\Templating\Twig\Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 4
        echo "<div class=\"";
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getCssClass", array(), "method"), "html", null, true);
        echo "\"";
        if (($this->getAttribute((isset($context["this"]) ? $context["this"] : null), "isDisabled", array(), "method") && $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getDisabledTitle", array(), "method"))) {
            echo " title=\"";
            echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getDisabledTitle", array(), "method"), "html", null, true);
            echo "\"";
        }
        echo ">
  ";
        // line 5
        if ( !$this->getAttribute((isset($context["this"]) ? $context["this"] : null), "isDisabled", array(), "method")) {
            // line 6
            echo "    <input type=\"hidden\" name=\"";
            echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getName", array(), "method"), "html", null, true);
            echo "\" value=\"\" />
  ";
        }
        // line 8
        echo "  ";
        if ($this->getAttribute((isset($context["this"]) ? $context["this"] : null), "isDisabled", array(), "method")) {
            // line 9
            echo "    <input type=\"hidden\" name=\"";
            echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getName", array(), "method"), "html", null, true);
            echo "\" value=\"";
            echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getValue", array(), "method"), "html", null, true);
            echo "\" />
  ";
        }
        // line 11
        echo "  <input";
        echo $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getAttributesCode", array(), "method");
        echo " />
  <label for=\"";
        // line 12
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getFieldId", array(), "method"), "html", null, true);
        echo "\">
    <div class=\"onoffswitch-inner\">
      <div class=\"on-caption\">";
        // line 14
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('t')->getCallable(), array($this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getOnLabel", array(), "method"))), "html", null, true);
        echo "</div>
      <div class=\"off-caption\">";
        // line 15
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('t')->getCallable(), array($this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getOffLabel", array(), "method"))), "html", null, true);
        echo "</div>
    </div>
    <span class=\"onoffswitch-switch\"></span>
  </label>
</div>
";
    }

    public function getTemplateName()
    {
        return "form_field/input/checkbox/on_off.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  63 => 15,  59 => 14,  54 => 12,  49 => 11,  41 => 9,  38 => 8,  32 => 6,  30 => 5,  19 => 4,);
    }
}
/* {##*/
/*  # OnOff checkbox*/
/*  #}*/
/* <div class="{{ this.getCssClass() }}"{% if this.isDisabled() and this.getDisabledTitle() %} title="{{ this.getDisabledTitle() }}"{% endif %}>*/
/*   {% if not this.isDisabled() %}*/
/*     <input type="hidden" name="{{ this.getName() }}" value="" />*/
/*   {% endif %}*/
/*   {% if this.isDisabled() %}*/
/*     <input type="hidden" name="{{ this.getName() }}" value="{{ this.getValue() }}" />*/
/*   {% endif %}*/
/*   <input{{ this.getAttributesCode()|raw }} />*/
/*   <label for="{{ this.getFieldId() }}">*/
/*     <div class="onoffswitch-inner">*/
/*       <div class="on-caption">{{ t(this.getOnLabel()) }}</div>*/
/*       <div class="off-caption">{{ t(this.getOffLabel()) }}</div>*/
/*     </div>*/
/*     <span class="onoffswitch-switch"></span>*/
/*   </label>*/
/* </div>*/
/* */
