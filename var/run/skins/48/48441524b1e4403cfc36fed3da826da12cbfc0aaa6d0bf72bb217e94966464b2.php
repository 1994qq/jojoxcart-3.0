<?php

/* /var/www/html/xcart/skins/customer/modules/CDev/MarketPrice/details/market_price.twig */
class __TwigTemplate_0ed740556d1230ed6d6767a15fed63e08b0c20a155d50c0413e226ab80e9f932 extends \XLite\Core\Templating\Twig\Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 6
        echo "
";
        // line 7
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('t')->getCallable(), array("Market price")), "html", null, true);
        echo ": <span class=\"value\">";
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "formatPrice", array(0 => $this->getAttribute($this->getAttribute((isset($context["this"]) ? $context["this"] : null), "product", array()), "getMarketPrice", array(), "method"), 1 => $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "null", array()), 2 => 1), "method"), "html", null, true);
        echo "</span>,
";
    }

    public function getTemplateName()
    {
        return "/var/www/html/xcart/skins/customer/modules/CDev/MarketPrice/details/market_price.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  22 => 7,  19 => 6,);
    }
}
/* {##*/
/*  # Market price (internal list element)*/
/*  #*/
/*  # @ListChild (list="product.plain_price.tail.market_price.text", weight="100")*/
/*  #}*/
/* */
/* {{ t('Market price') }}: <span class="value">{{ this.formatPrice(this.product.getMarketPrice(), this.null, 1) }}</span>,*/
/* */
