<?php

/* button/addon/install_module_selected.twig */
class __TwigTemplate_43f320708c8076b0726f8d88b3a6d86319004a0104b7b4568ee92d0a03068ddf extends \XLite\Core\Templating\Twig\Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 4
        if ($this->getAttribute((isset($context["this"]) ? $context["this"] : null), "moduleId", array())) {
            // line 5
            echo "<div class=\"";
            echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getParam", array(0 => "style"), "method"), "html", null, true);
            echo "\" id=\"module-box-";
            echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "moduleId", array()), "html", null, true);
            echo "\">
  <span class=\"module-name\">";
            // line 6
            echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getModuleName", array(0 => $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "moduleId", array())), "method"), "html", null, true);
            echo "</span>
  <div class=\"remove-action\"><span class=\"info\">";
            // line 7
            echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "moduleId", array()), "html", null, true);
            echo "</span></div>
</div>
";
        } else {
            // line 10
            echo "  <div class=\"";
            echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getParam", array(0 => "style"), "method"), "html", null, true);
            echo "\">
    <span class=\"module-name\"></span>
    <div class=\"remove-action\"><span class=\"info\"></span></div>
  </div>
";
        }
    }

    public function getTemplateName()
    {
        return "button/addon/install_module_selected.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  38 => 10,  32 => 7,  28 => 6,  21 => 5,  19 => 4,);
    }
}
/* {##*/
/*  # Selected module link*/
/*  #}*/
/* {% if this.moduleId %}*/
/* <div class="{{ this.getParam('style') }}" id="module-box-{{ this.moduleId }}">*/
/*   <span class="module-name">{{ this.getModuleName(this.moduleId) }}</span>*/
/*   <div class="remove-action"><span class="info">{{ this.moduleId }}</span></div>*/
/* </div>*/
/* {% else %}*/
/*   <div class="{{ this.getParam('style') }}">*/
/*     <span class="module-name"></span>*/
/*     <div class="remove-action"><span class="info"></span></div>*/
/*   </div>*/
/* {% endif %}*/
/* */
