<?php

/* upgrade/step/prepare/buttons/body.twig */
class __TwigTemplate_e3788de0ea46e608659e422ecc94dfc5cfe579b2250c2c8e91ac7ba6b3b83fab extends \XLite\Core\Templating\Twig\Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 4
        echo "<form action=\"";
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('url')->getCallable(), array($this->env, $context)), "html", null, true);
        echo "\" method=\"post\">
  <input type=\"hidden\" name=\"target\" value=\"upgrade\" />
  <input type=\"hidden\" name=\"action\" value=\"download\" />
  <input type=\"hidden\" name=\"mode\"   value=\"download_updates\" />

  ";
        // line 9
        if ( !$this->getAttribute((isset($context["this"]) ? $context["this"] : null), "isFreeSpaceCheckAvailable", array(), "method")) {
            // line 10
            echo "    <div class=\"free-space-warning-section\">
        <h2>";
            // line 11
            echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('t')->getCallable(), array("Check for available free disk space has failed")), "html", null, true);
            echo "</h2>
        <div>";
            // line 12
            echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('t')->getCallable(), array("You should check available free disk space yourself before continuing upgrade")), "html", null, true);
            echo "</div>
    </div>
  ";
        }
        // line 15
        echo "
  ";
        // line 16
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('widget')->getCallable(), array($this->env, $context, array(0 => "\\XLite\\View\\FormField\\Input\\FormId"))), "html", null, true);
        echo "

  <div class=\"incompatible-list\">
    ";
        // line 19
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('widget')->getCallable(), array($this->env, $context, array(0 => "\\XLite\\View\\Upgrade\\Step\\Prepare\\IncompatibleEntries"))), "html", null, true);
        echo "
  </div>

  <div class=\"incompatible-list-actions\">
    ";
        // line 23
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('widget_list')->getCallable(), array($this->env, $context, array(0 => "sections", "type" => "inherited"))), "html", null, true);
        echo "
    <div class=\"clear\"></div>
  </div>
</form>
";
    }

    public function getTemplateName()
    {
        return "upgrade/step/prepare/buttons/body.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  59 => 23,  52 => 19,  46 => 16,  43 => 15,  37 => 12,  33 => 11,  30 => 10,  28 => 9,  19 => 4,);
    }
}
/* {##*/
/*  # Buttons*/
/*  #}*/
/* <form action="{{ url() }}" method="post">*/
/*   <input type="hidden" name="target" value="upgrade" />*/
/*   <input type="hidden" name="action" value="download" />*/
/*   <input type="hidden" name="mode"   value="download_updates" />*/
/* */
/*   {% if not this.isFreeSpaceCheckAvailable() %}*/
/*     <div class="free-space-warning-section">*/
/*         <h2>{{ t('Check for available free disk space has failed') }}</h2>*/
/*         <div>{{ t('You should check available free disk space yourself before continuing upgrade') }}</div>*/
/*     </div>*/
/*   {% endif %}*/
/* */
/*   {{ widget('\\XLite\\View\\FormField\\Input\\FormId') }}*/
/* */
/*   <div class="incompatible-list">*/
/*     {{ widget('\\XLite\\View\\Upgrade\\Step\\Prepare\\IncompatibleEntries') }}*/
/*   </div>*/
/* */
/*   <div class="incompatible-list-actions">*/
/*     {{ widget_list('sections', type='inherited') }}*/
/*     <div class="clear"></div>*/
/*   </div>*/
/* </form>*/
/* */
