<?php

/* button/install_modules.twig */
class __TwigTemplate_a290a24c6f4c295dff88b0411722e387da2cbe70826d2b5539513c9e077175d6 extends \XLite\Core\Templating\Twig\Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 4
        echo "
<button type=\"button\" class=\"";
        // line 5
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getClass", array(), "method"), "html", null, true);
        echo "\">
  ";
        // line 6
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "displayCommentedData", array(0 => $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getURLParams", array(), "method")), "method"), "html", null, true);
        echo "
  <span>";
        // line 7
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('t')->getCallable(), array($this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getButtonContent", array(), "method"))), "html", null, true);
        echo "</span>
  ";
        // line 8
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getModulesToInstall", array(), "method"));
        foreach ($context['_seq'] as $context["id"] => $context["val"]) {
            // line 9
            echo "  <input type=\"hidden\" name=\"moduleIds[]\" value=\"";
            echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $context["val"], "html", null, true);
            echo "\" id=\"moduleids_";
            echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $context["val"], "html", null, true);
            echo "\" />
  ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['id'], $context['val'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 11
        echo "</button>
";
    }

    public function getTemplateName()
    {
        return "button/install_modules.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 11,  38 => 9,  34 => 8,  30 => 7,  26 => 6,  22 => 5,  19 => 4,);
    }
}
/* {##*/
/*  # Popup button*/
/*  #}*/
/* */
/* <button type="button" class="{{ this.getClass() }}">*/
/*   {{ this.displayCommentedData(this.getURLParams()) }}*/
/*   <span>{{ t(this.getButtonContent()) }}</span>*/
/*   {% for id, val in this.getModulesToInstall() %}*/
/*   <input type="hidden" name="moduleIds[]" value="{{ val }}" id="moduleids_{{ val }}" />*/
/*   {% endfor %}*/
/* </button>*/
/* */
