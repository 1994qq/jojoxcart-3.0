<?php

/* modules/XC/Onboarding/wizard_steps/payment/body.twig */
class __TwigTemplate_442bb27f024da397a8f04bce77fc015111fab59127a96c56cf84ad80a280946d extends \XLite\Core\Templating\Twig\Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 4
        echo "
<div class=\"onboarding-wizard-step step-";
        // line 5
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getStepIndex", array(), "method"), "html", null, true);
        echo "\"
     v-show=\"isCurrentStep('";
        // line 6
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getStepIndex", array(), "method"), "html", null, true);
        echo "')\"
     :transition=\"stepTransition\">
  <xlite-wizard-step-payment inline-template>
    <div class=\"step-contents\">
      <h2 class=\"heading\">";
        // line 10
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('t')->getCallable(), array("Manage your payment options")), "html", null, true);
        echo "</h2>
      <p class=\"text\">";
        // line 11
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('t')->getCallable(), array("120+ supported payment gateways, such as PayPal, Authorize.net and etc.")), "html", null, true);
        echo "</p>

      <div class=\"methods\">
        ";
        // line 14
        if ($this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getMethod", array(), "method")) {
            // line 15
            echo "          <div class=\"offline\">
            <div class=\"method-type\">
              ";
            // line 17
            echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('t')->getCallable(), array("Offline method")), "html", null, true);
            echo "
            </div>

            <div class=\"image\">";
            // line 20
            echo call_user_func_array($this->env->getFunction('svg')->getCallable(), array($this->env, $context, "modules/XC/Onboarding/images/icon-phone-order.svg"));
            echo "</div>

            <div class=\"method-name\">
              ";
            // line 23
            echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getOfflineMethodName", array(), "method"), "html", null, true);
            echo "
            </div>

            <div class=\"note\">
              ";
            // line 27
            echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('t')->getCallable(), array("Some people simply prefer to place their orders by phone — let them do that.")), "html", null, true);
            echo "
            </div>

            <div class=\"switcher";
            // line 30
            if ($this->getAttribute((isset($context["this"]) ? $context["this"] : null), "isOfflineMethodEnabled", array(), "method")) {
                echo " enabled";
            }
            echo "\">
              <span class=\"inactive\">";
            // line 31
            echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('t')->getCallable(), array("checkbox.onoff.off")), "html", null, true);
            echo "</span>
              <a href=\"#\" @click.prevent=\"switchOfflineMethod(";
            // line 32
            echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getOfflineMethodId", array(), "method"), "html", null, true);
            echo ", \$event)\">
                <div>
                  <span class=\"fa fa-check\"></span>
                </div>
              </a>
              <span class=\"active\">";
            // line 37
            echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('t')->getCallable(), array("checkbox.onoff.on")), "html", null, true);
            echo "</span>
            </div>
          </div>
        ";
        }
        // line 41
        echo "
        ";
        // line 42
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getOnlineWidgets", array(), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["widgetClass"]) {
            // line 43
            echo "          ";
            echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('widget')->getCallable(), array($this->env, $context, array(0 => $context["widgetClass"]))), "html", null, true);
            echo "
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['widgetClass'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 45
        echo "      </div>

      <div class=\"buttons\">
        <div class=\"more-button\">
          ";
        // line 49
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('widget')->getCallable(), array($this->env, $context, array(0 => "\\XLite\\View\\Button\\Link", "label" => "Set up offline & online payments", "location" => $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getMoreSettingsLocation", array(), "method"), "blank" => 1))), "html", null, true);
        echo "
        </div>
        <div class=\"next-step\">
          ";
        // line 52
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('widget')->getCallable(), array($this->env, $context, array(0 => "\\XLite\\View\\Button\\Regular", "label" => "Proceed to the next step", "style" => "regular-main-button", "attributes" => array("@click" => "goToNextStep"), "jsCode" => "null;"))), "html", null, true);
        echo "
        </div>
      </div>
    </div>
  </xlite-wizard-step-payment>
</div>";
    }

    public function getTemplateName()
    {
        return "modules/XC/Onboarding/wizard_steps/payment/body.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  127 => 52,  121 => 49,  115 => 45,  106 => 43,  102 => 42,  99 => 41,  92 => 37,  84 => 32,  80 => 31,  74 => 30,  68 => 27,  61 => 23,  55 => 20,  49 => 17,  45 => 15,  43 => 14,  37 => 11,  33 => 10,  26 => 6,  22 => 5,  19 => 4,);
    }
}
/* {##*/
/*  # Payment step*/
/*  #}*/
/* */
/* <div class="onboarding-wizard-step step-{{ this.getStepIndex() }}"*/
/*      v-show="isCurrentStep('{{ this.getStepIndex() }}')"*/
/*      :transition="stepTransition">*/
/*   <xlite-wizard-step-payment inline-template>*/
/*     <div class="step-contents">*/
/*       <h2 class="heading">{{ t("Manage your payment options") }}</h2>*/
/*       <p class="text">{{ t("120+ supported payment gateways, such as PayPal, Authorize.net and etc.") }}</p>*/
/* */
/*       <div class="methods">*/
/*         {% if this.getMethod() %}*/
/*           <div class="offline">*/
/*             <div class="method-type">*/
/*               {{ t('Offline method') }}*/
/*             </div>*/
/* */
/*             <div class="image">{{ svg('modules/XC/Onboarding/images/icon-phone-order.svg')|raw }}</div>*/
/* */
/*             <div class="method-name">*/
/*               {{ this.getOfflineMethodName() }}*/
/*             </div>*/
/* */
/*             <div class="note">*/
/*               {{ t('Some people simply prefer to place their orders by phone — let them do that.') }}*/
/*             </div>*/
/* */
/*             <div class="switcher{% if this.isOfflineMethodEnabled() %} enabled{% endif %}">*/
/*               <span class="inactive">{{ t('checkbox.onoff.off') }}</span>*/
/*               <a href="#" @click.prevent="switchOfflineMethod({{ this.getOfflineMethodId() }}, $event)">*/
/*                 <div>*/
/*                   <span class="fa fa-check"></span>*/
/*                 </div>*/
/*               </a>*/
/*               <span class="active">{{ t('checkbox.onoff.on') }}</span>*/
/*             </div>*/
/*           </div>*/
/*         {% endif %}*/
/* */
/*         {% for widgetClass in this.getOnlineWidgets() %}*/
/*           {{ widget(widgetClass) }}*/
/*         {% endfor %}*/
/*       </div>*/
/* */
/*       <div class="buttons">*/
/*         <div class="more-button">*/
/*           {{ widget('\\XLite\\View\\Button\\Link', label='Set up offline & online payments', location=this.getMoreSettingsLocation(), blank=1) }}*/
/*         </div>*/
/*         <div class="next-step">*/
/*           {{ widget('\\XLite\\View\\Button\\Regular', label='Proceed to the next step', style='regular-main-button', attributes={'@click': 'goToNextStep'}, jsCode="null;") }}*/
/*         </div>*/
/*       </div>*/
/*     </div>*/
/*   </xlite-wizard-step-payment>*/
/* </div>*/
