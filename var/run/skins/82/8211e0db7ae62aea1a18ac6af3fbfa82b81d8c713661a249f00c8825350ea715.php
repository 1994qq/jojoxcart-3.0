<?php

/* modules/XC/Onboarding/wizard_steps/shipping/body.twig */
class __TwigTemplate_c9ffde1ec24185ebf942f856fe93c5e2e6f263e5f649f1b346b348556323d3b5 extends \XLite\Core\Templating\Twig\Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 4
        echo "
<div class=\"onboarding-wizard-step step-";
        // line 5
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getStepIndex", array(), "method"), "html", null, true);
        echo "\"
     v-show=\"isCurrentStep('";
        // line 6
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getStepIndex", array(), "method"), "html", null, true);
        echo "')\"
     :transition=\"stepTransition\">
  <xlite-wizard-step-shipping inline-template>
    <div class=\"step-contents\">
      <h2 class=\"heading\">";
        // line 10
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('t')->getCallable(), array("Do you need to ship your products?")), "html", null, true);
        echo "</h2>

      <div class=\"step-content\" v-data='{ \"shipping_enabled\": ";
        // line 12
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getShippingEnabled", array(), "method"), "html", null, true);
        echo " }'>
        <div class=\"go-to-shipping\">
          <div class=\"image\">
            <img src=\"";
        // line 15
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('asset')->getCallable(), array("modules/XC/Onboarding/images/shipping.png")), "html", null, true);
        echo "\"
                 srcset=\"";
        // line 16
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('asset')->getCallable(), array("modules/XC/Onboarding/images/shipping@2x.png")), "html", null, true);
        echo " 2x\">
          </div>
          <div class=\"note\">
            ";
        // line 19
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('t')->getCallable(), array("Yes, I sell tangible goods that needed to be shipped")), "html", null, true);
        echo "
          </div>
          <div class=\"button\">
            ";
        // line 22
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('widget')->getCallable(), array($this->env, $context, array(0 => "\\XLite\\View\\Button\\Regular", "label" => "Set up shipping", "style" => "regular-main-button", "attributes" => array("@click" => "goToShipping"), "jsCode" => "null;"))), "html", null, true);
        echo "
          </div>
        </div>
        <div class=\"separator\">
          ";
        // line 26
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('t')->getCallable(), array("or")), "html", null, true);
        echo "
        </div>
        <div class=\"skip-shipping\">
          <div class=\"image\">
            <img src=\"";
        // line 30
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('asset')->getCallable(), array("modules/XC/Onboarding/images/digital-icons.png")), "html", null, true);
        echo "\">
          </div>
          <div class=\"note\">
            ";
        // line 33
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('t')->getCallable(), array("I sell digital goods or services and do not need to set up shipping")), "html", null, true);
        echo "
          </div>
          <div class=\"button\">
            ";
        // line 36
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('widget')->getCallable(), array($this->env, $context, array(0 => "\\XLite\\View\\Button\\Regular", "label" => "Skip Shipping Step", "style" => "regular-main-button", "attributes" => array("@click" => "skipShipping"), "jsCode" => "null;"))), "html", null, true);
        echo "
          </div>
        </div>
      </div>
    </div>
  </xlite-wizard-step-shipping>
</div>";
    }

    public function getTemplateName()
    {
        return "modules/XC/Onboarding/wizard_steps/shipping/body.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  86 => 36,  80 => 33,  74 => 30,  67 => 26,  60 => 22,  54 => 19,  48 => 16,  44 => 15,  38 => 12,  33 => 10,  26 => 6,  22 => 5,  19 => 4,);
    }
}
/* {##*/
/*  # Shipping step*/
/*  #}*/
/* */
/* <div class="onboarding-wizard-step step-{{ this.getStepIndex() }}"*/
/*      v-show="isCurrentStep('{{ this.getStepIndex() }}')"*/
/*      :transition="stepTransition">*/
/*   <xlite-wizard-step-shipping inline-template>*/
/*     <div class="step-contents">*/
/*       <h2 class="heading">{{ t("Do you need to ship your products?") }}</h2>*/
/* */
/*       <div class="step-content" v-data='{ "shipping_enabled": {{ this.getShippingEnabled() }} }'>*/
/*         <div class="go-to-shipping">*/
/*           <div class="image">*/
/*             <img src="{{ asset('modules/XC/Onboarding/images/shipping.png') }}"*/
/*                  srcset="{{ asset('modules/XC/Onboarding/images/shipping@2x.png') }} 2x">*/
/*           </div>*/
/*           <div class="note">*/
/*             {{ t("Yes, I sell tangible goods that needed to be shipped") }}*/
/*           </div>*/
/*           <div class="button">*/
/*             {{ widget('\\XLite\\View\\Button\\Regular', label='Set up shipping', style='regular-main-button', attributes={'@click': 'goToShipping'}, jsCode="null;") }}*/
/*           </div>*/
/*         </div>*/
/*         <div class="separator">*/
/*           {{ t('or') }}*/
/*         </div>*/
/*         <div class="skip-shipping">*/
/*           <div class="image">*/
/*             <img src="{{ asset('modules/XC/Onboarding/images/digital-icons.png') }}">*/
/*           </div>*/
/*           <div class="note">*/
/*             {{ t("I sell digital goods or services and do not need to set up shipping") }}*/
/*           </div>*/
/*           <div class="button">*/
/*             {{ widget('\\XLite\\View\\Button\\Regular', label='Skip Shipping Step', style='regular-main-button', attributes={'@click': 'skipShipping'}, jsCode="null;") }}*/
/*           </div>*/
/*         </div>*/
/*       </div>*/
/*     </div>*/
/*   </xlite-wizard-step-shipping>*/
/* </div>*/
