<?php

/* /var/www/html/xcart/skins/admin/header/parts/preloaded_labels.twig */
class __TwigTemplate_be347a66c08e8e085a0c8306c8b5da323558b2f7ce1a09c7fddaf6ec0f5e9a39 extends \XLite\Core\Templating\Twig\Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 6
        echo "
<script type=\"text/javascript\">
  window.xlite_preloaded_labels =";
        // line 8
        echo $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getPreloadedLabelsJSON", array(), "method");
        echo ";
</script>
";
    }

    public function getTemplateName()
    {
        return "/var/www/html/xcart/skins/admin/header/parts/preloaded_labels.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  23 => 8,  19 => 6,);
    }
}
/* {##*/
/*  # Head list children*/
/*  #*/
/*  # @ListChild (list="head", weight="1500")*/
/*  #}*/
/* */
/* <script type="text/javascript">*/
/*   window.xlite_preloaded_labels ={{ this.getPreloadedLabelsJSON()|raw }};*/
/* </script>*/
/* */
