<?php

/* modules/XC/Onboarding/form_model/product/other_options.twig */
class __TwigTemplate_394b8b0f1c03331ee28a3d56d561f4ab2de601f1dbd5e258958d99a74027c8cf extends \XLite\Core\Templating\Twig\Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<a class=\"options-link\" href=\"";
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getOtherOptionsUrl", array(), "method"), "html", null, true);
        echo "\" :href=\"optionsUrl\"><span>";
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('t')->getCallable(), array("Add other options")), "html", null, true);
        echo "</span></a> ";
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('t')->getCallable(), array("(like categories, weight, description, color)")), "html", null, true);
    }

    public function getTemplateName()
    {
        return "modules/XC/Onboarding/form_model/product/other_options.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  19 => 1,);
    }
}
/* <a class="options-link" href="{{ this.getOtherOptionsUrl() }}" :href="optionsUrl"><span>{{ t('Add other options') }}</span></a> {{ t('(like categories, weight, description, color)') }}*/
