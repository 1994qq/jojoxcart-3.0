<?php

/* /var/www/html/xcart/skins/customer/product/details/parts/common.notify-on-price-drop.twig */
class __TwigTemplate_6f217fa8e7afb3f140b91ccc9b643189a4464ef6d8445b518364328fc6fe9203 extends \XLite\Core\Templating\Twig\Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
    }

    public function getTemplateName()
    {
        return "/var/www/html/xcart/skins/customer/product/details/parts/common.notify-on-price-drop.twig";
    }

    public function getDebugInfo()
    {
        return array ();
    }
}
/* {##*/
/*  # "Notify when price drops" link*/
/*  #*/
/*  # @ListChild (list="product.details.page.info", weight="42")*/
/*  #}*/
/* */
