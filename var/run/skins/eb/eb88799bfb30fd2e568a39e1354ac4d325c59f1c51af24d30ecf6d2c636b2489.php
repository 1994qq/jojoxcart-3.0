<?php

/* items_list/module/filter/selector/body.twig */
class __TwigTemplate_9da364315e632d1fa66f95f67af14fb9911ebd2fe7f34c92f2bd81cfde5838f6 extends \XLite\Core\Templating\Twig\Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 4
        echo "<div class=\"";
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getStyle", array(), "method"), "html", null, true);
        echo "\">
  <div class=\"title\">
    ";
        // line 6
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getCaption", array(), "method"), "html", null, true);
        echo "
    <i class=\"fa fa-angle-right\"></i>
  </div>
  <div class=\"selector-items box\">
    ";
        // line 10
        if ($this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getItems", array(), "method")) {
            // line 11
            echo "      <ul class=\"menu\">
        ";
            // line 12
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getItems", array(), "method"));
            foreach ($context['_seq'] as $context["_key"] => $context["item"]) {
                // line 13
                echo "          <li class=\"";
                echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getItemStyle", array(0 => $context["item"]), "method"), "html", null, true);
                echo "\">
            <div class=\"line\">
              ";
                // line 15
                if ($this->getAttribute((isset($context["this"]) ? $context["this"] : null), "isItemSelected", array(0 => $context["item"]), "method")) {
                    // line 16
                    echo "                <span class=\"link\"><span>";
                    echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getItemLabel", array(0 => $context["item"]), "method"), "html", null, true);
                    echo "</span></span>
              ";
                } else {
                    // line 18
                    echo "              <a href=\"";
                    echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getItemURL", array(0 => $context["item"]), "method"), "html", null, true);
                    echo "\" class=\"link\" title=\"";
                    echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getItemLabel", array(0 => $context["item"]), "method"), "html", null, true);
                    echo "\">
                <span>";
                    // line 19
                    echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getItemLabel", array(0 => $context["item"]), "method"), "html", null, true);
                    echo "</span>
              </a>
              ";
                }
                // line 22
                echo "            </div>
          </li>
        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['item'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 25
            echo "      </ul>
    ";
        }
        // line 27
        echo "  </div>
</div>
";
    }

    public function getTemplateName()
    {
        return "items_list/module/filter/selector/body.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  80 => 27,  76 => 25,  68 => 22,  62 => 19,  55 => 18,  49 => 16,  47 => 15,  41 => 13,  37 => 12,  34 => 11,  32 => 10,  25 => 6,  19 => 4,);
    }
}
/* {##*/
/*  # Modules list*/
/*  #}*/
/* <div class="{{ this.getStyle() }}">*/
/*   <div class="title">*/
/*     {{ this.getCaption() }}*/
/*     <i class="fa fa-angle-right"></i>*/
/*   </div>*/
/*   <div class="selector-items box">*/
/*     {% if this.getItems() %}*/
/*       <ul class="menu">*/
/*         {% for item in this.getItems() %}*/
/*           <li class="{{ this.getItemStyle(item) }}">*/
/*             <div class="line">*/
/*               {% if this.isItemSelected(item) %}*/
/*                 <span class="link"><span>{{ this.getItemLabel(item) }}</span></span>*/
/*               {% else %}*/
/*               <a href="{{ this.getItemURL(item) }}" class="link" title="{{ this.getItemLabel(item) }}">*/
/*                 <span>{{ this.getItemLabel(item) }}</span>*/
/*               </a>*/
/*               {% endif %}*/
/*             </div>*/
/*           </li>*/
/*         {% endfor %}*/
/*       </ul>*/
/*     {% endif %}*/
/*   </div>*/
/* </div>*/
/* */
