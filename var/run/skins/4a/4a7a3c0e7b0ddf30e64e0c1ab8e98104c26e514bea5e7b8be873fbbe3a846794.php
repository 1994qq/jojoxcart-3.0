<?php

/* modules/XC/Onboarding/wizard_steps/shipping_done/body.twig */
class __TwigTemplate_97c600f21e632cb3658e825fd521d780d045120d8addf4182cb39804c4297fbf extends \XLite\Core\Templating\Twig\Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 4
        echo "
<div class=\"onboarding-wizard-step step-";
        // line 5
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getStepIndex", array(), "method"), "html", null, true);
        echo "\"
     v-show=\"isCurrentStep('";
        // line 6
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getStepIndex", array(), "method"), "html", null, true);
        echo "')\"
     :transition=\"stepTransition\">
  <div class=\"step-contents\">
    <h2 class=\"heading\">";
        // line 9
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('t')->getCallable(), array("You've successfully set up shipping areas and rates for your store")), "html", null, true);
        echo "</h2>
    <p class=\"text\">
      ";
        // line 11
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('t')->getCallable(), array("Now you can define how shipping rates should be calculated based on  delivery time, weight and other parameters.")), "html", null, true);
        echo "
      <a href=\"";
        // line 12
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getAdvancedShippingSettingsLink", array(), "method"), "html", null, true);
        echo "\" target=\"_blank\">";
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('t')->getCallable(), array("Advanced shipping settings")), "html", null, true);
        echo "</a>
    </p>

    <div class=\"placeholder\">
      <img class=\"bg\" src=\"";
        // line 16
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('asset')->getCallable(), array("modules/XC/Onboarding/images/shipping-done-bg.png")), "html", null, true);
        echo "\"
           srcset=\"";
        // line 17
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('asset')->getCallable(), array("modules/XC/Onboarding/images/shipping-done-bg@2x.png")), "html", null, true);
        echo " 2x\">
      <img class=\"sm-box\" src=\"";
        // line 18
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('asset')->getCallable(), array("modules/XC/Onboarding/images/shipping-done-sm-box.png")), "html", null, true);
        echo "\"
           srcset=\"";
        // line 19
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('asset')->getCallable(), array("modules/XC/Onboarding/images/shipping-done-sm-box@2x.png")), "html", null, true);
        echo " 2x\">
      <img class=\"lg-box\" src=\"";
        // line 20
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('asset')->getCallable(), array("modules/XC/Onboarding/images/shipping-done-lg-box.png")), "html", null, true);
        echo "\"
           srcset=\"";
        // line 21
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('asset')->getCallable(), array("modules/XC/Onboarding/images/shipping-done-lg-box@2x.png")), "html", null, true);
        echo " 2x\">
    </div>

    <div class=\"buttons\">
      <div class=\"next-step\">
        ";
        // line 26
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('widget')->getCallable(), array($this->env, $context, array(0 => "\\XLite\\View\\Button\\Regular", "label" => "Save and go to the next step", "style" => "regular-main-button", "attributes" => array("@click" => "goToNextStep"), "jsCode" => "null;"))), "html", null, true);
        echo "
      </div>
    </div>
  </div>
</div>";
    }

    public function getTemplateName()
    {
        return "modules/XC/Onboarding/wizard_steps/shipping_done/body.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  78 => 26,  70 => 21,  66 => 20,  62 => 19,  58 => 18,  54 => 17,  50 => 16,  41 => 12,  37 => 11,  32 => 9,  26 => 6,  22 => 5,  19 => 4,);
    }
}
/* {##*/
/*  # Shipping done step*/
/*  #}*/
/* */
/* <div class="onboarding-wizard-step step-{{ this.getStepIndex() }}"*/
/*      v-show="isCurrentStep('{{ this.getStepIndex() }}')"*/
/*      :transition="stepTransition">*/
/*   <div class="step-contents">*/
/*     <h2 class="heading">{{ t("You've successfully set up shipping areas and rates for your store") }}</h2>*/
/*     <p class="text">*/
/*       {{ t("Now you can define how shipping rates should be calculated based on  delivery time, weight and other parameters.") }}*/
/*       <a href="{{ this.getAdvancedShippingSettingsLink() }}" target="_blank">{{ t("Advanced shipping settings") }}</a>*/
/*     </p>*/
/* */
/*     <div class="placeholder">*/
/*       <img class="bg" src="{{ asset('modules/XC/Onboarding/images/shipping-done-bg.png') }}"*/
/*            srcset="{{ asset('modules/XC/Onboarding/images/shipping-done-bg@2x.png') }} 2x">*/
/*       <img class="sm-box" src="{{ asset('modules/XC/Onboarding/images/shipping-done-sm-box.png') }}"*/
/*            srcset="{{ asset('modules/XC/Onboarding/images/shipping-done-sm-box@2x.png') }} 2x">*/
/*       <img class="lg-box" src="{{ asset('modules/XC/Onboarding/images/shipping-done-lg-box.png') }}"*/
/*            srcset="{{ asset('modules/XC/Onboarding/images/shipping-done-lg-box@2x.png') }} 2x">*/
/*     </div>*/
/* */
/*     <div class="buttons">*/
/*       <div class="next-step">*/
/*         {{ widget('\\XLite\\View\\Button\\Regular', label='Save and go to the next step', style='regular-main-button', attributes={'@click': 'goToNextStep'}, jsCode="null;") }}*/
/*       </div>*/
/*     </div>*/
/*   </div>*/
/* </div>*/
