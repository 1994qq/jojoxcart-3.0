<?php

/* form_field/file_uploader/single.twig */
class __TwigTemplate_1ccfbb57a595b2ffc8c8384784fa1cfa55a99f87f00c38c13222385842df464a extends \XLite\Core\Templating\Twig\Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 4
        echo "
<div";
        // line 5
        echo $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getDataCode", array(), "method");
        echo ">
  ";
        // line 6
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('widget')->getCallable(), array($this->env, $context, array(0 => $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getFileUploaderWidget", array(), "method"), "object" => $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getValue", array(), "method"), "maxWidth" => $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getMaxWidth", array(), "method"), "maxHeight" => $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getMaxHeight", array(), "method"), "isImage" => $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "isImage", array(), "method"), "fieldName" => $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getName", array(), "method"), "isViaUrlAllowed" => $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "isViaUrlAllowed", array(), "method"), "helpMessage" => (((isset($context["object"]) ? $context["object"] : null)) ? (null) : ($this->getAttribute((isset($context["this"]) ? $context["this"] : null), "helpMessage", array())))))), "html", null, true);
        echo "
</div>
";
    }

    public function getTemplateName()
    {
        return "form_field/file_uploader/single.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  26 => 6,  22 => 5,  19 => 4,);
    }
}
/* {##*/
/*  # File upload template*/
/*  #}*/
/* */
/* <div{{ this.getDataCode()|raw }}>*/
/*   {{ widget(this.getFileUploaderWidget(), object=this.getValue(), maxWidth=this.getMaxWidth(), maxHeight=this.getMaxHeight(), isImage=this.isImage(), fieldName=this.getName(), isViaUrlAllowed=this.isViaUrlAllowed(), helpMessage=(object ? null : this.helpMessage) ) }}*/
/* </div>*/
/* */
