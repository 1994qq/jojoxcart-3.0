<?php

/* items_list/module/install/body.twig */
class __TwigTemplate_6caeaca37836ca573155120be241fe2157a0571c57d3cfe99e7acef827f0cd6e extends \XLite\Core\Templating\Twig\Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 4
        echo "<table cellspacing=\"0\" cellpadding=\"0\" class=\"data-table items-list modules-list";
        if ($this->getAttribute((isset($context["this"]) ? $context["this"] : null), "promoBanner", array())) {
            echo " module-list-has-promo-banner";
        }
        echo "\">
  ";
        // line 5
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getPageData", array(), "method"));
        foreach ($context['_seq'] as $context["idx"] => $context["module"]) {
            // line 6
            echo "    <tr class=\"";
            echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getModuleClassesCSS", array(0 => $context["module"]), "method"), "html", null, true);
            echo "\">
      ";
            // line 7
            echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('widget_list')->getCallable(), array($this->env, $context, array(0 => "columns", "type" => "inherited", "module" => $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getModuleFromMarketplace", array(0 => $context["module"]), "method")))), "html", null, true);
            echo "
    </tr>
  ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['idx'], $context['module'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 10
        echo "</table>
";
    }

    public function getTemplateName()
    {
        return "items_list/module/install/body.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  44 => 10,  35 => 7,  30 => 6,  26 => 5,  19 => 4,);
    }
}
/* {##*/
/*  # Modules list*/
/*  #}*/
/* <table cellspacing="0" cellpadding="0" class="data-table items-list modules-list{% if this.promoBanner %} module-list-has-promo-banner{% endif %}">*/
/*   {% for idx, module in this.getPageData() %}*/
/*     <tr class="{{ this.getModuleClassesCSS(module) }}">*/
/*       {{ widget_list('columns', type='inherited', module=this.getModuleFromMarketplace(module)) }}*/
/*     </tr>*/
/*   {% endfor %}*/
/* </table>*/
/* */
