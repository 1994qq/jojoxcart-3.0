<?php

/* menu/marketplace/top_menu_node.twig */
class __TwigTemplate_1b0121223193ce9b93886b232516e3f0067fd507c161552d9a69617aec8241f7 extends \XLite\Core\Templating\Twig\Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 4
        echo "<div class=\"marketplace-menu menu notification\">
  <div class=\"icon\">
    <a href=\"";
        // line 6
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "buildUrl", array(0 => "addons_list_marketplace", 1 => "", 2 => array("landing" => 1)), "method"), "html", null, true);
        echo "\">
      ";
        // line 7
        echo $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getSVGImage", array(0 => "images/marketplace.svg"), "method");
        echo "
      <div class=\"unread-mark\"></div>
    </a>
  </div>
  <div class=\"box\">";
        // line 11
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('widget')->getCallable(), array($this->env, $context, array(0 => "XLite\\View\\Menu\\Admin\\Marketplace\\LazyLoad"))), "html", null, true);
        echo "</div>
</div>
";
    }

    public function getTemplateName()
    {
        return "menu/marketplace/top_menu_node.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  34 => 11,  27 => 7,  23 => 6,  19 => 4,);
    }
}
/* {##*/
/*  # Quick menu top menu node*/
/*  #}*/
/* <div class="marketplace-menu menu notification">*/
/*   <div class="icon">*/
/*     <a href="{{ this.buildUrl('addons_list_marketplace', '', {landing: 1}) }}">*/
/*       {{ this.getSVGImage('images/marketplace.svg')|raw }}*/
/*       <div class="unread-mark"></div>*/
/*     </a>*/
/*   </div>*/
/*   <div class="box">{{ widget('XLite\\View\\Menu\\Admin\\Marketplace\\LazyLoad') }}</div>*/
/* </div>*/
/* */
