<?php

/* modules/XC/Onboarding/wizard_steps/add_product/body.twig */
class __TwigTemplate_205adad277fba85423dadfffec5c003f3c9e23eb646349395aa5e784c4a9b381 extends \XLite\Core\Templating\Twig\Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 4
        echo "
<div
  class=\"onboarding-wizard-step step-";
        // line 6
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getStepIndex", array(), "method"), "html", null, true);
        echo "\"
  v-show=\"isCurrentStep('";
        // line 7
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getStepIndex", array(), "method"), "html", null, true);
        echo "')\"
  :transition=\"stepTransition\">
  <xlite-wizard-step-add-product inline-template>
    <div class=\"step-contents\">
      <h2 class=\"heading\">";
        // line 11
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('t')->getCallable(), array("onboarding.add_product.heading")), "html", null, true);
        echo "</h2>
      <p class=\"text\">";
        // line 12
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('t')->getCallable(), array("onboarding.add_product.text")), "html", null, true);
        echo "</p>
      <div class=\"add-product-form\">
        ";
        // line 14
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('widget')->getCallable(), array($this->env, $context, array(0 => "XLite\\Module\\XC\\Onboarding\\View\\FormModel\\Product\\Simplified"))), "html", null, true);
        echo "
      </div>
    </div>
  </xlite-wizard-step-add-product>
</div>";
    }

    public function getTemplateName()
    {
        return "modules/XC/Onboarding/wizard_steps/add_product/body.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  43 => 14,  38 => 12,  34 => 11,  27 => 7,  23 => 6,  19 => 4,);
    }
}
/* {##*/
/*  # Add product step*/
/*  #}*/
/* */
/* <div*/
/*   class="onboarding-wizard-step step-{{ this.getStepIndex() }}"*/
/*   v-show="isCurrentStep('{{ this.getStepIndex() }}')"*/
/*   :transition="stepTransition">*/
/*   <xlite-wizard-step-add-product inline-template>*/
/*     <div class="step-contents">*/
/*       <h2 class="heading">{{ t('onboarding.add_product.heading') }}</h2>*/
/*       <p class="text">{{ t('onboarding.add_product.text') }}</p>*/
/*       <div class="add-product-form">*/
/*         {{ widget('XLite\\Module\\XC\\Onboarding\\View\\FormModel\\Product\\Simplified') }}*/
/*       </div>*/
/*     </div>*/
/*   </xlite-wizard-step-add-product>*/
/* </div>*/
