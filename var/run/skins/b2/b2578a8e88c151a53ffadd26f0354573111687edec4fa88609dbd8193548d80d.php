<?php

/* /var/www/html/xcart/skins/admin/upgrade/step/prepare/entries_list_update/parts/table/columns/version_separator.twig */
class __TwigTemplate_3f381aa66964f8a23f526bc5c47511641f7d13a5c97acf1b50c37bb350161356 extends \XLite\Core\Templating\Twig\Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 6
        echo "
<td class=\"version-separator\"></td>
";
    }

    public function getTemplateName()
    {
        return "/var/www/html/xcart/skins/admin/upgrade/step/prepare/entries_list_update/parts/table/columns/version_separator.twig";
    }

    public function getDebugInfo()
    {
        return array (  19 => 6,);
    }
}
/* {##*/
/*  # Entry version separator*/
/*  #*/
/*  # @ListChild (list="upgrade.step.prepare.entries_list_update.sections.table.columns", weight="400")*/
/*  #}*/
/* */
/* <td class="version-separator"></td>*/
/* */
