<?php

/* menu/quick_menu/top_menu_node.twig */
class __TwigTemplate_dc18c40ff80060549a2b325899393f2a73c2f1cb273d834973cb2ff80dcd6f60 extends \XLite\Core\Templating\Twig\Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 4
        echo "<div class=\"quick-menu-menu menu\">
  <div class=\"title\">
    ";
        // line 6
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('t')->getCallable(), array("Quick menu")), "html", null, true);
        echo "
    <i class=\"fa fa-angle-right\"></i>
  </div>
  <div class=\"box\">";
        // line 9
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('widget')->getCallable(), array($this->env, $context, array(0 => "XLite\\View\\Menu\\Admin\\QuickMenu\\Menu"))), "html", null, true);
        echo "</div>
</div>
";
    }

    public function getTemplateName()
    {
        return "menu/quick_menu/top_menu_node.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  29 => 9,  23 => 6,  19 => 4,);
    }
}
/* {##*/
/*  # Main logo*/
/*  #}*/
/* <div class="quick-menu-menu menu">*/
/*   <div class="title">*/
/*     {{ t('Quick menu') }}*/
/*     <i class="fa fa-angle-right"></i>*/
/*   </div>*/
/*   <div class="box">{{ widget('XLite\\View\\Menu\\Admin\\QuickMenu\\Menu') }}</div>*/
/* </div>*/
/* */
