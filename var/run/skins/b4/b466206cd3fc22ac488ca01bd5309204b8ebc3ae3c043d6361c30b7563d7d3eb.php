<?php

/* /var/www/html/xcart/skins/customer/items_list/order/parts/spec.break.twig */
class __TwigTemplate_c91b7783177eedde3bdd5afbf3f425db0fba34b3cd5410af2d2515a887255f97 extends \XLite\Core\Templating\Twig\Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 6
        echo "<li class=\"order-break-line\"></li>
";
    }

    public function getTemplateName()
    {
        return "/var/www/html/xcart/skins/customer/items_list/order/parts/spec.break.twig";
    }

    public function getDebugInfo()
    {
        return array (  19 => 6,);
    }
}
/* {##*/
/*  # Orders list item : spec : break line*/
/*  #*/
/*  # @ListChild (list="orders.children.spec", weight="400")*/
/*  #}*/
/* <li class="order-break-line"></li>*/
/* */
