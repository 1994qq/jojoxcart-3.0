<?php

/* modules/XC/Onboarding/wizard_steps/company_logo_added/body.twig */
class __TwigTemplate_bb9c24ddd6ff358cea6b8cc4103a48abc059dd56a2d8f847dece52e8573679ed extends \XLite\Core\Templating\Twig\Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 4
        echo "
<div
  class=\"onboarding-wizard-step step-";
        // line 6
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getStepIndex", array(), "method"), "html", null, true);
        echo "\"
  v-show=\"isCurrentStep('";
        // line 7
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getStepIndex", array(), "method"), "html", null, true);
        echo "')\"
  :transition=\"stepTransition\">
  <xlite-wizard-step-company-logo-added inline-template>
    <div class=\"step-contents\">
      <h2 class=\"heading\">";
        // line 11
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('t')->getCallable(), array("Good job! You company logo is added")), "html", null, true);
        echo "</h2>
      <p class=\"text\">";
        // line 12
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('t')->getCallable(), array("Here is the way it looks like on desktop & mobile devices. Do you like it?")), "html", null, true);
        echo "</p>
      ";
        // line 13
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('widget')->getCallable(), array($this->env, $context, array(0 => "XLite\\View\\Button\\SimpleLink", "label" => call_user_func_array($this->env->getFunction('t')->getCallable(), array("Upload a new logo")), "attributes" => array("@click" => "reuploadLogo"), "jsCode" => "null;"))), "html", null, true);
        echo "
      <div class=\"logo-showcase\">
        <div class=\"logo-image-wrapper\">
          <img src=\"";
        // line 16
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('asset')->getCallable(), array("modules/XC/Onboarding/images/logo-showcase.png")), "html", null, true);
        echo "\">
          <div class=\"logo-desktop-placeholder\" v-if=\"logoUrl\">
            <img :src=\"logoUrl\">
          </div>
          <div class=\"logo-mobile-placeholder\" v-if=\"logoUrl\">
            <img :src=\"logoUrl\">
          </div>
        </div>
      </div>
      <div class=\"buttons\">
        ";
        // line 26
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('widget')->getCallable(), array($this->env, $context, array(0 => "XLite\\View\\Button\\Regular", "label" => call_user_func_array($this->env->getFunction('t')->getCallable(), array("Save and go to the next step")), "style" => "regular-main-button", "attributes" => array("@click" => "save"), "jsCode" => "null;"))), "html", null, true);
        echo "
      </div>
    </div>
  </xlite-wizard-step-company-logo-added>
</div>";
    }

    public function getTemplateName()
    {
        return "modules/XC/Onboarding/wizard_steps/company_logo_added/body.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  61 => 26,  48 => 16,  42 => 13,  38 => 12,  34 => 11,  27 => 7,  23 => 6,  19 => 4,);
    }
}
/* {##*/
/*  # Company logo added step*/
/*  #}*/
/* */
/* <div*/
/*   class="onboarding-wizard-step step-{{ this.getStepIndex() }}"*/
/*   v-show="isCurrentStep('{{ this.getStepIndex() }}')"*/
/*   :transition="stepTransition">*/
/*   <xlite-wizard-step-company-logo-added inline-template>*/
/*     <div class="step-contents">*/
/*       <h2 class="heading">{{ t('Good job! You company logo is added') }}</h2>*/
/*       <p class="text">{{ t('Here is the way it looks like on desktop & mobile devices. Do you like it?') }}</p>*/
/*       {{ widget('XLite\\View\\Button\\SimpleLink', label=t("Upload a new logo"), attributes={'@click': 'reuploadLogo'}, jsCode="null;") }}*/
/*       <div class="logo-showcase">*/
/*         <div class="logo-image-wrapper">*/
/*           <img src="{{ asset('modules/XC/Onboarding/images/logo-showcase.png') }}">*/
/*           <div class="logo-desktop-placeholder" v-if="logoUrl">*/
/*             <img :src="logoUrl">*/
/*           </div>*/
/*           <div class="logo-mobile-placeholder" v-if="logoUrl">*/
/*             <img :src="logoUrl">*/
/*           </div>*/
/*         </div>*/
/*       </div>*/
/*       <div class="buttons">*/
/*         {{ widget('XLite\\View\\Button\\Regular', label=t("Save and go to the next step"), style='regular-main-button', attributes={'@click': 'save'}, jsCode="null;") }}*/
/*       </div>*/
/*     </div>*/
/*   </xlite-wizard-step-company-logo-added>*/
/* </div>*/
