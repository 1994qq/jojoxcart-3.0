<?php

/* modules/XC/Onboarding/wizard_steps/company_logo/body.twig */
class __TwigTemplate_01897301ebedddabaeb5c2c4c1b8891fa0cbeefc2341ad5db3a8b620dacc4888 extends \XLite\Core\Templating\Twig\Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 4
        echo "
<div
  class=\"onboarding-wizard-step step-";
        // line 6
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getStepIndex", array(), "method"), "html", null, true);
        echo "\"
  v-show=\"isCurrentStep('";
        // line 7
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getStepIndex", array(), "method"), "html", null, true);
        echo "')\"
  :transition=\"stepTransition\">
  <xlite-wizard-step-company-logo inline-template>
    <div class=\"step-contents\">
      <h2 class=\"heading\">";
        // line 11
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('t')->getCallable(), array("Upload your company logo")), "html", null, true);
        echo "</h2>
      <p class=\"text\">";
        // line 12
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('t')->getCallable(), array("Design your online store to fit your brand")), "html", null, true);
        echo "</p>
      ";
        // line 13
        $this->startForm("XLite\\View\\Form\\SimpleForm", array("formTarget" => "onboarding_wizard", "formAction" => "upload_company_logo", "className" => "logo-upload-form"));        // line 14
        echo "        ";
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('widget')->getCallable(), array($this->env, $context, array(0 => "XLite\\View\\FormField\\FileUploader\\Image", "helpMessage" => call_user_func_array($this->env->getFunction('t')->getCallable(), array("Click here to upload your logo. Recommended 400x150px (PNG, JPG or SVG)")), "fieldOnly" => true, "fieldName" => "company_logo", "maxHeight" => "218", "maxWidth" => "398"))), "html", null, true);
        echo "
      ";
        $this->endForm();        // line 16
        echo "      <div class=\"buttons\">
        ";
        // line 17
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('widget')->getCallable(), array($this->env, $context, array(0 => "XLite\\View\\Button\\Regular", "id" => "company_logo-next_button", "label" => call_user_func_array($this->env->getFunction('t')->getCallable(), array("Skip this step")), "style" => "regular-main-button", "attributes" => array("@click" => "skipStep", "data-dirty" => call_user_func_array($this->env->getFunction('t')->getCallable(), array("Save and go to the next step")), "data-pristine" => call_user_func_array($this->env->getFunction('t')->getCallable(), array("Skip this step"))), "jsCode" => "null;"))), "html", null, true);
        echo "
      </div>
    </div>
  </xlite-wizard-step-company-logo>
</div>";
    }

    public function getTemplateName()
    {
        return "modules/XC/Onboarding/wizard_steps/company_logo/body.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  51 => 17,  48 => 16,  43 => 14,  42 => 13,  38 => 12,  34 => 11,  27 => 7,  23 => 6,  19 => 4,);
    }
}
/* {##*/
/*  # Company logo step*/
/*  #}*/
/* */
/* <div*/
/*   class="onboarding-wizard-step step-{{ this.getStepIndex() }}"*/
/*   v-show="isCurrentStep('{{ this.getStepIndex() }}')"*/
/*   :transition="stepTransition">*/
/*   <xlite-wizard-step-company-logo inline-template>*/
/*     <div class="step-contents">*/
/*       <h2 class="heading">{{ t('Upload your company logo') }}</h2>*/
/*       <p class="text">{{ t('Design your online store to fit your brand') }}</p>*/
/*       {% form 'XLite\\View\\Form\\SimpleForm' with {formTarget: 'onboarding_wizard', formAction: 'upload_company_logo', className: "logo-upload-form"} %}*/
/*         {{ widget('XLite\\View\\FormField\\FileUploader\\Image', helpMessage=t("Click here to upload your logo. Recommended 400x150px (PNG, JPG or SVG)"), fieldOnly=true, fieldName='company_logo', maxHeight="218", maxWidth="398") }}*/
/*       {% endform %}*/
/*       <div class="buttons">*/
/*         {{ widget('XLite\\View\\Button\\Regular', id="company_logo-next_button", label=t("Skip this step"), style='regular-main-button', attributes={'@click': 'skipStep', 'data-dirty': t("Save and go to the next step"), 'data-pristine': t('Skip this step')}, jsCode="null;") }}*/
/*       </div>*/
/*     </div>*/
/*   </xlite-wizard-step-company-logo>*/
/* </div>*/
