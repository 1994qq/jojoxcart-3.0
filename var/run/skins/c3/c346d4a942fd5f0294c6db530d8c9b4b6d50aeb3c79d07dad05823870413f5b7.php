<?php

/* /var/www/html/xcart/skins/admin/modules/QSL/SpecialOffersBase/special_offers/conditions/name.twig */
class __TwigTemplate_835008df9a1e50c52ed3fb9a31648a5d6390a65f058c90e867e820da3610f023 extends \XLite\Core\Templating\Twig\Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 4
        echo "
<li class=\"condition name\">
  ";
        // line 6
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('widget')->getCallable(), array($this->env, $context, array(0 => "XLite\\View\\FormField\\Input\\Text", "fieldName" => "name", "value" => $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getCondition", array(0 => "name"), "method"), "label" => call_user_func_array($this->env->getFunction('t')->getCallable(), array("Name"))))), "html", null, true);
        echo "
</li>";
    }

    public function getTemplateName()
    {
        return "/var/www/html/xcart/skins/admin/modules/QSL/SpecialOffersBase/special_offers/conditions/name.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  23 => 6,  19 => 4,);
    }
}
/* {##*/
/*  # @ListChild (list="qsl.specialoffersbase.special_offers.list.search.conditions", weight="100")*/
/*  #}*/
/* */
/* <li class="condition name">*/
/*   {{ widget('XLite\\View\\FormField\\Input\\Text', fieldName='name', value=this.getCondition('name'), label=t('Name')) }}*/
/* </li>*/
