<?php

/* items_list/module/install/parts/columns/info/actions/install.twig */
class __TwigTemplate_12a19cba38e9a31dabbfdff0a75b90e6d41f4c3406a8b5333f2a2d82f34c5e19 extends \XLite\Core\Templating\Twig\Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 4
        echo "<label for=\"install-";
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["this"]) ? $context["this"] : null), "module", array()), "getModuleId", array(), "method"), "html", null, true);
        echo "\" class=\"install-module-button btn regular-button\">
  <input
    id=\"install-";
        // line 6
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["this"]) ? $context["this"] : null), "module", array()), "getModuleId", array(), "method"), "html", null, true);
        echo "\"
    type=\"checkbox\"
    data=\"";
        // line 8
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["this"]) ? $context["this"] : null), "module", array()), "getModuleId", array(), "method"), "html", null, true);
        echo "\"
    name=\"selectToInstall[";
        // line 9
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["this"]) ? $context["this"] : null), "module", array()), "getModuleId", array(), "method"), "html", null, true);
        echo "]\"
    ";
        // line 10
        if ($this->getAttribute((isset($context["this"]) ? $context["this"] : null), "isModuleSelected", array(0 => $this->getAttribute($this->getAttribute((isset($context["this"]) ? $context["this"] : null), "module", array()), "getModuleId", array(), "method")), "method")) {
            echo "checked=\"checked\"";
        }
        // line 11
        echo "    class=\"install-module-action\" />
  <span class=\"text\">";
        // line 12
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('t')->getCallable(), array("Install")), "html", null, true);
        echo "</span>
</label>
";
    }

    public function getTemplateName()
    {
        return "items_list/module/install/parts/columns/info/actions/install.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  45 => 12,  42 => 11,  38 => 10,  34 => 9,  30 => 8,  25 => 6,  19 => 4,);
    }
}
/* {##*/
/*  # Module select to install action widget*/
/*  #}*/
/* <label for="install-{{ this.module.getModuleId() }}" class="install-module-button btn regular-button">*/
/*   <input*/
/*     id="install-{{ this.module.getModuleId() }}"*/
/*     type="checkbox"*/
/*     data="{{ this.module.getModuleId() }}"*/
/*     name="selectToInstall[{{ this.module.getModuleId() }}]"*/
/*     {% if this.isModuleSelected(this.module.getModuleId()) %}checked="checked"{% endif %}*/
/*     class="install-module-action" />*/
/*   <span class="text">{{ t('Install') }}</span>*/
/* </label>*/
/* */
