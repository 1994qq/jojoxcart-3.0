<?php
// vim: set ts=4 sw=4 sts=4 et:

/**
 * Copyright (c) 2011-present Qualiteam software Ltd. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 */

namespace XLite\Module\CDev\VAT\Model;

/**
 * Product model
 */
 class Product extends \XLite\Module\QSL\FlyoutCategoriesMenu\Model\Product implements \XLite\Base\IDecorator
{
    /**
     * Included tax list 
     * 
     * @var array
     */
    protected $includedTaxList;


    /**
     * Get included tax list
     *
     * @param boolean $override Override calculation flag OPTIONAL
     * 
     * @return array
     */
    public function getIncludedTaxList($override = false)
    {
        if ($this->getTaxable() && !isset($this->includedTaxList) || $override) {
            $this->includedTaxList = \XLite\Module\CDev\VAT\Logic\Product\Tax::getInstance()
                ->calculateProductTaxes($this, $this->getNetPrice());
        }

        return $this->includedTaxList;
    }
}

