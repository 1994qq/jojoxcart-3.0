<?php
// vim: set ts=4 sw=4 sts=4 et:

/**
 * Copyright (c) 2011-present Qualiteam software Ltd. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 */

namespace XLite\Module\CDev\VAT\Module\XC\FastLaneCheckout\View\Block;

/**
 * Class Address
 *
 * @Decorator\Depend("XC\FastLaneCheckout")
 */
abstract class Address extends \XLite\Module\XC\FastLaneCheckout\View\Blocks\AddressAbstract implements \XLite\Base\IDecorator
{
    /**
     * Get JS files
     *
     * @return array
     */
    public function getJSFiles()
    {
        $list = parent::getJSFiles();
        $list[] = 'modules/CDev/VAT/fastlane_checkout.js';

        return $list;
    }

    public function getCSSFiles()
    {
        $list = parent::getCSSFiles();
        $list[] = 'modules/CDev/VAT/fastlane_checkout.css';

        return $list;
    }
}
