<?php
// vim: set ts=4 sw=4 sts=4 et:

/**
 * Copyright (c) 2011-present Qualiteam software Ltd. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 */

namespace XLite\Module\CDev\VAT\View\FormField;

/**
 * Tax number checker selector
 */
class VATNumberChecker extends \XLite\View\FormField\Select\Regular
{
    /**
     * getDefaultOptions
     *
     * @return array
     */
    protected function getDefaultOptions()
    {
        return array(
            '\XLite\Module\CDev\VAT\Core\VIES'          => static::t('VAT Information Exchange System (VIES)'),
            '\XLite\Module\CDev\VAT\Core\VatLayer'  => static::t('European VAT Number Validation API (vatlayer.com)'),
        );
    }
}
