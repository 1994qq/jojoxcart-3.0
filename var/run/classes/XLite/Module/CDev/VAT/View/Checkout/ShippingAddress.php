<?php
// vim: set ts=4 sw=4 sts=4 et:

/**
 * Copyright (c) 2011-present Qualiteam software Ltd. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 */

namespace XLite\Module\CDev\VAT\View\Checkout;

/**
 * Shipping address block
 */
abstract class ShippingAddress extends \XLite\View\Checkout\ShippingAddressAbstract implements \XLite\Base\IDecorator
{
    /**
     * Check - shipping and billing addrsses are same or not
     *
     * @return boolean
     */
    protected function isSameAddress()
    {
        return is_null(\XLite\Core\Session::getInstance()->same_address)
            ? !$this->getCart()->getProfile() || $this->getCart()->getProfile()->isEqualAddress()
            : \XLite\Core\Session::getInstance()->same_address;
    }

    /**
     * Get an array of address fields
     *
     * @return array
     */
    protected function getAddressFields()
    {
        $result = parent::getAddressFields();

        if (isset($result['vat_number']) && !$this->isSameAddress()) {

            $result['vat_number']['additionalClass'] .= ' vat-hidden';
        }

        return $result;
    }
}
