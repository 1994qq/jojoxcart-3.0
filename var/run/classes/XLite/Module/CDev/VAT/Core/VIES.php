<?php
// vim: set ts=4 sw=4 sts=4 et:

/**
 * Copyright (c) 2011-present Qualiteam software Ltd. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 */

namespace XLite\Module\CDev\VAT\Core;

/**
 * Check vat number at VAT Information Exchange System (VIES)
 * WSDL: http://ec.europa.eu/taxation_customs/vies/checkVatService.wsdl
 */
class VIES extends \XLite\Module\CDev\VAT\Core\AVATNumberChecker
{
    /**
     * Check if VAT number is valid
     *
     * @param string $countryCode Country code
     * @param string $vatNumber   VAT number
     *
     * @return mixed
     */
    public function isValid($countryCode, $vatNumber)
    {
        $result = false;

        if ($countryCode && $vatNumber) {
            $result = $this->doRequest($countryCode, $vatNumber);
        }

        return $result;
    }

    /**
     * Return true if service exists
     *
     * @return boolean
     */
    public function isAvailable()
    {
        return class_exists('\SoapClient');
    }


    /**
     * Returns service priority (the higher the better)
     *
     * @return integer
     */
    public function getPriority()
    {
        return 5;
    }

    /**
     * Do request
     *
     * @param string $url Url
     *
     * @return boolean
     */
    protected function doRequest($countryCode, $vatNumber)
    {
        $result = null;

        $params = array(
            'countryCode' => $countryCode,
            'vatNumber'   => $vatNumber,
        );

        $wsdl = LC_DIR_MODULES . 'CDev' . LC_DS . 'VAT' . LC_DS . 'Core' . LC_DS . 'wsdl' . LC_DS . 'checkVatService.wsdl';

        try {

            $client = new \SoapClient($wsdl);
            $response = $client->checkVat($params);

            $result = $response->valid;

            $log = array(
                'countryCode' => $response->countryCode,
                'vatNumber'   => $response->vatNumber,
                'valid'       => $response->valid,
                'requestDate' => $response->requestDate,
                'name'        => $response->name,
                'address'     => $response->address,
            );

        } catch(\Exception $e) {

            if ($e->faultstring) {

                switch ($e->faultstring) {
                    case 'INVALID_INPUT': {
                        $log = 'The provided CountryCode is invalid or the VAT number is empty.';
                        break;
                    }

                    case 'SERVICE_UNAVAILABLE': {
                        $log = 'The SOAP service is unavailable, try again later.';
                        break;
                    }

                    case 'MS_UNAVAILABLE': {
                        $log = 'The Member State service is unavailable, try again later or with another Member State.';
                        break;
                    }

                    case 'TIMEOUT': {
                        $log = 'The Member State service could not be reach in time, try again later or with another Member State.';
                        break;
                    }

                    case 'SERVER_BUSY': {
                        $log = 'The service can\'t process your request. Try again later.';
                        break;
                    }
                }
            }

            if (empty($log) && $e->getMessage()) {
                $log = $e->getMessage();

            } else {
                $log = 'Unknown error.';
            }
        }

        $this->addLog(
            array(
                'class'       => get_class($this),
                'countryCode' => $countryCode,
                'vatNumber'   => $vatNumber,
                'response'    => $log,
            )
        );

        return $result;
    }
}
