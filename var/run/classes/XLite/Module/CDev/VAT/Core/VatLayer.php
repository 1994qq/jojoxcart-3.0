<?php
// vim: set ts=4 sw=4 sts=4 et:

/**
 * Copyright (c) 2011-present Qualiteam software Ltd. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 */

namespace XLite\Module\CDev\VAT\Core;

/**
 * Check vat number at http://apilayer.net (Vatlayer)
 */
class VatLayer extends \XLite\Module\CDev\VAT\Core\AVATNumberChecker
{
    const ENDPOINT = 'http://apilayer.net/api/validate';

    /**
     * Check if VAT number is valid
     *
     * @param string $countryCode Country code
     * @param string $vatNumber   VAT number
     *
     * @return mixed
     */
    public function isValid($countryCode, $vatNumber)
    {
        $result = false;

        if ($countryCode && $vatNumber) {
            $fullVat = $countryCode . strtoupper($vatNumber);
            $data = $this->doRequest($fullVat);

            $result = $data && $data['valid'] && $data['country_code'] == $countryCode;
        }

        return $result;
    }

    /**
     * Return true if service exists
     *
     * @return boolean
     */
    public function isAvailable()
    {
        $key = $this->getApiKey();
        return !empty($key);
    }

    /**
     * Returns service priority (the higher the better)
     *
     * @return integer
     */
    public function getPriority()
    {
        return $this->isAvailable() ? 10 : parent::getPriority();
    }

    /**
     * Returns API key for Vatlayer.
     * 
     * @return string
     */
    protected function getApiKey()
    {
        return \XLite\Core\Config::getInstance()->CDev->VAT->vatlayer_apikey;
    }

    /**
     * Do request
     *
     * @param string $vatNumber VAT number to check
     *
     * @return array|null
     */
    protected function doRequest($vatNumber)
    {
        $result = null;
        
        $url = static::ENDPOINT . '?' . 'access_key=' . $this->getApiKey() . '&vat_number=' . $vatNumber;
        $request = new \XLite\Core\HTTP\Request($url);
        $request->verb = 'GET';
        $response = $request->sendRequest();

        if (200 == $response->code && !empty($response->body)) {
            $result = json_decode($response->body, true);
        }

        $this->addLog(
            array(
                'class' => get_class($this),
                'url'   => $url,
                'response' => $response->body,
            )
        );

        return $result;
    }
}
