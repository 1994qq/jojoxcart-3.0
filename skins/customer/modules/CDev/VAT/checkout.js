/* vim: set ts=2 sw=2 sts=2 et: */

/**
 * Shipping address controller
 *
 * Copyright (c) 2011-present Qualiteam software Ltd. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 */

core.bind('load', function () {
  decorate(
    'BillingAddressView',
    'handleSameAddress',
    function(event)
    {
      arguments.callee.previousMethod.apply(this, arguments);

      if (this.base.find('#same_address:checked').length) {
        // The billing address IS the same one with the shipping - hide the billing section
        jQuery('.shipping-address li.item-vat_number').removeClass('vat-hidden');
      } else {
        // The billing address IS NOT the same one with the shipping - show the billing section
        jQuery('.shipping-address li.item-vat_number').addClass('vat-hidden');
      }
    }
  );

  decorate(
      'CartItemsView',
      'handleUpdateCart',
      function(event, data)
      {
        arguments.callee.previousMethod.apply(this, arguments);

        var intersect = _.intersection(
            _.keys(data),
            ['shippingAddressFields', 'billingAddressFields', 'sameAddress']
        );

        if (0 < intersect.length) {
          this.load();
        }
      }
  );

});
