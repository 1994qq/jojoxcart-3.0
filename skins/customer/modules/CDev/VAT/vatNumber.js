/* vim: set ts=2 sw=2 sts=2 et: */

/**
 * Vat number field validation
 *
 * Copyright (c) 2011-present Qualiteam software Ltd. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 */

core.bind(
  'load',
  function () {

  CommonElement.prototype.validationCacheVAT = {};

  // List of specific VAT number prefixes for some countries
  CommonElement.prototype.countryCodeVATPrefixes = {
    'GR': 'EL'
  };

  CommonElement.prototype.validateVat_number = function()
  {
      var countryCode = this.getForm().getElements().filter('.field-country_code').val() ||
        jQuery('.address-country_code', this.element.form).find('select').val();
      var result = this.isVATValidFormat(this.element.value, countryCode);

      if (result.status && this.element.value) {
        if (typeof(this.validationCacheVAT[countryCode + this.element.value]) !== 'undefined') {
          result = this.getVATValidationResult(result, this.validationCacheVAT[countryCode + this.element.value]);

        } else if (!this.$element.hasClass('progress-mark-apply')) {
          this.markAsProgress();

          this.runVatValidationRequest(result, countryCode);
        }

      } else if (!this.element.value) {
        this.markAsInvalid();
        this.unmarkAsInvalid();

      } else if (!result.status) {
        var message = core.t(result.message);
        this.markAsInvalid(message);
      }

      return result;
    }

    CommonElement.prototype.runVatValidationRequest = function (result, countryCode) {
      core.get(
        URLHandler.buildURL({
          target:      'address_book',
          action:      'check_vat_number',
          countryCode: countryCode,
          vatNumber:   this.element.value
        }),
        (function(xhr, status, data) {
          this.unmarkAsProgress();
          this.validationCacheVAT[countryCode + this.element.value] = data;
          result = this.getVATValidationResult(result, this.validationCacheVAT[countryCode + this.element.value]);
        }).bind(this),
        {},
        {async: true}
      );
    }

    CommonElement.prototype.isVATValidFormat = function (vatNumber, countryCode)
    {
      var regexp = new Array();

      // Source of regexps is here: http://www.braemoor.co.uk/software/vat.shtml

      regexp.push (/^(AT)U(\d{8})$/);                           //** Austria
      regexp.push (/^(BE)(0?\d{9})$/);                          //** Belgium
      regexp.push (/^(BG)(\d{9,10})$/);                         //** Bulgaria
      regexp.push (/^(CH)E(\d{9})(MWST)?$/);                    //** Switzerland
      regexp.push (/^(CY)([0-59]\d{7}[A-Z])$/);                 //** Cyprus
      regexp.push (/^(CZ)(\d{8,10})(\d{3})?$/);                 //** Czech Republic
      regexp.push (/^(DE)([1-9]\d{8})$/);                       //** Germany
      regexp.push (/^(DK)(\d{8})$/);                            //** Denmark
      regexp.push (/^(EE)(10\d{7})$/);                          //** Estonia
      regexp.push (/^(EL)(\d{9})$/);                            //** Greece
      regexp.push (/^(ES)([A-Z]\d{8})$/);                       //** Spain (National juridical entities)
      regexp.push (/^(ES)([A-HN-SW]\d{7}[A-J])$/);              //** Spain (Other juridical entities)
      regexp.push (/^(ES)([0-9YZ]\d{7}[A-Z])$/);                //** Spain (Personal entities type 1)
      regexp.push (/^(ES)([KLMX]\d{7}[A-Z])$/);                 //** Spain (Personal entities type 2)
      regexp.push (/^(EU)(\d{9})$/);                            //** EU-type
      regexp.push (/^(FI)(\d{8})$/);                            //** Finland
      regexp.push (/^(FR)(\d{11})$/);                           //** France (1)
      regexp.push (/^(FR)([A-HJ-NP-Z]\d{10})$/);                // France (2)
      regexp.push (/^(FR)(\d[A-HJ-NP-Z]\d{9})$/);               // France (3)
      regexp.push (/^(FR)([A-HJ-NP-Z]{2}\d{9})$/);              // France (4)
      regexp.push (/^(GB)?(\d{9})$/);                           //** UK (Standard)
      regexp.push (/^(GB)?(\d{12})$/);                          //** UK (Branches)
      regexp.push (/^(GB)?(GD\d{3})$/);                         //** UK (Government)
      regexp.push (/^(GB)?(HA\d{3})$/);                         //** UK (Health authority)
      regexp.push (/^(HR)(\d{11})$/);                           //** Croatia
      regexp.push (/^(HU)(\d{8})$/);                            //** Hungary
      regexp.push (/^(IE)(\d{7}[A-W])$/);                       //** Ireland (1)
      regexp.push (/^(IE)([7-9][A-Z\*\+)]\d{5}[A-W])$/);        //** Ireland (2)
      regexp.push (/^(IE)(\d{7}[A-W][AH])$/);                   //** Ireland (3)
      regexp.push (/^(IT)(\d{11})$/);                           //** Italy
      regexp.push (/^(LV)(\d{11})$/);                           //** Latvia
      regexp.push (/^(LT)(\d{9}|\d{12})$/);                     //** Lithunia
      regexp.push (/^(LU)(\d{8})$/);                            //** Luxembourg
      regexp.push (/^(MT)([1-9]\d{7})$/);                       //** Malta
      regexp.push (/^(NL)(\d{9})B\d{2}$/);                      //** Netherlands
      regexp.push (/^(NO)(\d{9})$/);                            //** Norway (not EU)
      regexp.push (/^(PL)(\d{10})$/);                           //** Poland
      regexp.push (/^(PT)(\d{9})$/);                            //** Portugal
      regexp.push (/^(RO)([1-9]\d{1,9})$/);                     //** Romania
      regexp.push (/^(RU)(\d{10}|\d{12})$/);                    //** Russia
      regexp.push (/^(RS)(\d{9})$/);                            //** Serbia
      regexp.push (/^(SI)([1-9]\d{7})$/);                       //** Slovenia
      regexp.push (/^(SK)([1-9]\d[2346-9]\d{7})$/);             //** Slovakia Republic
      regexp.push (/^(SE)(\d{10}01)$/);                         //** Sweden

      var apply = isElement(this.element, 'input');
      var result = {
        status:  true,
        message: '',
        apply:   apply
      };

      if (this.element.value) {

        vatNumber = vatNumber.toUpperCase();
        vatNumber = vatNumber.replace (/(\s|-|\.)+/g, '');

        if (this.countryCodeVATPrefixes[countryCode]) {
          countryCode = this.countryCodeVATPrefixes[countryCode];
        }

        var found = false;
        var values2Check = new Array();
        values2Check.push(countryCode + vatNumber);
        values2Check.push(vatNumber);

        for (j = 0; j < values2Check.length; j++) {
          for (i = 0; i < regexp.length; i++) {
            if (regexp[i].test(values2Check[j])) {
              found = true;
              break;
            }
          }
          if (found) {
            break;
          }
        }

        if (!found) {
          result.status = false;
          result.message = 'Wrong VAT number format for selected country.';
        }
      }

      return result;
    }

    CommonElement.prototype.getVATValidationResult = function(result, status)
    {
      if ('"valid"' === status) {
        result.status = true;
        result.message = 'VAT number is valid';
        this.markAsValid(core.t(result.message));

      } else if ('"format_valid"' === status) {
        result.status = true;
        result.message = 'VAT number format is valid';
        this.markAsValid(core.t(result.message));

      } else if ('"invalid"' === status) {
        result.status = false;
        result.message = 'VAT number is invalid';
        this.markAsInvalid(core.t(result.message));

      } else if ('"error"' === status) {
        result.status = false;
        result.message = 'VAT number is invalid';
        this.markAsInvalid(core.t(result.message));
      } 

      return result;
    }

  }
);
