/* vim: set ts=2 sw=2 sts=2 et: */

/**
 * Taxes controller
 *
 * Copyright (c) 2011-present Qualiteam software Ltd. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 */

jQuery().ready(
  function() {
    jQuery('.edit-vat-tax table.form button.switch-state')
      .removeAttr('onclick')
      .click(
      function() {
        var o = this;
        o.disabled = true;
        core.post(
          URLHandler.buildURL({target: 'vat_tax', action: 'switch'}),
          function(XMLHttpRequest, textStatus, data, valid) {
            o.disabled = false;
            if (valid) {
              var td = jQuery('.edit-vat-tax table.form td.button');
              if (td.hasClass('enabled')) {
                td.removeClass('enabled');
                td.addClass('disabled');

              } else {
                td.removeClass('disabled');
                td.addClass('enabled');
              }
            }
          }
        );

        return false;
      }
    );

    jQuery('#ignore-memberships').click(
      function() {
        jQuery('.edit-sales-tax').toggleClass('no-memberships', jQuery(this).is(':checked'));
      }
    );

    jQuery('#common-settings-link span').click(
      function() {
        var box = jQuery('#common-settings');
        var doExpand = jQuery(box).hasClass('hidden');
        var boxAction;
        if (doExpand) {
          boxAction = 'expand';
          jQuery(box).removeClass('hidden');
          jQuery('#common-settings-link span.collapsed-common-settings').addClass('hidden');
          jQuery('#common-settings-link span.expanded-common-settings').removeClass('hidden');

        } else {
          boxAction = 'collapse';
          jQuery(box).addClass('hidden');
          jQuery('#common-settings-link span.expanded-common-settings').addClass('hidden');
          jQuery('#common-settings-link span.collapsed-common-settings').removeClass('hidden');
        }

        core.post(
          URLHandler.buildURL({target: 'vat_tax', action: boxAction})
        )
      }
    );

    jQuery('#use-simplified-invoice').change(
      function() {
        if ('O' == jQuery(this).val()) {
          jQuery('#simplified-invoice-limit-block').removeClass('hidden');

        } else {
          jQuery('#simplified-invoice-limit-block').addClass('hidden');
        }
      }
    );
  }
);
