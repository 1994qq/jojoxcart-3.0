/* vim: set ts=2 sw=2 sts=2 et: */

/**
 * VAT management form controller
 *
 * Copyright (c) 2011-present Qualiteam software Ltd. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 */

jQuery().ready(
  function() {
    jQuery('.edit-vat-tax form').commonController('submitOnlyChanged', false);
  }
);

core.microhandlers.add(
    'IgnoreMemberships',
    '#ignore-memberships',
    function () {
        jQuery(this).bind('change', function () {
            var value = jQuery(this).is(':checked');
            jQuery('.edit-vat-tax').toggleClass('no-memberships', value)
        });
    }
);
