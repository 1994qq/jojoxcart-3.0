<?php
// vim: set ts=4 sw=4 sts=4 et:

/**
 * Copyright (c) 2011-present Qualiteam software Ltd. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 */

namespace XLite\Module\CDev\VAT\Model\Tax;

/**
 * Rate
 *
 * @Entity
 * @Table (name="vat_tax_rates")
 */
class Rate extends \XLite\Model\AEntity
{
    /**
     * Rate type codes
     */
    const TYPE_ABSOLUTE = 'a';
    const TYPE_PERCENT  = 'p';


    /**
     * Rate unique ID
     *
     * @var integer
     *
     * @Id
     * @GeneratedValue (strategy="AUTO")
     * @Column         (type="integer", options={ "unsigned": true })
     */
    protected $id;

    /**
     * Value
     *
     * @var float
     *
     * @Column (type="decimal", precision=14, scale=4)
     */
    protected $value = 0.0000;

    /**
     * Type
     *
     * @var string
     *
     * @Column (type="string", options={ "fixed": true }, length=1)
     */
    protected $type = self::TYPE_PERCENT;

    /**
     * Position
     *
     * @var integer
     *
     * @Column (type="integer")
     */
    protected $position = 0;

    /**
     * Tax (relation)
     *
     * @var \XLite\Module\CDev\VAT\Model\Tax
     *
     * @ManyToOne  (targetEntity="XLite\Module\CDev\VAT\Model\Tax", inversedBy="rates")
     * @JoinColumn (name="tax_id", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $tax;

    /**
     * Zone (relation)
     *
     * @var \XLite\Model\Zone
     *
     * @ManyToOne  (targetEntity="XLite\Model\Zone")
     * @JoinColumn (name="zone_id", referencedColumnName="zone_id", onDelete="CASCADE")
     */
    protected $zone;

    /**
     * Tax class (relation)
     *
     * @var \XLite\Model\TaxClass
     *
     * @ManyToOne  (targetEntity="XLite\Model\TaxClass")
     * @JoinColumn (name="tax_class_id", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $taxClass;

    /**
     * Membership (relation)
     *
     * @var \XLite\Model\Membership
     *
     * @ManyToOne  (targetEntity="XLite\Model\Membership")
     * @JoinColumn (name="membership_id", referencedColumnName="membership_id", onDelete="CASCADE")
     */
    protected $membership;

    /**
     * For product without tax class
     *
     * @var boolean
     *
     * @Column (type="boolean")
     */
    protected $noTaxClass = false;

    /**
     * Check if rate is applied by specified zones and membership
     *
     * @param array                   $zones        Zone id list
     * @param \XLite\Model\Membership $membership   Membership OPTIONAL
     * @param \XLite\Model\TaxClass   $taxClass     Tax class OPTIONAL
     * @param boolean                 $skipTaxClass Skip tax class OPTIONAL
     *
     * @return boolean
     */
    public function isApplied(
        array $zones,
        \XLite\Model\Membership $membership = null,
        \XLite\Model\TaxClass $taxClass = null,
        $skipTaxClass = false
    ) {

        // Check: is VAT rate zone in the list of price inc.VAT zones
        $result = $this->getZone() && in_array($this->getZone()->getZoneId(), $zones);

        if (!$result && $this->getZone() && $this->getZone()->getIsDefault() && !$this->getZone()->hasZoneElements()) {
            // VAT rate zone is default and empty: mark rate as applied to zone
            $result = true;
        }

        if (
            $result
            && !\XLite\Core\Config::getInstance()->CDev->VAT->ignore_memberships
            && $this->getMembership()
        ) {
            $result = $membership && $this->getMembership()->getMembershipId() == $membership->getMembershipId();
        }

        if ($result && !$skipTaxClass) {
            if ($this->getNoTaxClass()) {
                $result = !$taxClass;

            } else if ($this->getTaxClass()) {
                $result = $taxClass && $this->getTaxClass()->getId() == $taxClass->getId();
            }
        }

        return $result;
    }

    // {{{ Product price calculation

    /**
     * Calculate and return tax rate value for price which includes tax rate
     *
     * @param \XLite\Model\Product $product Product
     * @param float                $price   Price
     *
     * @return float
     */
    public function calculateProductPriceExcludingTax(\XLite\Model\Product $product, $price)
    {
        return $price
            ? $this->calculateValueExcludingTax($price)
            : 0;
    }

    /**
     * Calculate value excluding tax
     *
     * @param float $base Base
     *
     * @return float
     */
    public function calculateValueExcludingTax($base)
    {
        return $this->getType() == static::TYPE_PERCENT
            ? $this->calculatePriceIncludePercent($base)
            : $this->calculatePriceIncludeAbsolute($base);
    }

    /**
     * Calculate product price including tax
     *
     * @param \XLite\Model\Product $product Product
     * @param float                $price   Pure price, without including tax
     *
     * @return float
     */
    public function calculateProductPriceIncludingTax(\XLite\Model\Product $product, $price)
    {
        return $price
            ? $this->calculateValueIncludingTax($price)
            : 0;
    }

    /**
     * Calculate value including tax
     *
     * @param float $base Base
     *
     * @return float
     */
    public function calculateValueIncludingTax($base)
    {
        return $this->getType() == static::TYPE_PERCENT
            ? $this->calculatePriceExcludePercent($base)
            : $this->calculatePriceExcludeAbsolute($base);
    }

    /**
     * Calculate VAT for single product price (percent value)
     *
     * @param float $price Price
     *
     * @return float
     */
    protected function calculatePriceIncludePercent($price)
    {
        return $price - $price / (100 + $this->getValue()) * 100;
    }

    /**
     * Calculate VAT for single product price (absolute value)
     *
     * @param float $price Price
     *
     * @return float
     */
    protected function calculatePriceIncludeAbsolute($price)
    {
        return $this->getValue();
    }

    /**
     * Calculate product price's excluded tax (as percent)
     *
     * @param float $price Product price
     *
     * @return float
     */
    protected function calculatePriceExcludePercent($price)
    {
        return $price * $this->getValue() / 100;
    }

    /**
     * Calculate product price's excluded tax (as absolute)
     *
     * @param float $price Price
     *
     * @return float
     */
    protected function calculatePriceExcludeAbsolute($price)
    {
        return $this->getValue();
    }

    // }}}

    // {{{ Search conditions

    /**
     * Get exclude tax formula
     *
     * @param string $priceField Product price field
     *
     * @return string
     */
    public function getExcludeTaxFormula($priceField)
    {
        return $this->getType() == self::TYPE_PERCENT
            ? $priceField . ' * ' . $this->getValue() . ' / ' . (100 + $this->getValue())
            : $this->getValue();
    }

    /**
     * Get include tax formula
     *
     * @param string $priceField Product price field
     *
     * @return string
     */
    public function getIncludeTaxFormula($priceField)
    {
        return $this->getType() == self::TYPE_PERCENT
            ? $priceField . ' * ' . ((100 + $this->getValue()) / 100)
            : $this->getValue();
    }

    // }}}

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set value
     *
     * @param float $value
     * @return Rate
     */
    public function setValue($value)
    {
        $this->value = $value;
        return $this;
    }

    /**
     * Get value
     *
     * @return float
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Set type
     *
     * @param string $type
     * @return Rate
     */
    public function setType($type)
    {
        $this->type = $type;
        return $this;
    }

    /**
     * Get type
     *
     * @return string 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set position
     *
     * @param integer $position
     * @return Rate
     */
    public function setPosition($position)
    {
        $this->position = $position;
        return $this;
    }

    /**
     * Get position
     *
     * @return integer 
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * Set noTaxClass
     *
     * @param boolean $noTaxClass
     * @return Rate
     */
    public function setNoTaxClass($noTaxClass)
    {
        $this->noTaxClass = $noTaxClass;
        return $this;
    }

    /**
     * Get noTaxClass
     *
     * @return boolean 
     */
    public function getNoTaxClass()
    {
        return $this->noTaxClass;
    }

    /**
     * Set tax
     *
     * @param \XLite\Module\CDev\VAT\Model\Tax $tax
     * @return Rate
     */
    public function setTax(\XLite\Module\CDev\VAT\Model\Tax $tax = null)
    {
        $this->tax = $tax;
        return $this;
    }

    /**
     * Get tax
     *
     * @return \XLite\Module\CDev\VAT\Model\Tax 
     */
    public function getTax()
    {
        return $this->tax;
    }

    /**
     * Set zone
     *
     * @param \XLite\Model\Zone $zone
     * @return Rate
     */
    public function setZone(\XLite\Model\Zone $zone = null)
    {
        $this->zone = $zone;
        return $this;
    }

    /**
     * Get zone
     *
     * @return \XLite\Model\Zone 
     */
    public function getZone()
    {
        return $this->zone;
    }

    /**
     * Set taxClass
     *
     * @param \XLite\Model\TaxClass $taxClass
     * @return Rate
     */
    public function setTaxClass(\XLite\Model\TaxClass $taxClass = null)
    {
        $this->taxClass = $taxClass;
        return $this;
    }

    /**
     * Get taxClass
     *
     * @return \XLite\Model\TaxClass 
     */
    public function getTaxClass()
    {
        return $this->taxClass;
    }

    /**
     * Set membership
     *
     * @param \XLite\Model\Membership $membership
     * @return Rate
     */
    public function setMembership(\XLite\Model\Membership $membership = null)
    {
        $this->membership = $membership;
        return $this;
    }

    /**
     * Get membership
     *
     * @return \XLite\Model\Membership 
     */
    public function getMembership()
    {
        return $this->membership;
    }
}
