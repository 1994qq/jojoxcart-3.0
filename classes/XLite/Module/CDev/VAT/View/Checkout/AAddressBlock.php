<?php
// vim: set ts=4 sw=4 sts=4 et:

/**
 * Copyright (c) 2011-present Qualiteam software Ltd. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 */

namespace XLite\Module\CDev\VAT\View\Checkout;

/**
 * Address block info
 */
abstract class AAddressBlock extends \XLite\View\Checkout\AAddressBlock implements \XLite\Base\IDecorator
{
    /**
     * Get JS files
     *
     * @return array
     */
    public function getJSFiles()
    {
        $list = parent::getJSFiles();
        $list[] = 'modules/CDev/VAT/checkout.js';

        return $list;
    }

    /**
     * Get CSS files
     *
     * @return array
     */
    public function getCSSFiles()
    {
        $list = parent::getCSSFiles();
        $list[] = 'modules/CDev/VAT/checkout.css';

        return $list;
    }

    /**
     * Get an array of address fields
     *
     * @return array
     */
    protected function getAddressFields()
    {
        $result = parent::getAddressFields();

        if (isset($result['vat_number'])) {
            $result['vat_number'][\XLite\View\Model\AModel::SCHEMA_DEPENDENCY] = array(
                \XLite\View\Model\AModel::DEPENDENCY_SHOW => array(
                    $this->getFieldFullName('country_code')
                        => \XLite\Module\CDev\VAT\Core\VATNumberChecker::getAllowedCountries(),
                ),
            );
            $result['vat_number']['additionalClass'] = 'has-dependency';
        }

        return $result;
    }
}
