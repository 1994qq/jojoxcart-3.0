<?php
// vim: set ts=4 sw=4 sts=4 et:

/**
 * Copyright (c) 2011-present Qualiteam software Ltd. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 */

namespace XLite\Module\CDev\VAT\Core;

/**
 * Check vat number
 */
abstract class AVATNumberChecker extends \XLite\Base\Singleton
{
    /**
     * Check if vat number is valid
     *
     * @param string $countryCode Country code
     * @param string $vatNumber   VAT number
     *
     * @return boolean|string
     */
    abstract public function isValid($countryCode, $vatNumber);

    /**
     * Return true if service is available
     *
     * @return boolean
     */
    public function isAvailable()
    {
        return true;
    }

    /**
     * Returns service priority (the higher the better)
     *
     * @return integer
     */
    public function getPriority()
    {
        return 0;
    }

    /**
     * Add log
     *
     * @param mixed $data Data to log
     *
     * @return void
     */
    protected function addLog($data)
    {
        \XLite\Logger::logCustom('VAT', $data);
    }
}
