<?php
// vim: set ts=4 sw=4 sts=4 et:

/**
 * Copyright (c) 2011-present Qualiteam software Ltd. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 */

namespace XLite\Module\CDev\VAT\Core;

/**
 * Check VAT number
 */
class VATNumberChecker extends \XLite\Base\Singleton
{
    /**
     * VAT is confirmed with online service
     */
    const STATUS_VALID = 'valid';

    /**
     * Only VAT format is checked, online services are down
     */
    const STATUS_FORMAT_VALID = 'format_valid';

    /**
     * VAT is invalid
     */
    const STATUS_INVALID = 'invalid';

    /**
     * Error checking VAT number (should not really happen, but who knows)
     */
    const STATUS_CHECK_ERROR = 'error';

    /**
     * Static cache of validation results
     * Format: [key] => [value], where key is md5(country.vatNumber) and value is (true|false)
     *
     * @var array
     */
    protected static $staticCache = array();

    /**
     * Get list of allowed countries
     *
     * @return array
     */
    public static function getAllowedCountries()
    {
        return array(
            'AT', 'BE', 'CZ', 'DE', 'CY',
            'DK', 'EE', 'GR', 'ES', 'FI',
            'FR', 'GB', 'HU', 'IE', 'IT',
            'LT', 'LU', 'LV', 'MT', 'NL',
            'PL', 'PT', 'SE', 'SI', 'SK',
            'BG', 'HR', 'RO',
        );
    }

    /**
     * Check if VAT number is valid
     *
     * @param string  $countryCode Country code
     * @param string  $vatNumber   VAT number
     * @param boolean $ignoreCache Ignore cache OPTIONAL
     *
     * @return string
     */
    public function checkVATNumber($countryCode, $vatNumber, $ignoreCache = false)
    {
        $status = null;

        // #BUG-1409
        if ('GR' == $countryCode) {
            $countryCode = 'EL';
        }

        $vatNumber = preg_replace('/(\s|-|\.)+/', '', $vatNumber);

        $vatNumber = preg_replace('/^' . preg_quote($countryCode) . '/', '', $vatNumber);

        if (!$ignoreCache) {
            $status = static::getDataFromCache($countryCode, $vatNumber);
        }

        if (null === $status) {
            $services = $this->getServicesList();

            usort($services, array($this, 'compareServicePriority'));

            foreach ($services as $service) {
                if ($service && $service->isAvailable()) {
                    $status = $service->isValid($countryCode, $vatNumber);

                    if (!is_null($status)) {
                        static::saveDataInCache($countryCode, $vatNumber, $status);
                        break;
                    }
                }
            }

            // perform offline format check
            if (null === $status) {
                $status = \XLite\Module\CDev\VAT\Core\OfflineChecker::getInstance()
                        ->isValid($countryCode, $vatNumber);
            }
        }  

        $status = $this->assignStatusType($status);

        return $status;
    }

    /**
     * Get list of all services
     *
     * @return array
     */
    protected function getServicesList()
    {
        return array(
            \XLite\Module\CDev\VAT\Core\VatLayer::getInstance(),
            \XLite\Module\CDev\VAT\Core\VIES::getInstance(),
        );
    }

    /**
     * Sort services by their priority
     * 
     * @param  \XLite\Module\CDev\VAT\Core\AVATNumberChecker $a First service
     * @param  \XLite\Module\CDev\VAT\Core\AVATNumberChecker $b First service
     * @return integer
     */
    protected function compareServicePriority($a, $b)
    {
        if ($a->getPriority() == $b->getPriority()) {
            return 0;
        }

        return ($a->getPriority() > $b->getPriority()) ? -1 : 1;
    }

    /**
     * Translates boolean checks status into string format for compatibility with online checkers.
     * @param  mixed $status VAT No. check status
     * @return string
     */
    protected function assignStatusType($status)
    {
        if ($status === true) {
            return static::STATUS_VALID;
        } elseif ($status === false) {
            return static::STATUS_INVALID;
        } elseif ($status === null) {
            return static::STATUS_CHECK_ERROR;
        }

        return $status;
    }

    /**
     * getDataFromCache
     *
     * @param string $countryCode Country code
     * @param string $vatNumber   VAT number
     *
     * @return mixed
     */
    protected function getDataFromCache($countryCode, $vatNumber)
    {
        $result = null;

        $key = md5($countryCode . $vatNumber);

        if (isset(static::$staticCache[$key])) {
            $result = static::$staticCache[$key];

        } else {
            $cacheDriver = \XLite\Core\Database::getCacheDriver();

            if ($cacheDriver->contains($key)) {
                $result = $cacheDriver->fetch($key);
            }
        }

        return $result;
    }

    /**
     * saveDataInCache
     *
     * @param string $countryCode Country code
     * @param string $vatNumber   Tax number
     * @param mixed  $data        Data object for saving in the cache
     *
     * @return void
     */
    protected function saveDataInCache($countryCode, $vatNumber, $data)
    {
        $key = md5($countryCode . $vatNumber);

        static::$staticCache[$key] = $data;

        \XLite\Core\Database::getCacheDriver()->save($key, $data);
    }
}
